#include "board.h"
#include "ds18b20.h"

typedef struct
{
	uint8_t	Buffer[8];
	uint8_t RxIndex;
	uint8_t TxIndex;
	uint8_t Bits;
	uint8_t Command;
	uint8_t	Error;
} DS18B20_Context_t;

DS18B20_Context_t DS18B20_Context;

USART_InitTypeDef DS18B20_UART_InitData;

#define DS18B20_0	0x00
#define DS18B20_1	0xFF

/**
  * @brief  Convert one byte to 8 bytes array
  * @param
  * @param
  * @retval
  */
void DS18B20_ToBits(uint8_t data)
{
	register int i;
	register DS18B20_Context_t * ds = &DS18B20_Context;
	ds->Command = data;
	for (i = 0; i < 8; i++)
	{
		ds->Buffer[i] = (data & 0x01) ? DS18B20_1 : DS18B20_0;
		data = data >> 1;
	}
}

/**
  * @brief  Convert 8 bytes array to one byte
  * @param
  * @param
  * @retval
  */
uint8_t DS18B20_ToByte()
{
	uint8_t data = 0, i;
	for (i = 0; i < 8; i++)
	{
		data = data >> 1;
		if (DS18B20_Context.Buffer[i] == DS18B20_1)
			data |= 0x80;
	}
	return data;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
uint8_t DS18B20_Init()
{
	register USART_InitTypeDef * ds = &DS18B20_UART_InitData;

	DS18B20_CLK(ENABLE);
	DS18B20_PIN_INIT();

	ds->USART_BaudRate = 115200;
	ds->USART_WordLength = USART_WordLength_8b;
	ds->USART_StopBits = USART_StopBits_1;
	ds->USART_Parity = USART_Parity_No;
	ds->USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	ds->USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

	USART_Init(DS18B20_UART, ds);
	USART_Cmd(DS18B20_UART, ENABLE);

	NVIC_SetPriority(DS18B20_IRQn,	0);		// Highest priority IRQ
	NVIC_ClearPendingIRQ(DS18B20_IRQn);
	NVIC_EnableIRQ(DS18B20_IRQn);

	return DS18B20_OK;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
void DS18B20_PowerOff(void)
{
	DS18B20_PIN_INIT();
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
void DS18B20_IRQHandler(void)
{
	register DS18B20_Context_t * ds = &DS18B20_Context;
	register USART_TypeDef *uart = DS18B20_UART;

	if (uart->SR & USART_SR_RXNE)
	{
		if (ds->RxIndex == ds->Bits)
		{
			(void)uart->DR;
			uart->CR1 &= ~USART_CR1_RXNEIE;
		}
		else
		{
			ds->Buffer[ds->RxIndex++] = uart->DR;
			if (ds->RxIndex == ds->Bits)
			{
				DS18B20_PIN_POWER();
			}
		}
	}

	if(uart->SR & USART_SR_TC)
	{
		if (ds->TxIndex == ds->Bits)
			uart->CR1 &= ~USART_CR1_TCIE;
		else
			uart->DR = ds->Buffer[ds->TxIndex++];
	}
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
void DS18B20_SendBits(uint8_t bits)
{
	uint32_t timeout = 5000;

	DS18B20_PIN_INIT();
	USART_ClearFlag(DS18B20_UART, USART_FLAG_RXNE);

	DS18B20_Context.RxIndex = DS18B20_Context.TxIndex = 0;
	DS18B20_Context.Bits = bits;
	DS18B20_UART->CR1 |= (USART_CR1_RXNEIE | USART_CR1_TCIE);

	while (DS18B20_Context.RxIndex != bits && timeout != 0)
	{
#ifdef DS18B20_GIVE_TICK_RTOS
		taskYIELD();
#endif
		if (DS18B20_Context.TxIndex != bits)
			timeout = 5000;
		else
			--timeout;
	}
	DS18B20_UART->CR1 &= ~(USART_CR1_RXNEIE | USART_CR1_TCIE);
	if (timeout == 0)
		DS18B20_Context.Error = 1;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
uint8_t DS18B20_Reset()
{
	register USART_InitTypeDef * us = &DS18B20_UART_InitData;

	us->USART_BaudRate = 9600;
	USART_Init(DS18B20_UART, us);
	DS18B20_Context.Buffer[0] = 0xF0;
	DS18B20_SendBits(1);

	us->USART_BaudRate = 115200;
	USART_Init(DS18B20_UART, us);

	return (DS18B20_Context.Buffer[0] == 0xF0) ? DS18B20_NO_DEVICE : DS18B20_OK;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
uint8_t DS18B20_Send (
	const uint8_t *command,
	uint8_t cLen,
	uint8_t *data,
	uint8_t dLen,
	uint8_t readStart
	)
{
	if (DS18B20_Reset() != DS18B20_OK)
		return DS18B20_NO_DEVICE;

	DS18B20_Context.Error = 0;

	while (cLen-- != 0)
	{
		DS18B20_ToBits(*command++);
		DS18B20_SendBits(8);

		if (readStart == 0)
		{
			if (dLen != 0)
			{
				*data++ = DS18B20_ToByte();
				--dLen;
			}
		}
		else
			--readStart;
	}
	return (DS18B20_Context.Error == 0) ? DS18B20_OK : DS18B20_ERROR;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
uint8_t DS18B20_Scan(uint8_t *buf, uint8_t num)
{
	uint8_t found = 0;
	uint8_t *lastDevice;
	uint8_t *curDevice = buf;
	uint8_t numBit, lastCollision, currentCollision, currentSelection;

	lastCollision = 0;
	while (found < num)
	{
		numBit = 1;
		currentCollision = 0;

		DS18B20_Send((const uint8_t *)"\xF0", 1, (void*)0, 0, 0);

		for (numBit = 1; numBit <= 64; numBit++)
		{
			DS18B20_ToBits(DS18B20_READ_SLOT);
			DS18B20_SendBits(2);

			if (DS18B20_Context.Buffer[0] == DS18B20_1)
			{
				if (DS18B20_Context.Buffer[1] == DS18B20_1)
					return found;
				else
					currentSelection = 1;
			}
			else
			{
				if (DS18B20_Context.Buffer[1] == DS18B20_1)
				{
					currentSelection = 0;
				}
				else
				{
					if (numBit < lastCollision)
					{
						//!!! if (lastDevice[(numBit - 1) >> 3] & 1 << ((numBit - 1) & 0x07))
						if (lastDevice[(numBit - 1) >> 3] & (1 << ((numBit - 1) & 0x07)))
						{
							currentSelection = 1;
							if (currentCollision < numBit)
								currentCollision = numBit;
						} else {
							currentSelection = 0;
						}
					}
					else
					{
						if (numBit == lastCollision)
							currentSelection = 0;
						else
						{
							currentSelection = 1;
							if (currentCollision < numBit)
								currentCollision = numBit;
						}
					}
				}
			}

			if (currentSelection == 1)
			{
				curDevice[(numBit - 1) >> 3] |= 1 << ((numBit - 1) & 0x07);
				DS18B20_ToBits(0x01);
			}
			else
			{
				curDevice[(numBit - 1) >> 3] &= ~(1 << ((numBit - 1) & 0x07));
				DS18B20_ToBits(0x00);
			}
			DS18B20_SendBits(1);
		}
		found++;
		lastDevice = curDevice;
		curDevice += 8;
		if (currentCollision == 0)
			return found;

		lastCollision = currentCollision;
	}

	return found;
}

float DS18B20_Celcius(uint8_t * data)
{
	int16_t raw = (data[1] << 8) | data[0];
	switch (data[4] & 0x60)
	{
		case 0x00:
			raw = raw & ~7;
			break;
		case 0x20:
			raw = raw & ~3;
			break;
		case 0x40:
			raw = raw & ~1;
			break;
	}
	return (float)raw / 16.0;
}
