#ifndef __CPAL_CONF_H
#define __CPAL_CONF_H

#define FM24_ADDRESS_READ	(0xA0 | 1)
#define FM24_ADDRESS_WRITE	(0xA0 | 0)

/* DMA Transfer UserCallbacks : To use a DMA Transfer UserCallbacks comment the relative define */
#define CPAL_I2C_DMATXTC_UserCallback()
#define CPAL_I2C_DMARXTC_UserCallback()

/* CriticalSectionCallback : Call User callback for critical section (should typically disable interrupts) */
#define CPAL_EnterCriticalSection_UserCallback	__disable_irq
#define CPAL_ExitCriticalSection_UserCallback	__enable_irq

/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------*/

/*  -- Section 4 :         **** Configure Timeout method, TimeoutCallback ****

    Description: This section allows you to implement your own Timeout Procedure.
                 By default Timeout procedure is implemented with Systick timer and 
                 CPAL_I2C_TIMEOUT_Manager is defined as SysTick_Handler.
                 */

#define __CPAL_TIMEOUT_INIT()		SysTick_Config((SystemCoreClock / 1000));	\
									NVIC_SetPriority (SysTick_IRQn, 0)

#define _CPAL_TIMEOUT_DEINIT()		SysTick->CTRL = 0	/*<! Disable the systick timer */ 
#define CPAL_I2C_TIMEOUT_Manager	SysTick_Handler		/*<! This callback is used to handle Timeout error.
															When a timeout occurs CPAL_TIMEOUT_UserCallback
															is called to handle this error */
#ifndef CPAL_I2C_TIMEOUT_Manager
	void CPAL_I2C_TIMEOUT_Manager(void);
#endif

/* Maximum Timeout values for each communication operation (preferably, Time base should be 1 Millisecond).
   The exact maximum value is the sum of event timeout value and the CPAL_I2C_TIMEOUT_MIN value defined below */
#define CPAL_I2C_TIMEOUT_SB             30
#define CPAL_I2C_TIMEOUT_ADDR           3
#define CPAL_I2C_TIMEOUT_ADD10          3
#define CPAL_I2C_TIMEOUT_TXE            2
#define CPAL_I2C_TIMEOUT_RXNE           2
#define CPAL_I2C_TIMEOUT_BTF            4
#define CPAL_I2C_TIMEOUT_BUSY           5

/* DO NOT MODIFY THESE VALUES ---------------------------------------------------------*/
#define CPAL_I2C_TIMEOUT_DEFAULT        ((uint32_t)0xFFFFFFFF)
#define CPAL_I2C_TIMEOUT_MIN            ((uint32_t)0x00000001)
#define CPAL_I2C_TIMEOUT_DETECTED       ((uint32_t)0x00000000)
 
/*-----------NVIC Group Priority-------------*/
#define CPAL_NVIC_PRIOGROUP	NVIC_PriorityGroup_0 /*!< 0 bits for preemption priority, 4 bits for subpriority */
/* #define CPAL_NVIC_PRIOGROUP	NVIC_PriorityGroup_1 */ /*!< 1 bits for preemption priority, 3 bits for subpriority */
/* #define CPAL_NVIC_PRIOGROUP		NVIC_PriorityGroup_2 */	/*!< 2 bits for preemption priority, 2 bits for subpriority */
/* #define CPAL_NVIC_PRIOGROUP	NVIC_PriorityGroup_3 */ /*!< 3 bits for preemption priority, 1 bits for subpriority */
/* #define CPAL_NVIC_PRIOGROUP	NVIC_PriorityGroup_4 */ /*!< 4 bits for preemption priority */
  
/*-----------------------------------------------------------------------------------------------------------------------*/

#define CPAL_I2C_SCL_GPIO_PORT		GPIOB
#define CPAL_I2C_SCL_GPIO_CLK		RCC_APB2Periph_GPIOB
#define CPAL_I2C_SCL_GPIO_PIN		GPIO_Pin_6
  
#define CPAL_I2C_SDA_GPIO_PORT		GPIOB
#define CPAL_I2C_SDA_GPIO_CLK		RCC_APB2Periph_GPIOB
#define CPAL_I2C_SDA_GPIO_PIN		GPIO_Pin_7
 

#define CPAL_I2C_DMA_TX_Channel     DMA1_Channel6
#define CPAL_I2C_DMA_RX_Channel     DMA1_Channel7
#define CPAL_I2C_DEVICE  			I2C1
	
#define CPAL_I2C_CLK				RCC_APB1Periph_I2C1
#define CPAL_I2C_DR					((uint32_t)&I2C1->DR)
  
#define CPAL_I2C_DMA				DMA1
#define CPAL_I2C_DMA_CLK			RCC_AHBPeriph_DMA1 
   
#define CPAL_I2C_IT_EVT_IRQn		I2C1_EV_IRQn
#define CPAL_I2C_IT_ERR_IRQn		I2C1_ER_IRQn   
#define I2Cx_ER_IRQHandler			I2C1_ER_IRQHandler
#define I2Cx_EV_IRQHandler			I2C1_EV_IRQHandler
  
#define CPAL_I2C_DMA_TX_IRQn		DMA1_Channel6_IRQn
#define CPAL_I2C_DMA_RX_IRQn		DMA1_Channel7_IRQn
  
#define CPAL_I2Cx_DMA_TX_IRQHandler	DMA1_Channel6_IRQHandler
#define CPAL_I2Cx_DMA_RX_IRQHandler	DMA1_Channel7_IRQHandler
  
#define CPAL_I2C_DMA_TX_TC_FLAG		DMA1_FLAG_TC6
#define CPAL_I2C_DMA_TX_HT_FLAG		DMA1_FLAG_HT6
#define CPAL_I2C_DMA_TX_TE_FLAG		DMA1_FLAG_TE6
  
#define CPAL_I2C_DMA_RX_TC_FLAG		DMA1_FLAG_TC7
#define CPAL_I2C_DMA_RX_HT_FLAG		DMA1_FLAG_HT7
#define CPAL_I2C_DMA_RX_TE_FLAG		DMA1_FLAG_TE7

/*-----------Interrupt Priority Offset-------------*/

/* This defines can be used to decrease the Level of Interrupt Priority for I2Cx Device (ERR, EVT, DMA_TX, DMA_RX).
   The value of I2Cx_IT_OFFSET_SUBPRIO is added to I2Cx_IT_XXX_SUBPRIO and the value of I2Cx_IT_OFFSET_PREPRIO 
   is added to I2Cx_IT_XXX_PREPRIO (XXX: ERR, EVT, DMATX, DMARX). 
   I2Cx Interrupt Priority are defined in cpal_i2c_hal_stm32f10x.h file in Section 3  */

#define I2C_IT_OFFSET_SUBPRIO		0      /* I2C1 SUB-PRIORITY Offset */ 
#define I2C_IT_OFFSET_PREPRIO		0      /* I2C1 PREEMPTION PRIORITY Offset */ 

#define I2C_IT_EVT_SUBPRIO			I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 EVT SUB-PRIORITY */ 
#define I2C_IT_EVT_PREPRIO			I2C_IT_OFFSET_PREPRIO + 2   /* I2C1 EVT PREEMPTION PRIORITY */

#define I2C_IT_ERR_SUBPRIO			I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 ERR SUB-PRIORITY */
#define I2C_IT_ERR_PREPRIO			I2C_IT_OFFSET_PREPRIO + 0   /* I2C1 ERR PREEMPTION PRIORITY */

#define I2C_IT_DMATX_SUBPRIO		I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 DMA TX SUB-PRIORITY */
#define I2C_IT_DMATX_PREPRIO		I2C_IT_OFFSET_PREPRIO + 1   /* I2C1 DMA TX PREEMPTION PRIORITY */

#define I2C_IT_DMARX_SUBPRIO		I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 DMA RX SUB-PRIORITY */
#define I2C_IT_DMARX_PREPRIO		I2C_IT_OFFSET_PREPRIO + 1   /* I2C1 DMA RX PREEMPTION PRIORITY */

#endif /* __CPAL_CONF_H */
