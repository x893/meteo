#ifndef __CPAL_H
#define __CPAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "cpal_conf.h"
#include "stm32f10x.h"
   
typedef enum
{
	CPAL_DIRECTION_TX	= 1,	/*!<Transmitter only direction */
	CPAL_DIRECTION_RX	= 2,	/*!<Receiver only direction */
	CPAL_DIRECTION_TXRX	= 3,	/*!<Transmitter and Receiver direction */
} CPAL_DirectionTypeDef;

typedef enum
{
	CPAL_I2C_ERR_NONE		= 0x0000,
	CPAL_I2C_ERR_TIMEOUT	= 0x00FF,
	CPAL_I2C_ERR_BERR		= 0x0100,
	CPAL_I2C_ERR_ARLO		= 0x0200,
	CPAL_I2C_ERR_AF			= 0x0400,
	CPAL_I2C_ERR_OVR		= 0x0800,             
} CPAL_I2CErrorTypeDef;

typedef enum
{
	CPAL_STATE_DISABLED = 0x00,
	CPAL_STATE_READY    = 0x01,
	CPAL_STATE_READY_TX = 0x03,
	CPAL_STATE_READY_RX = 0x05,
	CPAL_STATE_BUSY     = 0x02,
	CPAL_STATE_BUSY_TX  = 0x06,
	CPAL_STATE_BUSY_RX  = 0x0A,
	CPAL_STATE_ERROR    = 0x10,
} CPAL_StateTypeDef;

typedef struct 
{
	__IO	uint32_t	Timeout;
	const	uint8_t *	Buffer;
	__IO	uint16_t	Count;
			uint16_t	Address;
	__IO	CPAL_I2CErrorTypeDef	Error;
	__IO	CPAL_StateTypeDef		State;
} CPAL_InitTypeDef;

extern CPAL_InitTypeDef FM24_Device;

#define CPAL_PASS	0
#define CPAL_FAIL	1
#define pNULL		(void*)0

#ifndef CPAL_TIMEOUT_UserCallback
	uint32_t CPAL_TIMEOUT_UserCallback(void);
#endif

#ifdef __cplusplus
}
#endif

#endif /*__CPAL_H */
