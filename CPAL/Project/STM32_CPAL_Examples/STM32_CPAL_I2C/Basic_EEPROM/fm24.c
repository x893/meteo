#include "board.h"
#include "fm24.h"

#define DMA_CCR_EN					((uint16_t)0x0001)
#define FM24_I2C_STATUS_ERR_MASK	((uint16_t)0x0F00)
#define FM24_I2C_STATUS1_EVT_MASK	((uint16_t)0x00DF)

#define I2C_CLK_CMD(clk,cmd)			RCC_APB1PeriphClockCmd((clk),(cmd))
#define I2C_RCC_RESET(clk)				RCC_APB1PeriphResetCmd((clk),ENABLE);	\
										RCC_APB1PeriphResetCmd((clk),DISABLE)

#define I2C_GPIO_CLK_CMD(clk,cmd)		RCC_APB2PeriphClockCmd((clk),(cmd))
#define DMA_CLK_CMD(clk,cmd)			RCC_AHBPeriphClockCmd((clk),(cmd))

/*========= DMA =========*/

#define FM24_HAL_ENABLE_DMATX()			FM24_I2C_DMA_TX_Channel->CCR |= DMA_CCR_EN
#define FM24_HAL_ENABLE_DMARX()			FM24_I2C_DMA_RX_Channel->CCR |= DMA_CCR_EN
#define FM24_HAL_DISABLE_DMATX()		FM24_I2C_DMA_TX_Channel->CCR &= ~DMA_CCR_EN
#define FM24_HAL_DISABLE_DMARX()		FM24_I2C_DMA_RX_Channel->CCR &= ~DMA_CCR_EN

/* DMA interrupts enable/disable */  
#define FM24_I2C_ENABLE_DMATX_TCIT()	FM24_I2C_DMA_TX_Channel->CCR |= DMA_IT_TC
#define FM24_I2C_ENABLE_DMATX_TEIT()	FM24_I2C_DMA_TX_Channel->CCR |= DMA_IT_TE
#define FM24_I2C_ENABLE_DMARX_TCIT()	FM24_I2C_DMA_RX_Channel->CCR |= DMA_IT_TC
#define FM24_I2C_ENABLE_DMARX_TEIT()	FM24_I2C_DMA_RX_Channel->CCR |= DMA_IT_TE
  
/* DMA interrupts flag management */
#define FM24_HAL_GET_DMATX_TCIT()		(uint32_t)(FM24_I2C_DMA->ISR & FM24_I2C_DMA_TX_TC_FLAG)
#define FM24_HAL_GET_DMATX_TEIT()		(uint32_t)(FM24_I2C_DMA->ISR & FM24_I2C_DMA_TX_TE_FLAG)
#define FM24_HAL_GET_DMARX_TCIT()		(uint32_t)(FM24_I2C_DMA->ISR & FM24_I2C_DMA_RX_TC_FLAG)
#define FM24_HAL_GET_DMARX_TEIT()		(uint32_t)(FM24_I2C_DMA->ISR & FM24_I2C_DMA_RX_TE_FLAG)
#define FM24_HAL_CLEAR_DMATX_IT()		FM24_I2C_DMA->IFCR = (FM24_I2C_DMA_TX_TC_FLAG | FM24_I2C_DMA_TX_TE_FLAG)
#define FM24_HAL_CLEAR_DMARX_IT()		FM24_I2C_DMA->IFCR = (FM24_I2C_DMA_RX_TC_FLAG | FM24_I2C_DMA_RX_TE_FLAG)

/* Get DMA data counter */
#define FM24_HAL_DMATX_GET_CNDT()		(uint32_t)(FM24_I2C_DMA_TX_Channel->CNDTR)
#define FM24_HAL_DMARX_GET_CNDT()		(uint32_t)(FM24_I2C_DMA_RX_Channel->CNDTR) 

/* I2C enable/disable */
#define FM24_HAL_ENABLE_DEV()			i2c->CR1 |=  I2C_CR1_PE
#define FM24_HAL_DISABLE_DEV()			i2c->CR1 &= ~I2C_CR1_PE

/* I2C software reset */
#define FM24_HAL_SWRST()				i2c->CR1 |= I2C_CR1_SWRST; \
										i2c->CR1 &= ~I2C_CR1_SWRST

/* I2C interrupts enable/disable */
#define FM24_HAL_ENABLE_ERRIT()			i2c->CR2 |=  I2C_CR2_ITERREN
#define FM24_HAL_DISABLE_ERRIT()		i2c->CR2 &= ~I2C_CR2_ITERREN
#define FM24_HAL_ENABLE_EVTIT()			i2c->CR2 |=  I2C_CR2_ITEVTEN
#define FM24_HAL_DISABLE_EVTIT()		i2c->CR2 &= ~I2C_CR2_ITEVTEN

/* I2C misc configuration */
#define FM24_HAL_ENABLE_DMAREQ()		i2c->CR2 |= I2C_CR2_DMAEN
#define FM24_HAL_DISABLE_DMAREQ()		i2c->CR2 &= ~I2C_CR2_DMAEN

#define FM24_HAL_ENABLE_ACK()			i2c->CR1 |= I2C_CR1_ACK
#define FM24_HAL_DISABLE_ACK()			i2c->CR1 &= ~I2C_CR1_ACK

#define FM24_HAL_ENABLE_LAST()			i2c->CR2 |= I2C_CR2_LAST
#define FM24_HAL_DISABLE_LAST()			i2c->CR2 &= ~I2C_CR2_LAST

#define FM24_HAL_START()				i2c->CR1 |= I2C_CR1_START
#define FM24_HAL_STOP()					i2c->CR1 |= I2C_CR1_STOP 

/* I2C data management */
#define FM24_HAL_RECEIVE()				(uint8_t)(i2c->DR)
#define FM24_HAL_SEND(value)			i2c->DR = (uint8_t)((value))

/* I2C flags management */
#define FM24_HAL_GET_EVENT()			(uint16_t)(i2c->SR1 & FM24_I2C_STATUS1_EVT_MASK)
#define FM24_HAL_GET_ERROR()			(uint16_t)(i2c->SR1 & FM24_I2C_STATUS_ERR_MASK)
#define FM24_HAL_GET_SB()				(uint16_t)(i2c->SR1 & I2C_SR1_SB) 
#define FM24_HAL_GET_ADDR()				(uint16_t)(i2c->SR1 & I2C_SR1_ADDR) 
#define FM24_HAL_GET_ADD10()			(uint16_t)(i2c->SR1 & I2C_SR1_ADD10) 
#define FM24_HAL_GET_STOPF()			(uint16_t)(i2c->SR1 & I2C_SR1_STOPF) 
#define FM24_HAL_GET_BTF()				(uint16_t)(i2c->SR1 & I2C_SR1_BTF) 
#define FM24_HAL_GET_TXE()				(uint16_t)(i2c->SR1 & I2C_SR1_TXE) 
#define FM24_HAL_GET_RXNE()				(uint16_t)(i2c->SR1 & I2C_SR1_RXNE) 
#define FM24_HAL_GET_BUSY()				(uint16_t)(i2c->SR2 & I2C_SR2_BUSY) 
#define FM24_HAL_GET_GENCALL()			(uint16_t)(i2c->SR2 & I2C_SR2_GENCALL) 
#define FM24_HAL_GET_DUALF()			(uint16_t)(i2c->SR2 & I2C_SR2_DUALF) 
#define FM24_HAL_GET_TRA()				(uint16_t)(i2c->SR2 & I2C_SR2_TRA)  
#define FM24_HAL_GET_OVR()				(uint16_t)(i2c->SR1 & I2C_SR1_OVR) 
#define FM24_HAL_GET_AF()				(uint16_t)(i2c->SR1 & I2C_SR1_AF) 
#define FM24_HAL_GET_ARLO()				(uint16_t)(i2c->SR1 & I2C_SR1_ARLO) 
#define FM24_HAL_GET_BERR()				(uint16_t)(i2c->SR1 & I2C_SR1_BERR)   

#define FM24_HAL_CLEAR_SB()				i2c->SR1; i2c->SR2
#define FM24_HAL_CLEAR_ADDR()			i2c->SR1; i2c->SR2
#define FM24_HAL_CLEAR_AF()				i2c->SR1 = ~I2C_SR1_AF
#define FM24_HAL_CLEAR_ERROR()			i2c->SR1 = ~FM24_I2C_STATUS_ERR_MASK

#define __FM24_I2C_TIMEOUT_DETECT	(	(ds->Timeout == FM24_I2C_TIMEOUT_MIN) 		\
									||	(ds->Timeout == FM24_I2C_TIMEOUT_DEFAULT)	\
									)

#define __FM24_I2C_TIMEOUT(cmd, timeout)						\
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + (timeout);				\
	while (((cmd) == 0) && (!__FM24_I2C_TIMEOUT_DETECT));		\
	if (__FM24_I2C_TIMEOUT_DETECT)								\
		return FM24_I2C_Timeout();								\
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT

#define __FM24_I2C_TIMEOUT_VOID(cmd, timeout)					\
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + (timeout);				\
	while (((cmd) == 0) && (!__FM24_I2C_TIMEOUT_DETECT));		\
	if (__FM24_I2C_TIMEOUT_DETECT)								\
	{															\
		FM24_I2C_Timeout();										\
		return;													\
	}															\
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT

/*========= Local structures used in FM24_I2C_StructInit() function =========*/ 

const I2C_InitTypeDef FM24_I2C_InitData = {
	.I2C_ClockSpeed 	= 400000,
	.I2C_Mode			= I2C_Mode_I2C,
	.I2C_DutyCycle		= I2C_DutyCycle_2,
	.I2C_OwnAddress1	= 0,
	.I2C_Ack			= I2C_Ack_Enable,
	.I2C_AcknowledgedAddress	= I2C_AcknowledgedAddress_7bit
};

uint32_t FM24_I2C_Timeout (void);

DMA_InitTypeDef FM24_DMA_InitStructure;
FM24Context_t FM24_Device;

/**
  * @brief  Enable the DMA clock and initialize needed DMA Channels 
  *         used by the I2C device.
  * @param  Device : I2C Device instance.
  * @param  Direction : Transfer direction.
  * @param  Options :  Transfer Options.
  * @retval None. 
  */             
void FM24_I2C_HAL_DMAInit(void)
{  
	DMA_CLK_CMD(FM24_I2C_DMA_CLK, ENABLE);

	/* I2Cx Common Channel Configuration */
	FM24_DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	FM24_DMA_InitStructure.DMA_BufferSize = 0xFFFF;
	FM24_DMA_InitStructure.DMA_PeripheralInc =  DMA_PeripheralInc_Disable;
	FM24_DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	FM24_DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte ;
	FM24_DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	FM24_DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	FM24_DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	FM24_DMA_InitStructure.DMA_PeripheralBaseAddr = FM24_I2C_DR;

	FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_Init(FM24_I2C_DMA_TX_Channel, &FM24_DMA_InitStructure);

	FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_Init(FM24_I2C_DMA_RX_Channel, &FM24_DMA_InitStructure);
}

/**
  * @brief  Deinitialize the DMA channel used by I2C Device(configured to their default state).
  *         DMA clock is not disabled. 
  * @param  Device : I2C Device instance.
  * @param  Direction : Transfer direction.
  * @retval None.
  */
void FM24_I2C_HAL_DMADeInit(void)
{
	DMA_DeInit(FM24_I2C_DMA_TX_Channel);  
	DMA_DeInit(FM24_I2C_DMA_RX_Channel);  
}

/**
  * @brief  Configure NVIC and interrupts used by I2C Device according to 
  *         enabled options
  * @param  Device : I2C Device instance.
  * @param  Options : I2C Transfer Options.
  * @retval None. 
  */
void FM24_I2C_HAL_ITInit(void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;

	NVIC_InitTypeDef NVIC_InitStructure; 

	/* Configure NVIC priority Group */ 
	NVIC_PriorityGroupConfig (FM24_NVIC_PRIOGROUP);

	/* Enable the IRQ channel */
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	/* Configure NVIC for I2Cx EVT Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = FM24_I2C_IT_EVT_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_EVT_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_EVT_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure NVIC for I2Cx ERR Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = FM24_I2C_IT_ERR_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_ERR_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_ERR_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable I2C Error Interrupts */
	FM24_HAL_ENABLE_ERRIT();

	/* Configure NVIC for DMA TX channel interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = FM24_I2C_DMA_TX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_DMATX_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_DMATX_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable DMA TX Channel TCIT  */
	FM24_I2C_ENABLE_DMATX_TCIT();

	/* Enable DMA TX Channel TEIT  */    
	FM24_I2C_ENABLE_DMATX_TEIT(); 

	/* Configure NVIC for DMA RX channel interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = FM24_I2C_DMA_RX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_DMARX_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_DMARX_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable DMA RX Channel TCIT  */
	FM24_I2C_ENABLE_DMARX_TCIT();  

	/* Enable DMA RX Channel TEIT  */
	FM24_I2C_ENABLE_DMARX_TEIT(); 
}


/**
  * @brief  Deinitialize the IO pins used by the I2C device 
  *         (configured to their default state).
  * @param  Device : I2C Device instance. 
  * @retval None. 
  */
void FM24_I2C_HAL_GPIODeInit(void)
{      
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;

	GPIO_InitStructure.GPIO_Pin = FM24_I2C_SCL_GPIO_PIN;
	GPIO_Init(FM24_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = FM24_I2C_SDA_GPIO_PIN;
	GPIO_Init(FM24_I2C_SDA_GPIO_PORT, &GPIO_InitStructure); 
}

void DelayMicro(uint16_t us)
{
	register uint32_t count = (SystemCoreClock / 1000000UL) * us;
	while (count-- != 0)
			__NOP();
}

/**
  * @brief  Configure the IO pins used by the I2C device.
  * @param  Device : I2C Device instance. 
  * @retval None. 
  */
void FM24_I2C_HAL_GPIOInit(void)
{
	uint8_t retry_count_clk;
	uint8_t retry_count;

	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable I2Cx SCL and SDA Pin Clock */
	I2C_GPIO_CLK_CMD((FM24_I2C_SCL_GPIO_CLK | FM24_I2C_SDA_GPIO_CLK), ENABLE); 

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_SetBits(FM24_I2C_SCL_GPIO_PORT, FM24_I2C_SCL_GPIO_PIN);
	GPIO_SetBits(FM24_I2C_SDA_GPIO_PORT, FM24_I2C_SDA_GPIO_PIN);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;

	GPIO_InitStructure.GPIO_Pin = FM24_I2C_SCL_GPIO_PIN;
	GPIO_Init(FM24_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = FM24_I2C_SDA_GPIO_PIN;
	GPIO_Init(FM24_I2C_SDA_GPIO_PORT, &GPIO_InitStructure);

	retry_count_clk = 10;
	while (GPIO_ReadInputDataBit(FM24_I2C_SDA_GPIO_PORT, FM24_I2C_SDA_GPIO_PIN) == Bit_RESET
		&& retry_count_clk-- != 0) 
	{
		retry_count = 10;
		/* Set clock high and wait for any clock stretching to finish. */
		GPIO_SetBits(FM24_I2C_SCL_GPIO_PORT, FM24_I2C_SCL_GPIO_PIN);
		while (GPIO_ReadInputDataBit(FM24_I2C_SCL_GPIO_PORT, FM24_I2C_SCL_GPIO_PIN) == Bit_RESET && retry_count-- != 0)
			DelayMicro(1);
		DelayMicro(2);

		GPIO_ResetBits(FM24_I2C_SCL_GPIO_PORT, FM24_I2C_SCL_GPIO_PIN);
		DelayMicro(2);

		GPIO_SetBits(FM24_I2C_SCL_GPIO_PORT, FM24_I2C_SCL_GPIO_PIN);
		DelayMicro(2);
	}

	/* Generate a start then stop condition */
	GPIO_SetBits(FM24_I2C_SCL_GPIO_PORT, FM24_I2C_SCL_GPIO_PIN);
	DelayMicro(2);
	
	GPIO_ResetBits(FM24_I2C_SDA_GPIO_PORT, FM24_I2C_SDA_GPIO_PIN);
	DelayMicro(2);

	GPIO_SetBits(FM24_I2C_SDA_GPIO_PORT, FM24_I2C_SDA_GPIO_PIN);
	DelayMicro(2);

	retry_count = 10;
	while (GPIO_ReadInputDataBit(FM24_I2C_SCL_GPIO_PORT, FM24_I2C_SCL_GPIO_PIN) == Bit_RESET && retry_count-- != 0)
		DelayMicro(1);

	retry_count = 10;
	while (GPIO_ReadInputDataBit(FM24_I2C_SDA_GPIO_PORT, FM24_I2C_SDA_GPIO_PIN) == Bit_RESET && retry_count-- != 0)
		DelayMicro(1);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;

	GPIO_InitStructure.GPIO_Pin = FM24_I2C_SCL_GPIO_PIN;
	GPIO_Init(FM24_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = FM24_I2C_SDA_GPIO_PIN;
	GPIO_Init(FM24_I2C_SDA_GPIO_PORT, &GPIO_InitStructure);
}

/**
  * @brief  Reset then disable the I2C device clock.
  * @param  Device : I2C Device instance 
  * @retval None. 
  */
void FM24_I2C_HAL_CLKDeInit(void)
{   
	I2C_RCC_RESET(FM24_I2C_CLK);			/* Reset I2Cx device clock in order to avoid non-cleared error flags */
	I2C_CLK_CMD(FM24_I2C_CLK, DISABLE);	/* Disable I2Cx device clock */
}

/**
  * @brief	Initialize the peripheral and all related clocks, GPIOs, DMA and 
	*				 Interrupts according to the specified parameters in the 
	*				 FM24Context_t structure.
  * @param	pDevInitStruct : Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL 
  */
uint32_t FM24_I2C_Init(void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;
	register FM24Context_t *ds = &FM24_Device;

	/* If FM24_State is not BUSY */
	if (ds->State == FM24_STATE_READY
	||	ds->State == FM24_STATE_ERROR
	||	ds->State == FM24_STATE_DISABLED
		)
	{
		FM24_HAL_DISABLE_DEV();
		FM24_I2C_HAL_GPIODeInit();
		FM24_I2C_HAL_CLKDeInit();
		FM24_I2C_HAL_DMADeInit();

		I2C_RCC_RESET(FM24_I2C_CLK);
		I2C_CLK_CMD(FM24_I2C_CLK, ENABLE);

		FM24_I2C_HAL_GPIOInit();
		FM24_HAL_ENABLE_DEV();
		I2C_Init(FM24_I2C_DEVICE, (I2C_InitTypeDef *)&FM24_I2C_InitData);

		FM24_I2C_HAL_DMAInit();
		FM24_I2C_HAL_ITInit();
		ds->State = FM24_STATE_READY;

		FM24_TIMEOUT_INIT();

		return FM24_NO_ERROR;
	}		
	return FM24_FAIL; 
}


/**
  * @brief	Deinitialize the peripheral and all related clocks, GPIOs, DMA and NVIC 
	*				 to their reset values.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL
	* @note	 The Peripheral clock is disabled but the GPIO Ports clocks remains 
	*				 enabled after this deinitialization. 
  */
uint32_t FM24_I2C_DeInit(void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;
	register FM24Context_t *ds = &FM24_Device;

	/* If FM24_State is not BUSY */
	if ((ds->State == FM24_STATE_READY)
	||	(ds->State == FM24_STATE_ERROR)
	||	(ds->State == FM24_STATE_DISABLED)
		)
	{
		FM24_I2C_HAL_GPIODeInit();
		FM24_HAL_DISABLE_DEV();
		FM24_I2C_HAL_CLKDeInit();

		FM24_I2C_HAL_DMADeInit();	//	DMA Deinitialization : if DMA Programming model is selected

		// Deinitialize NVIC and interrupts used by I2C Device in
		NVIC_DisableIRQ(FM24_I2C_IT_EVT_IRQn);
		NVIC_DisableIRQ(FM24_I2C_IT_ERR_IRQn);
		NVIC_DisableIRQ(FM24_I2C_DMA_TX_IRQn);
		NVIC_DisableIRQ(FM24_I2C_DMA_RX_IRQn);
		
		ds->State	= FM24_STATE_DISABLED;
		ds->Error	= FM24_NO_ERROR;
		ds->Timeout	= FM24_I2C_TIMEOUT_DEFAULT;

		FM24_TIMEOUT_DEINIT();	//	Deinitialize Timeout Procedure
		
		return FM24_NO_ERROR;
	}
	return FM24_FAIL; 
}

/**
  * @brief	Initialize the peripheral structure with default values according
	*			to the specified parameters in the FM24_I2CDevTypeDef structure.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval
  */
void FM24_I2C_StructInit(void)
{	 
	register FM24Context_t *ds = &FM24_Device;
	ds->Buffer			= (void*)0;
	ds->State			= FM24_STATE_DISABLED;
	ds->Error			= FM24_NO_ERROR;
	ds->Timeout			= FM24_I2C_TIMEOUT_DEFAULT;
}

/**
  * @brief	Allows to send a data or a buffer of data through the peripheral to 
	*			 a selected device in a selected location address.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval	NO_ERROR or FM24_FAIL.
  */
uint32_t FM24_I2C_Write(const uint8_t* src, uint16_t address, uint32_t count)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;
	register FM24Context_t *ds = &FM24_Device;

	if ((ds->State & FM24_STATE_BUSY) != 0
	||	ds->State == FM24_STATE_READY_TX 
	||	ds->State == FM24_STATE_READY_RX
	||	ds->State == FM24_STATE_DISABLED	/* If FM24_State is FM24_STATE_DISABLED (device is not initialized) Exit Write function */	
	||	ds->State == FM24_STATE_ERROR		/* If FM24_State is FM24_STATE_ERROR (Error occurred ) */
		)
		return FM24_FAIL;

	ds->Count	= count;
	ds->Buffer	= src;
	ds->Address	= address;

	ds->State = FM24_STATE_BUSY;

	/* Wait until Busy flag is reset */ 
	__FM24_I2C_TIMEOUT( !(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY );

	FM24_HAL_START();
	ds->State = FM24_STATE_READY_TX;
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_SB;

	FM24_HAL_ENABLE_EVTIT();	/* Enable EVENT Interrupts*/

	 return FM24_NO_ERROR;
}

/**
  * @brief	Allows to receive a data or a buffer of data through the peripheral 
	*				 from a selected device in a selected location address.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL. 
  */
uint32_t FM24_I2C_Read(uint8_t* pBuffer, uint16_t ReadAddr, uint32_t NumByteToRead)
{
	register I2C_TypeDef * i2c		= FM24_I2C_DEVICE;
	register FM24Context_t * ds	= &FM24_Device;

	if ((ds->State & FM24_STATE_BUSY) != 0
	||	ds->State == FM24_STATE_READY_TX
	||	ds->State == FM24_STATE_READY_RX
	||	ds->State == FM24_STATE_DISABLED	/* If FM24_State is FM24_STATE_DISABLED (device is not initialized) Exit Read function */
	||	ds->State == FM24_STATE_ERROR	/* If FM24_State is FM24_STATE_ERROR (Error occurred ) */
		)
		return FM24_FAIL;
	
   	ds->Count = NumByteToRead;
	ds->Buffer = pBuffer;

	/* Update FM24_State to FM24_STATE_BUSY */
	ds->State = FM24_STATE_BUSY;
 
	/* Wait until Busy flag is reset */ 
	__FM24_I2C_TIMEOUT(!(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY);

	FM24_HAL_START();
	__FM24_I2C_TIMEOUT(FM24_HAL_GET_SB(), FM24_I2C_TIMEOUT_SB);

	FM24_HAL_SEND(FM24_ADDRESS_WRITE);
	__FM24_I2C_TIMEOUT(FM24_HAL_GET_ADDR(), FM24_I2C_TIMEOUT_ADDR);

	FM24_HAL_CLEAR_ADDR(); /* Clear ADDR flag: (Read SR1 followed by read of SR2), SR1 read operation is already done */

	__FM24_I2C_TIMEOUT(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);
	FM24_HAL_SEND( ReadAddr >> 8);

	__FM24_I2C_TIMEOUT(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);
	FM24_HAL_SEND( ReadAddr );

	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_TXE;
	__FM24_I2C_TIMEOUT(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);

	/* Update FM24_State to FM24_STATE_READY_RX */
	ds->State = FM24_STATE_READY_RX;
	FM24_HAL_START();

	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_SB;

	FM24_HAL_ENABLE_EVTIT();

	return FM24_NO_ERROR;
}

/**
  * @brief	Wait until target device is ready for communication (This function is 
	*				 used with Memory devices).					 
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval	NO_ERROR or FM24_FAIL. 
  */
uint32_t FM24_I2C_IsDeviceReady(void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;
	register __IO uint32_t Timeout = 0xFFFF;
	register FM24Context_t * ds	= &FM24_Device;

	ds->State = FM24_STATE_BUSY;

	FM24_HAL_DISABLE_ERRIT();
	FM24_HAL_DISABLE_DEV();
	FM24_HAL_ENABLE_DEV();

	FM24_HAL_START();
	__FM24_I2C_TIMEOUT(FM24_HAL_GET_SB(), FM24_I2C_TIMEOUT_SB);

	/* Send Slave address with bit0 reset for write */

	FM24_HAL_SEND(FM24_ADDRESS_WRITE);
	while (FM24_HAL_GET_ADDR() == 0 && Timeout != 0)
		--Timeout;

	FM24_HAL_CLEAR_AF();
	FM24_HAL_STOP();

	while (FM24_HAL_GET_BUSY());

	FM24_HAL_DISABLE_DEV();
	FM24_HAL_ENABLE_DEV();

	FM24_HAL_ENABLE_ACK();
	FM24_HAL_ENABLE_ERRIT();

	ds->State = FM24_STATE_READY;

	return (Timeout == 0)  ? FM24_FAIL : FM24_NO_ERROR;
}

/**
  * @brief	Handles Master Start condition (SB) interrupt event.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval	NO_ERROR or FM24_FAIL. 
  */
void I2C_MASTER_START_Handle(register I2C_TypeDef * i2c)
{
	register FM24Context_t * ds	= &FM24_Device;

	if (ds->State == FM24_STATE_READY_RX)
	{
		FM24_HAL_SEND(FM24_ADDRESS_READ);
		ds->State = FM24_STATE_BUSY_RX;
	}
	else
	{
		FM24_HAL_SEND(FM24_ADDRESS_WRITE);
		ds->State = FM24_STATE_BUSY_TX;
	}
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_ADDR;
}

/**
  * @brief	This function Configure I2C DMA and Interrupts before starting transfer phase.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @param	Direction : Transfer direction.
  * @retval	NO_ERROR or FM24_FAIL. 
  */
void FM24_I2C_Enable_DMA_IT (void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;
	register FM24Context_t * ds	= &FM24_Device;

	FM24_HAL_DISABLE_EVTIT();
	FM24_HAL_ENABLE_DMAREQ();
	
	FM24_DMA_InitStructure.DMA_MemoryBaseAddr	= (uint32_t)(ds->Buffer);
	FM24_DMA_InitStructure.DMA_BufferSize		= ds->Count;

	if (ds->State == FM24_STATE_BUSY_TX)
	{
		/* Configure TX DMA Channels */
		FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
		DMA_Init(FM24_I2C_DMA_TX_Channel, &FM24_DMA_InitStructure);

		FM24_HAL_DISABLE_LAST(); 	/* Disable DMA automatic NACK generation */
		FM24_HAL_ENABLE_DMATX();
	}		
	else if (ds->State == FM24_STATE_BUSY_RX)
	{
		/* Configure RX DMA Channels */
		FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
		DMA_Init(FM24_I2C_DMA_RX_Channel, &FM24_DMA_InitStructure);

		FM24_HAL_ENABLE_LAST();		/* Enable DMA automatic NACK generation */
		FM24_HAL_ENABLE_DMARX();
	}
	else
		return;
}

/**
  * @brief	Handles Master address matched (ADDR) interrupt event. 
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL. 
  */
void I2C_MASTER_ADDR_Handle(register I2C_TypeDef * i2c)
{
	register FM24Context_t * ds	= &FM24_Device;

	if (ds->State == FM24_STATE_BUSY_TX)
		ds->Timeout = FM24_I2C_TIMEOUT_MIN + ds->Count;	/* Set 1ms timeout for each data transfer in case of DMA Tx mode */
	else if (ds->State == FM24_STATE_BUSY_RX)
		ds->Timeout = FM24_I2C_TIMEOUT_MIN + ds->Count;	/* Set 1ms timeout for each data transfer in case of DMA Rx mode */
	else
		ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;			/* Reinitialize Timeout Value to default (no timeout initiated) */

	FM24_HAL_CLEAR_ADDR();

	if (ds->State == FM24_STATE_BUSY_TX)
	{
		FM24_HAL_SEND((ds->Address & 0xFF00) >> 8);
		__FM24_I2C_TIMEOUT_VOID(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);

		FM24_HAL_SEND(ds->Address & 0x00FF);
		__FM24_I2C_TIMEOUT_VOID(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);
	}

	FM24_I2C_Enable_DMA_IT();
}

/**
  * @brief  This function handles I2C1 interrupt request.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void I2Cx_EV_IRQHandler(void)
{  
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;
	register __IO uint16_t I2CFlagStatus;

	/* Read I2C1 Status Registers 1 and 2 */
	I2CFlagStatus = FM24_HAL_GET_EVENT();

	if ((I2CFlagStatus & I2C_SR1_SB ) != 0)
	{
		I2C_MASTER_START_Handle(i2c);
	}
	else
	if ((I2CFlagStatus & I2C_SR1_ADDR ) != 0)
	{
		I2C_MASTER_ADDR_Handle(i2c);
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void FM24_ReInit(void)
{
	FM24_I2C_DeInit();
	FM24_I2C_StructInit();
	FM24_I2C_Init();
}

/**
  * @brief  This function handles I2C1 Errors interrupt.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void I2Cx_ER_IRQHandler(void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;
	register FM24Context_t * ds	= &FM24_Device;

	ds->Error = (FM24Error_t)FM24_HAL_GET_ERROR();
	ds->State = FM24_STATE_ERROR;
	FM24_HAL_CLEAR_ERROR();

	/* If Bus error occurred ---------------------------------------------------*/
	if ((ds->Error & FM24_I2C_ERR_BERR) != 0
	||	(ds->Error & FM24_I2C_ERR_ARLO) != 0
		)
	{
		FM24_HAL_SWRST();
	}

	ds->Timeout  = FM24_I2C_TIMEOUT_DEFAULT;
	FM24_ReInit();
}

/**
  * @brief  This function handles I2C1 TX DMA interrupt request.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void FM24_I2Cx_DMA_TX_IRQHandler(void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;	
	register FM24Context_t * ds	= &FM24_Device;

	/* Reinitialize Timeout Value to default (no timeout initiated) */
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;

	if ((FM24_HAL_GET_DMATX_TCIT()) != 0)
	{
		ds->Count = 0;

		FM24_HAL_DISABLE_DMAREQ();
		
		/* Wait until BTF flag is set */ 
		__FM24_I2C_TIMEOUT_VOID(FM24_HAL_GET_BTF(), FM24_I2C_TIMEOUT_BTF);
		FM24_HAL_STOP();
		__FM24_I2C_TIMEOUT_VOID(!(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY);

		FM24_HAL_DISABLE_DMATX();		/* Disable DMA Channel */
		FM24_HAL_DISABLE_EVTIT();	 	/* Disable EVENT Interrupt */

		ds->State = FM24_STATE_READY; 	/* Update FM24_State to FM24_STATE_READY */
	}
	else if ((FM24_HAL_GET_DMATX_TEIT()) != 0)
	{
		ds->State = FM24_STATE_ERROR;
		ds->Count = FM24_HAL_DMATX_GET_CNDT();
	}

	FM24_HAL_CLEAR_DMATX_IT();
}

/**
  * @brief  This function handles I2C1 RX DMA interrupt request.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void FM24_I2Cx_DMA_RX_IRQHandler(void)
{
	register I2C_TypeDef * i2c = FM24_I2C_DEVICE;	
	register FM24Context_t * ds	= &FM24_Device;

	/* Reinitialize Timeout Value to default (no timeout initiated) */
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT; 
	
	if ((FM24_HAL_GET_DMARX_TCIT()) != 0)
	{	 
		ds->Count = 0;

		FM24_HAL_STOP();				/* Generate Stop Condition */
		FM24_HAL_DISABLE_DMAREQ();	/* Disable DMA Request and Channel */
		
		__FM24_I2C_TIMEOUT_VOID(!(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY);
		
		FM24_HAL_DISABLE_DMARX();		/* Disable DMA Channel */
		FM24_HAL_DISABLE_EVTIT();		/* Disable EVENT Interrupt */
		FM24_HAL_DISABLE_LAST();		/* Disable DMA automatic NACK generation */

		ds->State = FM24_STATE_READY;
	}
	else if ((FM24_HAL_GET_DMARX_TEIT()) != 0)
	{	 
		ds->State = FM24_STATE_ERROR; 	/* Update FM24_State to FM24_STATE_ERROR */
		ds->Count = FM24_HAL_DMARX_GET_CNDT();
	}
	
	FM24_HAL_CLEAR_DMARX_IT();
}

/**
  * @brief	This function Manages I2C Timeouts when waiting for specific events.
  * @param	None
  * @retval	NO_ERROR or FM24_FAIL. 
  */
void FM24_I2C_TIMEOUT_Manager(void)
{
	register FM24Context_t * ds	= &FM24_Device;

	/* If Timeout occurred  */
	if (ds->Timeout == FM24_I2C_TIMEOUT_DETECTED)
	{
		ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;		/* Reinitialize Timeout Value */
		ds->State = FM24_STATE_ERROR;				/* update FM24_State to FM24_STATE_ERROR */

		/* In case of Device Error Timeout_Callback should not be called */
		if (ds->Error == FM24_NO_ERROR)
		{
			ds->Error = FM24_I2C_ERR_TIMEOUT;
			FM24_ReInit();
		}
	}
	/* If Timeout is triggered (Timeout != FM24_I2C_TIMEOUT_DEFAULT)*/
	else if (ds->Timeout != FM24_I2C_TIMEOUT_DEFAULT)
	{
		ds->Timeout--;	/* Decrement the timeout value */
	}
}

/**
  * @brief	This function Manages I2C Timeouts when Timeout occurred.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval	NO_ERROR or FM24_FAIL. 
  */
uint32_t FM24_I2C_Timeout (void)
{
	register FM24Context_t * ds	= &FM24_Device;

	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;
	ds->State = FM24_STATE_ERROR;
	ds->Error = FM24_I2C_ERR_TIMEOUT;

	FM24_ReInit();
	return FM24_NO_ERROR;
}

/**
  * @brief
  * @param
  * @retval
  */
FM24State_t FM24_I2C_GetState(void)
{
	return FM24_Device.State;
}

/**
  * @brief
  * @param
  * @retval
  */
FM24Error_t FM24_I2C_GetError(void)
{
	return FM24_Device.Error;
}
