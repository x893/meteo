#ifndef __DS18B20_H__
#define __DS18B20_H__

#include "stm32f10x.h"
#include <stdbool.h>

#define DS18B20_UART		UART5
#define DS18B20_IRQn		UART5_IRQn
#define DS18B20_IRQHandler	UART5_IRQHandler
#define DS18B20_CLK(cmd)	\
	do {				\
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, cmd);	\
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, cmd);	\
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, cmd);	\
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, cmd);	\
	} while (0)
#define DS18B20_PIN_INIT()	\
	do {	\
	GPIO_InitTypeDef GPIO_InitStruct;	\
	GPIO_SetBits(GPIOC, GPIO_Pin_12);					\
	GPIO_SetBits(GPIOD, GPIO_Pin_2);					\
	\
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_OD;		\
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		\
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;				\
	GPIO_Init(GPIOC, &GPIO_InitStruct);					\
	\
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;				\
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;	\
	GPIO_Init(GPIOD, &GPIO_InitStruct);					\
	} while (0)

#define DS18B20_PIN_POWER()	\
	do {	\
	GPIO_InitTypeDef GPIO_InitStruct;	\
	\
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;		\
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;		\
	\
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;				\
	GPIO_Init(GPIOC, &GPIO_InitStruct);					\
	\
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;				\
	GPIO_Init(GPIOD, &GPIO_InitStruct);					\
	} while (0)

//#define DS18B20_GIVE_TICK_RTOS

#define DS18B20_NO_RESET	0
#define DS18B20_RESET		1

#define DS18B20_OK			0
#define DS18B20_ERROR		1
#define DS18B20_NO_DEVICE	2

#define DS18B20_NO_READ		0xFF
#define DS18B20_READ_SLOT	0xFF

uint8_t DS18B20_Init(void);
uint8_t DS18B20_Reset(void);
uint8_t DS18B20_Send(const uint8_t *command, uint8_t cLen, uint8_t *data, uint8_t dLen, uint8_t readStart);
uint8_t DS18B20_Scan(uint8_t *buf, uint8_t num);
void DS18B20_PowerOff(void);
float DS18B20_Celcius(uint8_t * data);

#endif /* __DS18B20_H__ */
