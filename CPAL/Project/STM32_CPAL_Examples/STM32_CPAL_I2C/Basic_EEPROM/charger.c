#include "charger.h"

#define CHARGER_CLOCK		24000000UL

#define CHARGER_MIN_FREQ	90
#define CHARGER_MAX_FREQ	120

void ChargerToGpio(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/*
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;
	GPIO_Init(GPIOE, &GPIO_InitStruct);
	*/
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	TIM_Cmd(TIM1, DISABLE);
}

void ChargerToTimer(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/*
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;
	GPIO_Init(GPIOE, &GPIO_InitStruct);
	*/
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void ChargerOn(void)
{
	// GPIO_SetBits(GPIOE, GPIO_Pin_9);
	GPIO_SetBits(GPIOA, GPIO_Pin_8);
	ChargerToGpio();
}

void ChargerOff(void)
{
	// GPIO_ResetBits(GPIOE, GPIO_Pin_9);
	GPIO_ResetBits(GPIOA, GPIO_Pin_8);
	ChargerToGpio();
}

// GPIO clocks must be already enabled
void ChargerDeInit(void)
{
	ChargerOff();
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_TIM1, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_TIM1, DISABLE);  
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, DISABLE);
}

ERROR_t ChargerInit(uint16_t freqKHz)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	// RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);

	// Remap TIM1_CH1 to PE9 (PA8 w/o remap)
	// GPIO_PinRemapConfig(GPIO_FullRemap_TIM1, ENABLE);

	ChargerDeInit();

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

    /*	The TIM1CLK frequency is set to SystemCoreClock (72 MHz), to get TIM1 counter
		clock at 1 MHz the prescaler is computed as following:
		prescaler = (TIM1CLK / TIM1 counter clock) - 1
	*/
	if (freqKHz < CHARGER_MIN_FREQ)
		return CHARGE_BAD_FREQ;
	else if (freqKHz > CHARGER_MAX_FREQ)
		return CHARGE_BAD_FREQ;

	TIM_TimeBaseStructure.TIM_Period = (CHARGER_CLOCK / 1000UL) / freqKHz;
	TIM_TimeBaseStructure.TIM_Prescaler = (SystemCoreClock / CHARGER_CLOCK) - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	return NO_ERROR;
}

/**
  * @brief  Set duty
  * @param  from 0 (off) to 100 (on)
  * @retval
  */
void ChargerDuty(uint16_t dutyPercent)
{
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	if (dutyPercent >= 100)
	{
		ChargerOn();
	}
	else if (dutyPercent > 0)
	{
		TIM_Cmd(TIM1, DISABLE);

		ChargerToTimer();

		TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
		TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
		TIM_OCInitStructure.TIM_Pulse = TIM1->ARR * dutyPercent / 100;
		TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
		TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCPolarity_High;
		TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
		TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
		
		TIM_OC1Init(TIM1, &TIM_OCInitStructure);

		TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
		TIM_ARRPreloadConfig(TIM1, ENABLE);

		TIM_Cmd(TIM1, ENABLE);
		TIM_CtrlPWMOutputs(TIM1, ENABLE);
	}
	else
	{
		ChargerOff();
	}
}
