#include "cpal_i2c.h"

#define __CPAL_I2C_TIMEOUT_DETECT	(	(ds->Timeout == CPAL_I2C_TIMEOUT_MIN) 		\
									||	(ds->Timeout == CPAL_I2C_TIMEOUT_DEFAULT)	\
									)

#define __CPAL_I2C_TIMEOUT(cmd, timeout)						\
	ds->Timeout = CPAL_I2C_TIMEOUT_MIN + (timeout);				\
	while (((cmd) == 0) && (!__CPAL_I2C_TIMEOUT_DETECT));		\
	if (__CPAL_I2C_TIMEOUT_DETECT)								\
		return CPAL_I2C_Timeout();								\
	ds->Timeout = CPAL_I2C_TIMEOUT_DEFAULT

#define __CPAL_I2C_TIMEOUT_VOID(cmd, timeout)					\
	ds->Timeout = CPAL_I2C_TIMEOUT_MIN + (timeout);				\
	while (((cmd) == 0) && (!__CPAL_I2C_TIMEOUT_DETECT));		\
	if (__CPAL_I2C_TIMEOUT_DETECT)								\
	{															\
		CPAL_I2C_Timeout();										\
		return;													\
	}															\
	ds->Timeout = CPAL_I2C_TIMEOUT_DEFAULT

/*========= Local structures used in CPAL_I2C_StructInit() function =========*/ 

const I2C_InitTypeDef FM24_I2C_InitData = {
	.I2C_ClockSpeed 	= 300000,
	.I2C_Mode			= I2C_Mode_I2C,
	.I2C_DutyCycle		= I2C_DutyCycle_2,
	.I2C_OwnAddress1	= 0,
	.I2C_Ack			= I2C_Ack_Enable,
	.I2C_AcknowledgedAddress	= I2C_AcknowledgedAddress_7bit
};

void I2C_MASTER_START_Handle(register I2C_TypeDef * i2c);
void I2C_MASTER_ADDR_Handle(register I2C_TypeDef * i2c);
uint32_t CPAL_I2C_Timeout (void);

DMA_InitTypeDef CPAL_DMA_InitStructure;
CPAL_InitTypeDef FM24_Device;

/**
  * @brief  Enable the DMA clock and initialize needed DMA Channels 
  *         used by the I2C device.
  * @param  Device : I2C Device instance.
  * @param  Direction : Transfer direction.
  * @param  Options :  Transfer Options.
  * @retval None. 
  */             
void CPAL_I2C_HAL_DMAInit(void)
{  
	/* Enable I2Cx DMA */
	__DMA_CLK_CMD(CPAL_I2C_DMA_CLK, ENABLE);

	/* I2Cx Common Channel Configuration */
	CPAL_DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	CPAL_DMA_InitStructure.DMA_BufferSize = 0xFFFF;
	CPAL_DMA_InitStructure.DMA_PeripheralInc =  DMA_PeripheralInc_Disable;
	CPAL_DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	CPAL_DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte ;
	CPAL_DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	CPAL_DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	CPAL_DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	CPAL_DMA_InitStructure.DMA_PeripheralBaseAddr = CPAL_I2C_DR;

	CPAL_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_Init(CPAL_I2C_DMA_TX_Channel, &CPAL_DMA_InitStructure);

	CPAL_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_Init(CPAL_I2C_DMA_RX_Channel, &CPAL_DMA_InitStructure);
}

/**
  * @brief  Deinitialize the DMA channel used by I2C Device(configured to their default state).
  *         DMA clock is not disabled. 
  * @param  Device : I2C Device instance.
  * @param  Direction : Transfer direction.
  * @retval None.
  */
void CPAL_I2C_HAL_DMADeInit(void)
{
	DMA_DeInit(CPAL_I2C_DMA_TX_Channel);  
	DMA_DeInit(CPAL_I2C_DMA_RX_Channel);  
}

/**
	* @brief	Initialize the peripheral and all related clocks, GPIOs, DMA and 
	*				 Interrupts according to the specified parameters in the 
	*				 CPAL_InitTypeDef structure.
	* @param	pDevInitStruct : Pointer to the peripheral configuration structure.
	* @retval CPAL_PASS or CPAL_FAIL 
	*/
uint32_t CPAL_I2C_Init(void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;
	register CPAL_InitTypeDef *ds = &FM24_Device;

	/* If CPAL_State is not BUSY */
	if (ds->State == CPAL_STATE_READY
	||	ds->State == CPAL_STATE_ERROR
	||	ds->State == CPAL_STATE_DISABLED
		)
	{
		__CPAL_I2C_HAL_DISABLE_DEV();
		CPAL_I2C_HAL_GPIODeInit();
		CPAL_I2C_HAL_CLKDeInit();
		CPAL_I2C_HAL_DMADeInit();

		/*----------------------------------------------------------------------------
		Peripheral Clock Initialization
		---------------------------------------------------------------------------*/	 
		CPAL_I2C_HAL_CLKInit();
		CPAL_I2C_HAL_GPIOInit();
		__CPAL_I2C_HAL_ENABLE_DEV();
		I2C_Init(CPAL_I2C_DEVICE, (I2C_InitTypeDef *)&FM24_I2C_InitData);

		CPAL_I2C_HAL_DMAInit();
		CPAL_I2C_HAL_ITInit();
		ds->State = CPAL_STATE_READY;

		__CPAL_TIMEOUT_INIT();

		return CPAL_PASS;
	}		
	return CPAL_FAIL; 
}


/**
	* @brief	Deinitialize the peripheral and all related clocks, GPIOs, DMA and NVIC 
	*				 to their reset values.
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval CPAL_PASS or CPAL_FAIL
	* @note	 The Peripheral clock is disabled but the GPIO Ports clocks remains 
	*				 enabled after this deinitialization. 
	*/
uint32_t CPAL_I2C_DeInit(void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;
	register CPAL_InitTypeDef *ds = &FM24_Device;

	/* If CPAL_State is not BUSY */
	if ((ds->State == CPAL_STATE_READY) 
	||	(ds->State == CPAL_STATE_ERROR) 
	||	(ds->State == CPAL_STATE_DISABLED)
		)
	{
		CPAL_I2C_HAL_GPIODeInit();		/* Deinitialize I2Cx GPIO */
		__CPAL_I2C_HAL_DISABLE_DEV();	/* Disable I2Cx Device */
		CPAL_I2C_HAL_CLKDeInit();		/* Deinitialize I2Cx Clock */

		CPAL_I2C_HAL_DMADeInit();		//	DMA Deinitialization : if DMA Programming model is selected
		CPAL_I2C_HAL_ITDeInit();		//	Interrupts Deinitialization
		
		ds->State	= CPAL_STATE_DISABLED;
		ds->Error	= CPAL_I2C_ERR_NONE;
		ds->Timeout	= CPAL_I2C_TIMEOUT_DEFAULT;

		_CPAL_TIMEOUT_DEINIT();	//	Deinitialize Timeout Procedure
		
		return CPAL_PASS;
	}
	return CPAL_FAIL; 
}

/**
	* @brief	Initialize the peripheral structure with default values according
	*			to the specified parameters in the CPAL_I2CDevTypeDef structure.
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval CPAL_PASS or CPAL_FAIL. 
	*/
uint32_t CPAL_I2C_StructInit(void)
{	 
	register CPAL_InitTypeDef *ds = &FM24_Device;

	/* Initialize pDevInitStruct parameter to their default values */
	ds->Buffer			= pNULL;				/* Point to data */
	ds->State			= CPAL_STATE_DISABLED;	/* Device Disabled */
	ds->Error			= CPAL_I2C_ERR_NONE;	/* No Device Error */
	ds->Timeout			= CPAL_I2C_TIMEOUT_DEFAULT;	/* Set timeout value to CPAL_I2C_TIMEOUT_DEFAULT */

	return CPAL_PASS;
}

/**
	* @brief	Allows to send a data or a buffer of data through the peripheral to 
	*			 a selected device in a selected location address.
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval	CPAL_PASS or CPAL_FAIL.
	*/
uint32_t CPAL_I2C_Write(const uint8_t* src, uint16_t address, uint32_t count)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;
	register CPAL_InitTypeDef *ds = &FM24_Device;

	if ((ds->State & CPAL_STATE_BUSY) != 0
	||	ds->State == CPAL_STATE_READY_TX 
	||	ds->State == CPAL_STATE_READY_RX
	||	ds->State == CPAL_STATE_DISABLED	/* If CPAL_State is CPAL_STATE_DISABLED (device is not initialized) Exit Write function */	
	||	ds->State == CPAL_STATE_ERROR		/* If CPAL_State is CPAL_STATE_ERROR (Error occurred ) */
		)
		return CPAL_FAIL;

	ds->Count	= count;
	ds->Buffer	= src;
	ds->Address	= address;

	ds->State = CPAL_STATE_BUSY;

	/* Wait until Busy flag is reset */ 
	__CPAL_I2C_TIMEOUT( !(__CPAL_I2C_HAL_GET_BUSY()), CPAL_I2C_TIMEOUT_BUSY );

	__CPAL_I2C_HAL_START();
	ds->State = CPAL_STATE_READY_TX;
	ds->Timeout = CPAL_I2C_TIMEOUT_MIN + CPAL_I2C_TIMEOUT_SB;

	__CPAL_I2C_HAL_ENABLE_EVTIT();	/* Enable EVENT Interrupts*/

	 return CPAL_PASS;
}

/**
	* @brief	Allows to receive a data or a buffer of data through the peripheral 
	*				 from a selected device in a selected location address.
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval CPAL_PASS or CPAL_FAIL. 
	*/
uint32_t CPAL_I2C_Read(uint8_t* pBuffer, uint16_t ReadAddr, uint32_t NumByteToRead)
{
	register I2C_TypeDef * i2c		= CPAL_I2C_DEVICE;
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	if ((ds->State & CPAL_STATE_BUSY) != 0
	||	ds->State == CPAL_STATE_READY_TX
	||	ds->State == CPAL_STATE_READY_RX
	||	ds->State == CPAL_STATE_DISABLED	/* If CPAL_State is CPAL_STATE_DISABLED (device is not initialized) Exit Read function */
	||	ds->State == CPAL_STATE_ERROR	/* If CPAL_State is CPAL_STATE_ERROR (Error occurred ) */
		)
		return CPAL_FAIL;
	
   	ds->Count = NumByteToRead;
	ds->Buffer = pBuffer;

	/* Update CPAL_State to CPAL_STATE_BUSY */
	ds->State = CPAL_STATE_BUSY;
 
	/* Wait until Busy flag is reset */ 
	__CPAL_I2C_TIMEOUT(!(__CPAL_I2C_HAL_GET_BUSY()), CPAL_I2C_TIMEOUT_BUSY);

	__CPAL_I2C_HAL_START();
	__CPAL_I2C_TIMEOUT(__CPAL_I2C_HAL_GET_SB(), CPAL_I2C_TIMEOUT_SB);

	__CPAL_I2C_HAL_SEND(FM24_ADDRESS_WRITE);
	__CPAL_I2C_TIMEOUT(__CPAL_I2C_HAL_GET_ADDR(), CPAL_I2C_TIMEOUT_ADDR);

	__CPAL_I2C_HAL_CLEAR_ADDR(); /* Clear ADDR flag: (Read SR1 followed by read of SR2), SR1 read operation is already done */

	__CPAL_I2C_TIMEOUT(__CPAL_I2C_HAL_GET_TXE(), CPAL_I2C_TIMEOUT_TXE);
	__CPAL_I2C_HAL_SEND( ReadAddr >> 8);

	__CPAL_I2C_TIMEOUT(__CPAL_I2C_HAL_GET_TXE(), CPAL_I2C_TIMEOUT_TXE);
	__CPAL_I2C_HAL_SEND( ReadAddr );

	ds->Timeout = CPAL_I2C_TIMEOUT_MIN + CPAL_I2C_TIMEOUT_TXE;
	__CPAL_I2C_TIMEOUT(__CPAL_I2C_HAL_GET_TXE(), CPAL_I2C_TIMEOUT_TXE);

	/* Update CPAL_State to CPAL_STATE_READY_RX */
	ds->State = CPAL_STATE_READY_RX;
	__CPAL_I2C_HAL_START();

	ds->Timeout = CPAL_I2C_TIMEOUT_MIN + CPAL_I2C_TIMEOUT_SB;

	__CPAL_I2C_HAL_ENABLE_EVTIT();

	return CPAL_PASS;
}

/**
	* @brief	Wait until target device is ready for communication (This function is 
	*				 used with Memory devices).					 
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval	CPAL_PASS or CPAL_FAIL. 
	*/
uint32_t CPAL_I2C_IsDeviceReady(void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;
	register __IO uint32_t Timeout = 0xFFFF;
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	ds->State = CPAL_STATE_BUSY;	/* Set CPAL state to CPAL_STATE_DISABLED */

	__CPAL_I2C_HAL_DISABLE_ERRIT();	/* Disable ERROR Interrupt */
	__CPAL_I2C_HAL_DISABLE_DEV();	/* Disable I2Cx Device */
	__CPAL_I2C_HAL_ENABLE_DEV();	/* Enable I2Cx Device */

//	DWT->CYCCNT = 0;
	__CPAL_I2C_HAL_START();			/* Generate Start */
	__CPAL_I2C_TIMEOUT(__CPAL_I2C_HAL_GET_SB(), CPAL_I2C_TIMEOUT_SB);
	// ~ 2 us
	/* Send Slave address with bit0 reset for write */
	// -- DWT->CYCCNT = 0;
	__CPAL_I2C_HAL_SEND(FM24_ADDRESS_WRITE);
	while (__CPAL_I2C_HAL_GET_ADDR() == 0 && Timeout != 0)
		--Timeout;
	// = 811 / 72 = 11 us
	__CPAL_I2C_HAL_CLEAR_AF();
	// DWT->CYCCNT = 0;
	__CPAL_I2C_HAL_STOP();
	while(__CPAL_I2C_HAL_GET_BUSY());	/* wait until Busy flag is reset */
	// = 813 / 72 = 11 us
	__CPAL_I2C_HAL_DISABLE_DEV();	/* Disable I2Cx Device */
	__CPAL_I2C_HAL_ENABLE_DEV();	/* Enable I2Cx Device */

	__CPAL_I2C_HAL_ENABLE_ACK();	/* Enable ACK */
	__CPAL_I2C_HAL_ENABLE_ERRIT();	/* Enable ERROR Interrupt */

	ds->State = CPAL_STATE_READY;	/* Set CPAL state to ready */

	/* If Timeout occurred	*/
	return (Timeout == 0)  ? CPAL_FAIL : CPAL_PASS;
}

/**
  * @brief  This function handles I2C1 interrupt request.
  * @param  None. 
  * @retval CPAL_PASS. 
  */
void I2Cx_EV_IRQHandler(void)
{  
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;
	register __IO uint16_t I2CFlagStatus;

	/* Read I2C1 Status Registers 1 and 2 */
	I2CFlagStatus = __CPAL_I2C_HAL_GET_EVENT();

	if ((I2CFlagStatus & I2C_SR1_SB ) != 0)
	{
		I2C_MASTER_START_Handle(i2c);
	}
	else
	if ((I2CFlagStatus & I2C_SR1_ADDR ) != 0)
	{
		I2C_MASTER_ADDR_Handle(i2c);
	}
}

/**
  * @brief  This function handles I2C1 Errors interrupt.
  * @param  None. 
  * @retval CPAL_PASS. 
  */
void I2Cx_ER_IRQHandler(void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;	
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	/* Read Error Register and affect to DevError */
	ds->Error = (CPAL_I2CErrorTypeDef)__CPAL_I2C_HAL_GET_ERROR();

	
	ds->State = CPAL_STATE_ERROR;	/* Set Device state to CPAL_STATE_ERROR */
	__CPAL_I2C_HAL_CLEAR_ERROR();		/* Clear error flags that can be cleared by writing to SR register */

	/* If Bus error occurred ---------------------------------------------------*/
	if ((ds->Error & CPAL_I2C_ERR_BERR) != 0)
	{
		/* Generate I2C software reset in order to release SDA and SCL lines */
		__CPAL_I2C_HAL_SWRST();
	}

	/* If Arbitration Loss error occurred --------------------------------------*/
	if ((ds->Error & CPAL_I2C_ERR_ARLO) != 0)
	{
		/* Generate I2C software reset in order to release SDA and SCL lines */
		__CPAL_I2C_HAL_SWRST();
	}

	/* Call Error UserCallback */
	CPAL_I2C_ERR_UserCallback(ds->Error);
}

/**
  * @brief  This function handles I2C1 TX DMA interrupt request.
  * @param  None. 
  * @retval CPAL_PASS. 
  */
void CPAL_I2Cx_DMA_TX_IRQHandler(void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;	
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	/* Reinitialize Timeout Value to default (no timeout initiated) */
	ds->Timeout = CPAL_I2C_TIMEOUT_DEFAULT;

	if ((__CPAL_I2C_HAL_GET_DMATX_TCIT()) != 0)
	{
		ds->Count = 0;

		CPAL_I2C_DMATXTC_UserCallback();

		__CPAL_I2C_HAL_DISABLE_DMAREQ();
		
		/* Wait until BTF flag is set */ 
		__CPAL_I2C_TIMEOUT_VOID(__CPAL_I2C_HAL_GET_BTF(), CPAL_I2C_TIMEOUT_BTF);
		__CPAL_I2C_HAL_STOP();
		__CPAL_I2C_TIMEOUT_VOID(!(__CPAL_I2C_HAL_GET_BUSY()), CPAL_I2C_TIMEOUT_BUSY);

		__CPAL_I2C_HAL_DISABLE_DMATX();		/* Disable DMA Channel */
		__CPAL_I2C_HAL_DISABLE_EVTIT();	 	/* Disable EVENT Interrupt */

		ds->State = CPAL_STATE_READY; 	/* Update CPAL_State to CPAL_STATE_READY */

		CPAL_I2C_TXTC_UserCallback();
	}
	else if ((__CPAL_I2C_HAL_GET_DMATX_TEIT()) != 0)
	{
		ds->State = CPAL_STATE_ERROR;
		ds->Count = __CPAL_I2C_HAL_DMATX_GET_CNDT();
	}

	__CPAL_I2C_HAL_CLEAR_DMATX_IT();
}

/**
  * @brief  This function handles I2C1 RX DMA interrupt request.
  * @param  None. 
  * @retval CPAL_PASS. 
  */
void CPAL_I2Cx_DMA_RX_IRQHandler(void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;	
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	/* Reinitialize Timeout Value to default (no timeout initiated) */
	ds->Timeout = CPAL_I2C_TIMEOUT_DEFAULT; 
	
	if ((__CPAL_I2C_HAL_GET_DMARX_TCIT()) != 0)
	{	 
		ds->Count = 0;

		CPAL_I2C_DMARXTC_UserCallback();

		__CPAL_I2C_HAL_STOP();				/* Generate Stop Condition */
		__CPAL_I2C_HAL_DISABLE_DMAREQ();	/* Disable DMA Request and Channel */
		
		__CPAL_I2C_TIMEOUT_VOID(!(__CPAL_I2C_HAL_GET_BUSY()), CPAL_I2C_TIMEOUT_BUSY);
		
		__CPAL_I2C_HAL_DISABLE_DMARX();		/* Disable DMA Channel */
		__CPAL_I2C_HAL_DISABLE_EVTIT();		/* Disable EVENT Interrupt */
		__CPAL_I2C_HAL_DISABLE_LAST();		/* Disable DMA automatic NACK generation */

		ds->State = CPAL_STATE_READY;

		CPAL_I2C_RXTC_UserCallback();
	}
	else if ((__CPAL_I2C_HAL_GET_DMARX_TEIT()) != 0)
	{	 
		ds->State = CPAL_STATE_ERROR; 	/* Update CPAL_State to CPAL_STATE_ERROR */
		ds->Count = __CPAL_I2C_HAL_DMARX_GET_CNDT();
	}
	
	__CPAL_I2C_HAL_CLEAR_DMARX_IT();
}

/**
	* @brief	This function Manages I2C Timeouts when waiting for specific events.
	* @param	None
	* @retval	CPAL_PASS or CPAL_FAIL. 
	*/
void CPAL_I2C_TIMEOUT_Manager(void)
{
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	/* If Timeout occurred	*/
	if (ds->Timeout == CPAL_I2C_TIMEOUT_DETECTED)
	{
		ds->Timeout = CPAL_I2C_TIMEOUT_DEFAULT;	/* Reinitialize Timeout Value */
		ds->State = CPAL_STATE_ERROR;				/* update CPAL_State to CPAL_STATE_ERROR */

		/* In case of Device Error Timeout_Callback should not be called */
		if (ds->Error == CPAL_I2C_ERR_NONE)
		{				
			ds->Error = CPAL_I2C_ERR_TIMEOUT;	/* update DevError to CPAL_I2C_ERR_TIMEOUT */
			CPAL_TIMEOUT_UserCallback();				/* Call CPAL_TIMEOUT_UserCallback */
		}
	}
	/* If Timeout is triggered (Timeout != CPAL_I2C_TIMEOUT_DEFAULT)*/
	else if (ds->Timeout != CPAL_I2C_TIMEOUT_DEFAULT)
	{
		ds->Timeout--;	/* Decrement the timeout value */
	}
}

/**
	* @brief	This function Manages I2C Timeouts when Timeout occurred.
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval	CPAL_PASS or CPAL_FAIL. 
	*/
uint32_t CPAL_I2C_Timeout (void)
{
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	ds->Timeout = CPAL_I2C_TIMEOUT_DEFAULT;	/* Reinitialize Timeout Value */
	ds->State = CPAL_STATE_ERROR;				/* update CPAL_State to CPAL_STATE_ERROR */
	ds->Error = CPAL_I2C_ERR_TIMEOUT;		/* update DevError to CPAL_I2C_ERR_TIMEOUT */
	return CPAL_TIMEOUT_UserCallback();				/* Call Timeout Callback and quit current function */
}

/**
	* @brief	Handles Master Start condition (SB) interrupt event.
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval	CPAL_PASS or CPAL_FAIL. 
	*/
void I2C_MASTER_START_Handle(register I2C_TypeDef * i2c)
{
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	if (ds->State == CPAL_STATE_READY_RX)
	{
		__CPAL_I2C_HAL_SEND(FM24_ADDRESS_READ);
		ds->State = CPAL_STATE_BUSY_RX;
	}
	else
	{
		__CPAL_I2C_HAL_SEND(FM24_ADDRESS_WRITE);
		ds->State = CPAL_STATE_BUSY_TX;
	}
	ds->Timeout = CPAL_I2C_TIMEOUT_MIN + CPAL_I2C_TIMEOUT_ADDR;
}

/**
	* @brief	This function Configure I2C DMA and Interrupts before starting transfer phase.
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @param	Direction : Transfer direction.
	* @retval	CPAL_PASS or CPAL_FAIL. 
	*/
uint32_t CPAL_I2C_Enable_DMA_IT (void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	__CPAL_I2C_HAL_DISABLE_EVTIT();	/* Disable EVENT Interrupt */
	__CPAL_I2C_HAL_ENABLE_DMAREQ();	/* Enable DMA request */
	
	if (ds->State == CPAL_STATE_BUSY_TX)
	{
		/* Configure TX DMA Channels */
		CPAL_DMA_InitStructure.DMA_MemoryBaseAddr	= (uint32_t)(ds->Buffer);
		CPAL_DMA_InitStructure.DMA_BufferSize		= ds->Count;
		CPAL_DMA_InitStructure.DMA_DIR				= DMA_DIR_PeripheralDST;
		DMA_Init(CPAL_I2C_DMA_TX_Channel, &CPAL_DMA_InitStructure);

		__CPAL_I2C_HAL_DISABLE_LAST(); 	/* Disable DMA automatic NACK generation */
		__CPAL_I2C_HAL_ENABLE_DMATX();	/* Enable TX DMA Channels */
	}		
	else if (ds->State == CPAL_STATE_BUSY_RX)
	{
		/* Configure RX DMA Channels */
		CPAL_DMA_InitStructure.DMA_MemoryBaseAddr	= (uint32_t)(ds->Buffer);
		CPAL_DMA_InitStructure.DMA_BufferSize		= ds->Count;
		CPAL_DMA_InitStructure.DMA_DIR				= DMA_DIR_PeripheralSRC;
		DMA_Init(CPAL_I2C_DMA_RX_Channel, &CPAL_DMA_InitStructure);

		__CPAL_I2C_HAL_ENABLE_LAST();	/* Enable DMA automatic NACK generation */
		__CPAL_I2C_HAL_ENABLE_DMARX();	/* Enable RX DMA Channels */
	}
	return CPAL_PASS;
}

/**
	* @brief	Handles Master address matched (ADDR) interrupt event. 
	* @param	pDevInitStruct: Pointer to the peripheral configuration structure.
	* @retval CPAL_PASS or CPAL_FAIL. 
	*/
void I2C_MASTER_ADDR_Handle(register I2C_TypeDef * i2c)
{
	register CPAL_InitTypeDef * ds	= &FM24_Device;

	if (ds->State == CPAL_STATE_BUSY_TX)
		ds->Timeout = CPAL_I2C_TIMEOUT_MIN + ds->Count;	/* Set 1ms timeout for each data transfer in case of DMA Tx mode */
	else if (ds->State == CPAL_STATE_BUSY_RX)
		ds->Timeout = CPAL_I2C_TIMEOUT_MIN + ds->Count;	/* Set 1ms timeout for each data transfer in case of DMA Rx mode */
	else
		ds->Timeout = CPAL_I2C_TIMEOUT_DEFAULT;			/* Reinitialize Timeout Value to default (no timeout initiated) */

	__CPAL_I2C_HAL_CLEAR_ADDR();

	if (ds->State == CPAL_STATE_BUSY_TX)
	{
		__CPAL_I2C_HAL_SEND((ds->Address & 0xFF00) >> 8);
		__CPAL_I2C_TIMEOUT_VOID(__CPAL_I2C_HAL_GET_TXE(), CPAL_I2C_TIMEOUT_TXE);

		__CPAL_I2C_HAL_SEND(ds->Address & 0x00FF);
		__CPAL_I2C_TIMEOUT_VOID(__CPAL_I2C_HAL_GET_TXE(), CPAL_I2C_TIMEOUT_TXE);
	}

	CPAL_I2C_Enable_DMA_IT();
}

/**
	* @brief
	* @param
	* @retval
	*/
CPAL_StateTypeDef CPAL_I2C_GetState(void)
{
	return FM24_Device.State;
}

/**
	* @brief
	* @param
	* @retval
	*/
CPAL_I2CErrorTypeDef CPAL_I2C_GetError(void)
{
	return FM24_Device.Error;
}
