#include "cpal_i2c_hal_stm32f10x.h"
#include "cpal_i2c.h"

/**
  * @brief  Reset then enable the I2C device clock.
  * @param  Device : I2C Device instance. 
  * @retval None
  */
void CPAL_I2C_HAL_CLKInit(void)
{
	/* Reset I2Cx device clock in order to avoid non-cleared error flags */
	__I2C_RCC_RESET(CPAL_I2C_CLK);

	/* Enable I2Cx device clock */
	__I2C_CLK_CMD(CPAL_I2C_CLK, ENABLE);
}

/**
  * @brief  Reset then disable the I2C device clock.
  * @param  Device : I2C Device instance 
  * @retval None. 
  */
void CPAL_I2C_HAL_CLKDeInit(void)
{   
	/* Reset I2Cx device clock in order to avoid non-cleared error flags */
	__I2C_RCC_RESET(CPAL_I2C_CLK);

	/* Disable I2Cx device clock */
	__I2C_CLK_CMD(CPAL_I2C_CLK, DISABLE);
}

void DelayMicro(uint16_t us)
{
	register uint32_t count = (SystemCoreClock / 1000000UL) * us;
	while (count-- != 0)
			__NOP();
}

/**
  * @brief  Configure the IO pins used by the I2C device.
  * @param  Device : I2C Device instance. 
  * @retval None. 
  */
void CPAL_I2C_HAL_GPIOInit(void)
{
	uint8_t retry_count_clk;
	uint8_t retry_count;

	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable I2Cx SCL and SDA Pin Clock */
	__I2C_GPIO_CLK_CMD((CPAL_I2C_SCL_GPIO_CLK | CPAL_I2C_SDA_GPIO_CLK), ENABLE); 

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_SetBits(CPAL_I2C_SCL_GPIO_PORT, CPAL_I2C_SCL_GPIO_PIN);
	GPIO_SetBits(CPAL_I2C_SDA_GPIO_PORT, CPAL_I2C_SDA_GPIO_PIN);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;

	GPIO_InitStructure.GPIO_Pin = CPAL_I2C_SCL_GPIO_PIN;
	GPIO_Init(CPAL_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CPAL_I2C_SDA_GPIO_PIN;
	GPIO_Init(CPAL_I2C_SDA_GPIO_PORT, &GPIO_InitStructure);

	retry_count_clk = 10;
	while (GPIO_ReadInputDataBit(CPAL_I2C_SDA_GPIO_PORT, CPAL_I2C_SDA_GPIO_PIN) == Bit_RESET
		&& retry_count_clk-- != 0) 
	{
		retry_count = 10;
		/* Set clock high and wait for any clock stretching to finish. */
		GPIO_SetBits(CPAL_I2C_SCL_GPIO_PORT, CPAL_I2C_SCL_GPIO_PIN);
		while (GPIO_ReadInputDataBit(CPAL_I2C_SCL_GPIO_PORT, CPAL_I2C_SCL_GPIO_PIN) == Bit_RESET && retry_count-- != 0)
			DelayMicro(1);
		DelayMicro(2);

		GPIO_ResetBits(CPAL_I2C_SCL_GPIO_PORT, CPAL_I2C_SCL_GPIO_PIN);
		DelayMicro(2);

		GPIO_SetBits(CPAL_I2C_SCL_GPIO_PORT, CPAL_I2C_SCL_GPIO_PIN);
		DelayMicro(2);
	}

	/* Generate a start then stop condition */
	GPIO_SetBits(CPAL_I2C_SCL_GPIO_PORT, CPAL_I2C_SCL_GPIO_PIN);
	DelayMicro(2);
	
	GPIO_ResetBits(CPAL_I2C_SDA_GPIO_PORT, CPAL_I2C_SDA_GPIO_PIN);
	DelayMicro(2);

	GPIO_SetBits(CPAL_I2C_SDA_GPIO_PORT, CPAL_I2C_SDA_GPIO_PIN);
	DelayMicro(2);

	retry_count = 10;
	while (GPIO_ReadInputDataBit(CPAL_I2C_SCL_GPIO_PORT, CPAL_I2C_SCL_GPIO_PIN) == Bit_RESET && retry_count-- != 0)
		DelayMicro(1);

	retry_count = 10;
	while (GPIO_ReadInputDataBit(CPAL_I2C_SDA_GPIO_PORT, CPAL_I2C_SDA_GPIO_PIN) == Bit_RESET && retry_count-- != 0)
		DelayMicro(1);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;

	GPIO_InitStructure.GPIO_Pin = CPAL_I2C_SCL_GPIO_PIN;
	GPIO_Init(CPAL_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CPAL_I2C_SDA_GPIO_PIN;
	GPIO_Init(CPAL_I2C_SDA_GPIO_PORT, &GPIO_InitStructure);
}


/**
  * @brief  Deinitialize the IO pins used by the I2C device 
  *         (configured to their default state).
  * @param  Device : I2C Device instance. 
  * @retval None. 
  */
void CPAL_I2C_HAL_GPIODeInit(void)
{      
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;

	GPIO_InitStructure.GPIO_Pin = CPAL_I2C_SCL_GPIO_PIN;
	GPIO_Init(CPAL_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CPAL_I2C_SDA_GPIO_PIN;
	GPIO_Init(CPAL_I2C_SDA_GPIO_PORT, &GPIO_InitStructure); 
}

/**
  * @brief  Configure NVIC Priority Group.
  * @param  None.
  * @retval None. 
  */
void CPAL_HAL_NVICInit(void)
{
	/* Set NVIC Group Priority */
	NVIC_PriorityGroupConfig (CPAL_NVIC_PRIOGROUP);
}

/**
  * @brief  Configure NVIC and interrupts used by I2C Device according to 
  *         enabled options
  * @param  Device : I2C Device instance.
  * @param  Options : I2C Transfer Options.
  * @retval None. 
  */
void CPAL_I2C_HAL_ITInit(void)
{
	register I2C_TypeDef * i2c = CPAL_I2C_DEVICE;

	NVIC_InitTypeDef NVIC_InitStructure; 

	/* Configure NVIC priority Group */ 
	CPAL_HAL_NVICInit();

	/* Enable the IRQ channel */
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	/* Configure NVIC for I2Cx EVT Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = CPAL_I2C_IT_EVT_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_EVT_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_EVT_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure NVIC for I2Cx ERR Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = CPAL_I2C_IT_ERR_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_ERR_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_ERR_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable I2C Error Interrupts */
	__CPAL_I2C_HAL_ENABLE_ERRIT();

	/* Configure NVIC for DMA TX channel interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = CPAL_I2C_DMA_TX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_DMATX_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_DMATX_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable DMA TX Channel TCIT  */
	__I2C_HAL_ENABLE_DMATX_TCIT();

	/* Enable DMA TX Channel TEIT  */    
	__I2C_HAL_ENABLE_DMATX_TEIT(); 

	/* Configure NVIC for DMA RX channel interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = CPAL_I2C_DMA_RX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = I2C_IT_DMARX_PREPRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = I2C_IT_DMARX_SUBPRIO;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable DMA RX Channel TCIT  */
	__I2C_HAL_ENABLE_DMARX_TCIT();  

	/* Enable DMA RX Channel TEIT  */
	__I2C_HAL_ENABLE_DMARX_TEIT(); 
}

/**
  * @brief  Deinitialize NVIC and interrupts used by I2C Device in 
  *         the current Configuration.
  * @param  Device : I2C Device instance.
  * @param  Options : I2C Transfer Options.
  * @retval None. 
  */
void CPAL_I2C_HAL_ITDeInit()
{
	/* Disable I2Cx EVT IRQn and I2Cx ERR IRQn */ 
	NVIC_DisableIRQ(CPAL_I2C_IT_EVT_IRQn);
	NVIC_DisableIRQ(CPAL_I2C_IT_ERR_IRQn);
  
	NVIC_DisableIRQ(CPAL_I2C_DMA_TX_IRQn);
	NVIC_DisableIRQ(CPAL_I2C_DMA_RX_IRQn);
}
