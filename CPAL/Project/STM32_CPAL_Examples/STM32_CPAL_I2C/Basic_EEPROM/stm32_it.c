#include "stm32_it.h"

void NMI_Handler(void)			{	while (1) { }	}
void HardFault_Handler(void)	{	while (1) { }	}
void MemManage_Handler(void)	{	while (1) { }	}
void BusFault_Handler(void)		{	while (1) { }	}
void UsageFault_Handler(void)	{	while (1) { }	}
void SVC_Handler(void)			{ }
void DebugMon_Handler(void)		{ }
void PendSV_Handler(void)		{ }
/*
void TIM7_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
	{   
		STM_EVAL_LEDToggle(LED4);
		TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
	}
}
*/
