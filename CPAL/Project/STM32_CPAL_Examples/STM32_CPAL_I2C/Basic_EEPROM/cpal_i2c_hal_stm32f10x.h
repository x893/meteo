#ifndef ___CPAL_I2C_HAL_STM32F10X_H
#define ___CPAL_I2C_HAL_STM32F10X_H

#ifdef __cplusplus
extern "C" {
#endif
  
#include "stm32f10x.h"
#include "stm32f10x_i2c.h"  
#include "stm32f10x_dma.h" 
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "misc.h"

#include "cpal.h" 

#define DMA_CCR_EN					((uint16_t)0x0001)
#define CPAL_I2C_STATUS_ERR_MASK	((uint16_t)0x0F00)
#define CPAL_I2C_STATUS1_EVT_MASK	((uint16_t)0x00DF)

/*========= Peripheral Clock Command =========*/

#define __AFIO_CLK_CMD(cmd)				RCC_APB2PeriphClockCmd((RCC_APB2Periph_AFIO),(cmd))
#define __I2C_CLK_CMD(clk,cmd)			RCC_APB1PeriphClockCmd((clk),(cmd))
#define __I2C_RCC_RESET(clk)			RCC_APB1PeriphResetCmd((clk),ENABLE);	\
										RCC_APB1PeriphResetCmd((clk),DISABLE)

#define __I2C_GPIO_CLK_CMD(clk,cmd)		RCC_APB2PeriphClockCmd((clk),(cmd))
#define __DMA_CLK_CMD(clk,cmd)			RCC_AHBPeriphClockCmd((clk),(cmd))
#define __DMA_RESET_CMD(clk,cmd)		RCC_AHBPeriphResetCmd((clk),(cmd))

/*========= DMA =========*/

#define __CPAL_I2C_HAL_ENABLE_DMATX()      CPAL_I2C_DMA_TX_Channel->CCR |= DMA_CCR_EN
#define __CPAL_I2C_HAL_DISABLE_DMATX()     CPAL_I2C_DMA_TX_Channel->CCR &= ~DMA_CCR_EN
#define __CPAL_I2C_HAL_ENABLE_DMARX()      CPAL_I2C_DMA_RX_Channel->CCR |= DMA_CCR_EN
#define __CPAL_I2C_HAL_DISABLE_DMARX()     CPAL_I2C_DMA_RX_Channel->CCR &= ~DMA_CCR_EN

/* DMA interrupts enable/disable */  
#define __I2C_HAL_ENABLE_DMATX_TCIT()      CPAL_I2C_DMA_TX_Channel->CCR |= DMA_IT_TC
#define __I2C_HAL_ENABLE_DMATX_TEIT()      CPAL_I2C_DMA_TX_Channel->CCR |= DMA_IT_TE
#define __I2C_HAL_ENABLE_DMARX_TCIT()      CPAL_I2C_DMA_RX_Channel->CCR |= DMA_IT_TC
#define __I2C_HAL_ENABLE_DMARX_TEIT()      CPAL_I2C_DMA_RX_Channel->CCR |= DMA_IT_TE
  
/* DMA interrupts flag management */
#define __CPAL_I2C_HAL_GET_DMATX_TCIT()    (uint32_t)(CPAL_I2C_DMA->ISR & CPAL_I2C_DMA_TX_TC_FLAG)
#define __CPAL_I2C_HAL_GET_DMATX_TEIT()    (uint32_t)(CPAL_I2C_DMA->ISR & CPAL_I2C_DMA_TX_TE_FLAG)
#define __CPAL_I2C_HAL_GET_DMARX_TCIT()    (uint32_t)(CPAL_I2C_DMA->ISR & CPAL_I2C_DMA_RX_TC_FLAG)
#define __CPAL_I2C_HAL_GET_DMARX_TEIT()    (uint32_t)(CPAL_I2C_DMA->ISR & CPAL_I2C_DMA_RX_TE_FLAG)
#define __CPAL_I2C_HAL_CLEAR_DMATX_IT()    CPAL_I2C_DMA->IFCR = (CPAL_I2C_DMA_TX_TC_FLAG | CPAL_I2C_DMA_TX_TE_FLAG)
#define __CPAL_I2C_HAL_CLEAR_DMARX_IT()    CPAL_I2C_DMA->IFCR = (CPAL_I2C_DMA_RX_TC_FLAG | CPAL_I2C_DMA_RX_TE_FLAG) 

/* Get DMA data counter */
#define __CPAL_I2C_HAL_DMATX_GET_CNDT()		(uint32_t)(CPAL_I2C_DMA_TX_Channel->CNDTR)
#define __CPAL_I2C_HAL_DMARX_GET_CNDT()		(uint32_t)(CPAL_I2C_DMA_RX_Channel->CNDTR) 

/* I2C enable/disable */  
#define __CPAL_I2C_HAL_ENABLE_DEV()			i2c->CR1 |= I2C_CR1_PE  
#define __CPAL_I2C_HAL_DISABLE_DEV()		i2c->CR1 &= ~I2C_CR1_PE

/* I2C software reset */
#define __CPAL_I2C_HAL_SWRST()				i2c->CR1 |= I2C_CR1_SWRST; \
											i2c->CR1 &= ~I2C_CR1_SWRST

/* I2C interrupts enable/disable */
#define __CPAL_I2C_HAL_ENABLE_ERRIT()		i2c->CR2 |=  I2C_CR2_ITERREN
#define __CPAL_I2C_HAL_DISABLE_ERRIT()		i2c->CR2 &= ~I2C_CR2_ITERREN
#define __CPAL_I2C_HAL_ENABLE_EVTIT()		i2c->CR2 |=  I2C_CR2_ITEVTEN
#define __CPAL_I2C_HAL_DISABLE_EVTIT()		i2c->CR2 &= ~I2C_CR2_ITEVTEN
#define __CPAL_I2C_HAL_ENABLE_BUFIT()		i2c->CR2 |=  I2C_CR2_ITBUFEN
#define __CPAL_I2C_HAL_DISABLE_BUFIT()		i2c->CR2 &= ~I2C_CR2_ITBUFEN

/* I2C misc configuration */
#define __CPAL_I2C_HAL_ENABLE_DMAREQ()		i2c->CR2 |= I2C_CR2_DMAEN
#define __CPAL_I2C_HAL_DISABLE_DMAREQ()		i2c->CR2 &= ~I2C_CR2_DMAEN

#define __CPAL_I2C_HAL_ENABLE_ACK()			i2c->CR1 |= I2C_CR1_ACK
#define __CPAL_I2C_HAL_DISABLE_ACK()		i2c->CR1 &= ~I2C_CR1_ACK

#define __CPAL_I2C_HAL_ENABLE_POS()			i2c->CR1 |= I2C_CR1_POS
#define __CPAL_I2C_HAL_DISABLE_POS()		i2c->CR1 &= ~I2C_CR1_POS

#define __CPAL_I2C_HAL_ENABLE_LAST()		i2c->CR2 |= I2C_CR2_LAST
#define __CPAL_I2C_HAL_DISABLE_LAST()		i2c->CR2 &= ~I2C_CR2_LAST

#define __CPAL_I2C_HAL_ENABLE_NOSTRETCH()	i2c->CR1 |= I2C_CR1_NOSTRETCH
#define __CPAL_I2C_HAL_DISABLE_NOSTRETCH()	i2c->CR1 &= ~I2C_CR1_NOSTRETCH

#define __CPAL_I2C_HAL_START()				i2c->CR1 |= I2C_CR1_START
#define __CPAL_I2C_HAL_STOP()				i2c->CR1 |= I2C_CR1_STOP 

/* I2C data management */
#define __CPAL_I2C_HAL_RECEIVE()			(uint8_t)(i2c->DR) 
#define __CPAL_I2C_HAL_SEND(value)			i2c->DR = (uint8_t)((value))

/* I2C flags management */
#define __CPAL_I2C_HAL_GET_EVENT()			(uint16_t)(i2c->SR1 & CPAL_I2C_STATUS1_EVT_MASK)
#define __CPAL_I2C_HAL_GET_ERROR()			(uint16_t)(i2c->SR1 & CPAL_I2C_STATUS_ERR_MASK)
#define __CPAL_I2C_HAL_GET_SB()				(uint16_t)(i2c->SR1 & I2C_SR1_SB) 
#define __CPAL_I2C_HAL_GET_ADDR()			(uint16_t)(i2c->SR1 & I2C_SR1_ADDR) 
#define __CPAL_I2C_HAL_GET_ADD10()			(uint16_t)(i2c->SR1 & I2C_SR1_ADD10) 
#define __CPAL_I2C_HAL_GET_STOPF()			(uint16_t)(i2c->SR1 & I2C_SR1_STOPF) 
#define __CPAL_I2C_HAL_GET_BTF()			(uint16_t)(i2c->SR1 & I2C_SR1_BTF) 
#define __CPAL_I2C_HAL_GET_TXE()			(uint16_t)(i2c->SR1 & I2C_SR1_TXE) 
#define __CPAL_I2C_HAL_GET_RXNE()			(uint16_t)(i2c->SR1 & I2C_SR1_RXNE) 
#define __CPAL_I2C_HAL_GET_BUSY()			(uint16_t)(i2c->SR2 & I2C_SR2_BUSY) 
#define __CPAL_I2C_HAL_GET_GENCALL()		(uint16_t)(i2c->SR2 & I2C_SR2_GENCALL) 
#define __CPAL_I2C_HAL_GET_DUALF()			(uint16_t)(i2c->SR2 & I2C_SR2_DUALF) 
#define __CPAL_I2C_HAL_GET_TRA()			(uint16_t)(i2c->SR2 & I2C_SR2_TRA)  
#define __CPAL_I2C_HAL_GET_OVR()			(uint16_t)(i2c->SR1 & I2C_SR1_OVR) 
#define __CPAL_I2C_HAL_GET_AF()				(uint16_t)(i2c->SR1 & I2C_SR1_AF) 
#define __CPAL_I2C_HAL_GET_ARLO()			(uint16_t)(i2c->SR1 & I2C_SR1_ARLO) 
#define __CPAL_I2C_HAL_GET_BERR()			(uint16_t)(i2c->SR1 & I2C_SR1_BERR)   

#define __CPAL_I2C_HAL_CLEAR_SB()			i2c->SR1; i2c->SR2
#define __CPAL_I2C_HAL_CLEAR_ADDR()			i2c->SR1; i2c->SR2
#define __CPAL_I2C_HAL_CLEAR_BTF()			i2c->SR1; i2c->DR
#define __CPAL_I2C_HAL_CLEAR_STOPF()		i2c->SR1; i2c->CR1 |= I2C_CR1_PE
#define __CPAL_I2C_HAL_CLEAR_AF()			i2c->SR1 = ~I2C_SR1_AF
#define __CPAL_I2C_HAL_CLEAR_ERROR()		i2c->SR1 = ~CPAL_I2C_STATUS_ERR_MASK

/*========= Hardware Abstraction Layer local =========*/      
void CPAL_I2C_HAL_CLKInit(void);
void CPAL_I2C_HAL_CLKDeInit(void);
void CPAL_I2C_HAL_GPIOInit(void);
void CPAL_I2C_HAL_GPIODeInit(void);

void CPAL_I2C_HAL_ITInit	(void);
void CPAL_I2C_HAL_ITDeInit	(void);

#ifdef __cplusplus
}
#endif

#endif /*___CPAL_I2C_HAL_STM32F10X_H */
