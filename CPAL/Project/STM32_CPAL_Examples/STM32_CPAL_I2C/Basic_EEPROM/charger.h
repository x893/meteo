#ifndef __CHARGER_H__
#define __CHARGER_H__

#include "common.h"
#include "board.h"

void ChargerOff(void);
void ChargerDeInit(void);
ERROR_t ChargerInit(uint16_t freqKHz);
void ChargerDuty(uint16_t duty);

#endif
