#ifndef __FM24_H
#define __FM24_H

#include "stm32f10x.h"
   
#ifdef __cplusplus
extern "C" {
#endif

#define FM24_ADDRESS_READ	(0xA0 | 1)
#define FM24_ADDRESS_WRITE	(0xA0 | 0)

/* CriticalSectionCallback : Call User callback for critical section (should typically disable interrupts) */
#define FM24_EnterCriticalSection_UserCallback	__disable_irq
#define FM24_ExitCriticalSection_UserCallback	__enable_irq

/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------*/

/*  -- Section 4 :         **** Configure Timeout method, TimeoutCallback ****

    Description: This section allows you to implement your own Timeout Procedure.
                 By default Timeout procedure is implemented with Systick timer and 
                 FM24_I2C_TIMEOUT_Manager is defined as SysTick_Handler.
                 */

#define FM24_TIMEOUT_INIT()			SysTick_Config((SystemCoreClock / 1000));	\
									NVIC_SetPriority (SysTick_IRQn, 0)

#define FM24_TIMEOUT_DEINIT()		SysTick->CTRL = 0	/*<! Disable the systick timer */ 
#define FM24_I2C_TIMEOUT_Manager	SysTick_Handler		/*<! This callback is used to handle Timeout error.
															When a timeout occurs FM24_TIMEOUT_UserCallback
															is called to handle this error */
#ifndef FM24_I2C_TIMEOUT_Manager
	void FM24_I2C_TIMEOUT_Manager(void);
#endif

/* Maximum Timeout values for each communication operation (preferably, Time base should be 1 Millisecond).
   The exact maximum value is the sum of event timeout value and the FM24_I2C_TIMEOUT_MIN value defined below */
#define FM24_I2C_TIMEOUT_SB             30
#define FM24_I2C_TIMEOUT_ADDR           3
#define FM24_I2C_TIMEOUT_ADD10          3
#define FM24_I2C_TIMEOUT_TXE            2
#define FM24_I2C_TIMEOUT_RXNE           2
#define FM24_I2C_TIMEOUT_BTF            4
#define FM24_I2C_TIMEOUT_BUSY           5

/* DO NOT MODIFY THESE VALUES ---------------------------------------------------------*/
#define FM24_I2C_TIMEOUT_DEFAULT        ((uint32_t)0xFFFFFFFF)
#define FM24_I2C_TIMEOUT_MIN            ((uint32_t)0x00000001)
#define FM24_I2C_TIMEOUT_DETECTED       ((uint32_t)0x00000000)
 
/*-----------NVIC Group Priority-------------*/
#define FM24_NVIC_PRIOGROUP	NVIC_PriorityGroup_0 /*!< 0 bits for preemption priority, 4 bits for subpriority */
/* #define FM24_NVIC_PRIOGROUP	NVIC_PriorityGroup_1 */ /*!< 1 bits for preemption priority, 3 bits for subpriority */
/* #define FM24_NVIC_PRIOGROUP		NVIC_PriorityGroup_2 */	/*!< 2 bits for preemption priority, 2 bits for subpriority */
/* #define FM24_NVIC_PRIOGROUP	NVIC_PriorityGroup_3 */ /*!< 3 bits for preemption priority, 1 bits for subpriority */
/* #define FM24_NVIC_PRIOGROUP	NVIC_PriorityGroup_4 */ /*!< 4 bits for preemption priority */
  
/*-----------------------------------------------------------------------------------------------------------------------*/

#define FM24_I2C_SCL_GPIO_PORT		GPIOB
#define FM24_I2C_SCL_GPIO_CLK		RCC_APB2Periph_GPIOB
#define FM24_I2C_SCL_GPIO_PIN		GPIO_Pin_6
  
#define FM24_I2C_SDA_GPIO_PORT		GPIOB
#define FM24_I2C_SDA_GPIO_CLK		RCC_APB2Periph_GPIOB
#define FM24_I2C_SDA_GPIO_PIN		GPIO_Pin_7
 

#define FM24_I2C_DMA_TX_Channel     DMA1_Channel6
#define FM24_I2C_DMA_RX_Channel     DMA1_Channel7
#define FM24_I2C_DEVICE  			I2C1
	
#define FM24_I2C_CLK				RCC_APB1Periph_I2C1
#define FM24_I2C_DR					((uint32_t)&I2C1->DR)
  
#define FM24_I2C_DMA				DMA1
#define FM24_I2C_DMA_CLK			RCC_AHBPeriph_DMA1 
   
#define FM24_I2C_IT_EVT_IRQn		I2C1_EV_IRQn
#define FM24_I2C_IT_ERR_IRQn		I2C1_ER_IRQn   
#define I2Cx_ER_IRQHandler			I2C1_ER_IRQHandler
#define I2Cx_EV_IRQHandler			I2C1_EV_IRQHandler
  
#define FM24_I2C_DMA_TX_IRQn		DMA1_Channel6_IRQn
#define FM24_I2C_DMA_RX_IRQn		DMA1_Channel7_IRQn
  
#define FM24_I2Cx_DMA_TX_IRQHandler	DMA1_Channel6_IRQHandler
#define FM24_I2Cx_DMA_RX_IRQHandler	DMA1_Channel7_IRQHandler
  
#define FM24_I2C_DMA_TX_TC_FLAG		DMA1_FLAG_TC6
#define FM24_I2C_DMA_TX_HT_FLAG		DMA1_FLAG_HT6
#define FM24_I2C_DMA_TX_TE_FLAG		DMA1_FLAG_TE6
  
#define FM24_I2C_DMA_RX_TC_FLAG		DMA1_FLAG_TC7
#define FM24_I2C_DMA_RX_HT_FLAG		DMA1_FLAG_HT7
#define FM24_I2C_DMA_RX_TE_FLAG		DMA1_FLAG_TE7

/*-----------Interrupt Priority Offset-------------*/

/* This defines can be used to decrease the Level of Interrupt Priority for I2Cx Device (ERR, EVT, DMA_TX, DMA_RX).
   The value of I2Cx_IT_OFFSET_SUBPRIO is added to I2Cx_IT_XXX_SUBPRIO and the value of I2Cx_IT_OFFSET_PREPRIO 
   is added to I2Cx_IT_XXX_PREPRIO (XXX: ERR, EVT, DMATX, DMARX). 
   I2Cx Interrupt Priority are defined in FM24_I2C_hal_stm32f10x.h file in Section 3  */

#define I2C_IT_OFFSET_SUBPRIO		0      /* I2C1 SUB-PRIORITY Offset */ 
#define I2C_IT_OFFSET_PREPRIO		0      /* I2C1 PREEMPTION PRIORITY Offset */ 

#define I2C_IT_EVT_SUBPRIO			I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 EVT SUB-PRIORITY */ 
#define I2C_IT_EVT_PREPRIO			I2C_IT_OFFSET_PREPRIO + 2   /* I2C1 EVT PREEMPTION PRIORITY */

#define I2C_IT_ERR_SUBPRIO			I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 ERR SUB-PRIORITY */
#define I2C_IT_ERR_PREPRIO			I2C_IT_OFFSET_PREPRIO + 0   /* I2C1 ERR PREEMPTION PRIORITY */

#define I2C_IT_DMATX_SUBPRIO		I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 DMA TX SUB-PRIORITY */
#define I2C_IT_DMATX_PREPRIO		I2C_IT_OFFSET_PREPRIO + 1   /* I2C1 DMA TX PREEMPTION PRIORITY */

#define I2C_IT_DMARX_SUBPRIO		I2C_IT_OFFSET_SUBPRIO + 0   /* I2C1 DMA RX SUB-PRIORITY */
#define I2C_IT_DMARX_PREPRIO		I2C_IT_OFFSET_PREPRIO + 1   /* I2C1 DMA RX PREEMPTION PRIORITY */


typedef enum
{
	FM24_NO_ERROR			= 0x0000,
	FM24_I2C_ERR_TIMEOUT	= 0x00FF,
	FM24_I2C_ERR_BERR		= 0x0100,
	FM24_I2C_ERR_ARLO		= 0x0200,
	FM24_I2C_ERR_AF			= 0x0400,
	FM24_I2C_ERR_OVR		= 0x0800,
	FM24_FAIL				= 0x0001,
} FM24Error_t;

typedef enum
{
	FM24_STATE_DISABLED		= 0x00,
	FM24_STATE_READY		= 0x01,
	FM24_STATE_READY_TX		= 0x03,
	FM24_STATE_READY_RX		= 0x05,
	FM24_STATE_BUSY			= 0x02,
	FM24_STATE_BUSY_TX		= 0x06,
	FM24_STATE_BUSY_RX		= 0x0A,
	FM24_STATE_ERROR		= 0x10,
} FM24State_t;

typedef struct 
{
	__IO	uint32_t	Timeout;
	const	uint8_t *	Buffer;
	__IO	uint16_t	Count;
			uint16_t	Address;
	__IO	FM24Error_t		Error;
	__IO	FM24State_t		State;
} FM24Context_t;

extern FM24Context_t FM24_Device;

void	 FM24_I2C_StructInit	(void);

uint32_t FM24_I2C_Init			(void);
uint32_t FM24_I2C_DeInit		(void);
uint32_t FM24_I2C_IsDeviceReady	(void);
uint32_t FM24_I2C_Write			( const uint8_t* pBuffer, uint16_t WriteAddr, uint32_t NumByteToWrite );
uint32_t FM24_I2C_Read  		( uint8_t* pBuffer, uint16_t ReadAddr, uint32_t NumByteToRead );

FM24State_t FM24_I2C_GetState	(void);
FM24Error_t FM24_I2C_GetError	(void);

#ifdef __cplusplus
}
#endif

#endif /*__FM24_H */
