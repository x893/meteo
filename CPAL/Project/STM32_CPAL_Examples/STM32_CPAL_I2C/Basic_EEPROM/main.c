#include <string.h>

#include "fm24.h"
#include "ds18b20.h"
#include "charger.h"

#define EEPROM_READ_ADDR1               ((uint16_t)0x0010)
#define EEPROM_WRITE_ADDR1              ((uint16_t)0x0010)

const uint8_t DS18B20_CMD_44[]	= { 0xCC, 0x44 };
const uint8_t DS18B20_CMD_BE[]	= { 0xCC, 0xBE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
const uint8_t DS18B20_CMD_33[]	= { 0x33, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

const uint8_t Tx1Buffer[] = "Alpha";

#define NELEMENTS(a) (sizeof(a) / sizeof(*(a)))
#define BUFFER1_SIZE	(NELEMENTS(Tx1Buffer) - 1)
uint8_t Rx1Buffer[BUFFER1_SIZE];
uint8_t OWBuffer[32];


void Delay_5s(void)
{
	for (uint32_t i = 0; i < 10000000; i++)
		__NOP();
}
void Delay_05s(void)
{
	for (uint32_t i = 0; i < 1000000; i++)
		__NOP();
}
void Delay_1s(void)
{
	for (uint32_t i = 0; i < 1000000; i++)
		__NOP();
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	DBGMCU_Config( 0
		| DBGMCU_SLEEP
		| DBGMCU_STOP
		| DBGMCU_STANDBY
		| DBGMCU_I2C1_SMBUS_TIMEOUT
		| DBGMCU_TIM1_STOP
		, ENABLE);

	SystemCoreClockUpdate();
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

	ChargerInit(100);

while(1)
{
	ChargerDuty(10);
	Delay_5s();
	ChargerDuty(50);
	Delay_5s();
	ChargerDuty(90);
	Delay_5s();
}
#if 0
	DS18B20_Init();
	// memset(&OWBuffer, 0, sizeof(OWBuffer));
	// DS18B20_Scan(OWBuffer, sizeof(OWBuffer) / 8);

while(1)
{
	memset(&OWBuffer, 0, sizeof(OWBuffer));

	if (DS18B20_OK == DS18B20_Send(DS18B20_CMD_33, sizeof(DS18B20_CMD_33), &OWBuffer[0], 8, 1))
	{
		DWT->CYCCNT = 0;
	}
	else
	{
		DWT->CYCCNT = 0;
	}
	if (DS18B20_OK == DS18B20_Send(DS18B20_CMD_44, sizeof(DS18B20_CMD_44), &OWBuffer[0], 0, 0))
	{
		DWT->CYCCNT = 0;
		for (uint32_t i = 0; i < 5000000; i++)
			__NOP();
		if (DS18B20_OK == DS18B20_Send(DS18B20_CMD_BE, sizeof(DS18B20_CMD_BE), &OWBuffer[0], 9, 2))
		{
			float cels = DS18B20_Celcius(OWBuffer);
			DWT->CYCCNT = 0;
		}
		else
		{
			DWT->CYCCNT = 0;
		}
	}
	else
	{
		DWT->CYCCNT = 0;
	}
	DS18B20_PowerOff();
	Delay_5s();
}

	FM24_I2C_StructInit();
	FM24_I2C_Init();

	FM24_I2C_IsDeviceReady();
while(FM24_I2C_GetState() != FM24_STATE_READY)
	;

	FM24_I2C_Write(Tx1Buffer, EEPROM_WRITE_ADDR1, BUFFER1_SIZE);
while(FM24_I2C_GetState() != FM24_STATE_READY)
	;

	FM24_I2C_Read(Rx1Buffer, EEPROM_READ_ADDR1, BUFFER1_SIZE);
while(FM24_I2C_GetState() != FM24_STATE_READY)
	;

while(1)
	;
#endif
}		  

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *			where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
