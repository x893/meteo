#ifndef __CPAL_I2C_H
#define __CPAL_I2C_H

#include "cpal_i2c_hal_stm32f10x.h"

#ifdef __cplusplus
extern "C" {
#endif

/*========= CPAL_I2C_Exported_Functions =========*/
/* These functions constitute the main CPAL API interface. All functions take as argument the 
   CPAL_InitTypeDef structure defined in @ref CPAL_Dev_TypeDef. */

uint32_t CPAL_I2C_Init			(void);
uint32_t CPAL_I2C_DeInit		(void);
uint32_t CPAL_I2C_StructInit	(void);
uint32_t CPAL_I2C_IsDeviceReady	(void);
uint32_t CPAL_I2C_Write ( const uint8_t* pBuffer, uint16_t WriteAddr, uint32_t NumByteToWrite );
uint32_t CPAL_I2C_Read  ( uint8_t* pBuffer, uint16_t ReadAddr, uint32_t NumByteToRead );
CPAL_StateTypeDef	 CPAL_I2C_GetState	(void);
CPAL_I2CErrorTypeDef CPAL_I2C_GetError	(void);

/*========= CPAL_I2C_User_Callbacks =========*/
/* These functions prototypes only are declared here. User can (optionally) 
   implement the function body in his own application depending on the application needs.
   Each callback is called in a particular situation detailed in the callback description. */

#ifndef CPAL_I2C_TXTC_UserCallback
void CPAL_I2C_TXTC_UserCallback(void);		/*	This function is called when (in DMA or Interrupt mode)
												TX Transfer is complete (to use in DMA mode, Transfer complete 
												interrupt must be enabled) */
#endif

#ifndef CPAL_I2C_RXTC_UserCallback
void CPAL_I2C_RXTC_UserCallback(void);		/*	This function is called when (in DMA or Interrupt mode)
												RX Transfer is complete (to use in DMA mode, Transfer complete 
												interrupt must be enabled) */
#endif

#ifndef CPAL_I2C_DMATXTC_UserCallback
void CPAL_I2C_DMATXTC_UserCallback(void);	/*	This function is called (in DMA mode) when 
												DMA Transmission is finished (If Transfer Complete 
												interrupt is enabled) */
#endif

#ifndef CPAL_I2C_DMARXTC_UserCallback
void CPAL_I2C_DMARXTC_UserCallback(void);	/*	This function is called when (in DMA mode) when 
												DMA Reception is finished (If Transfer Complete 
												interrupt is enabled) */
#endif

/*========= CPAL_User_ErrorCallback_Prototypes =========*/
#ifndef CPAL_I2C_ERR_UserCallback
void CPAL_I2C_ERR_UserCallback(uint32_t DeviceError);	/*	This callback is called when an error 
															occurred on the peripheral while transferring
															(If I2C Error interrupt is enabled). Device 
															instance and error type (DeviceError) are 
															passed as argument. Device_Error value can be
															one of CPAL_I2CErrorTypeDef enumeration */
#endif

#ifdef __cplusplus
}
#endif

#endif /*__CPAL_I2C_H */
