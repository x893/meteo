#include "adc.h"
#include "charger.h"
#include "config.h"

#define CHARGER_CLOCK		24000000UL

#define CHARGER_MIN_FREQ	90
#define CHARGER_MAX_FREQ	120

void ChargerToGpio(void)
{
	CHARGER_PIN_OUT();
	TIM_Cmd(CHARGER_TIM, DISABLE);
}

void ChargerOn(void)
{
	PIN_SET(CHARGER_OUT);
	ChargerToGpio();
}

void ChargerOff(void)
{
	PIN_RESET(CHARGER_OUT);
	ChargerToGpio();
}

void ChargerDeInit(void)
{
	ChargerOff();
	CHARGER_RESET();
}

/**
  *	@brief  Initialize charger (PWM) to frequency
  * @param	frequency (CHARGER_MIN_FREQ <= frequency <= CHARGER_MAX_FREQ
  */
void ChargerInit(uint16_t freqKHz)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	ChargerDeInit();

    /*	The TIM1CLK frequency is set to SystemCoreClock (72 MHz), to get TIM1 counter
		clock at 1 MHz the prescaler is computed as following:
		prescaler = (TIM1CLK / TIM1 counter clock) - 1
	*/
	if (freqKHz >= CHARGER_MIN_FREQ
	&&	freqKHz <= CHARGER_MAX_FREQ
		)
	{
		TIM_TimeBaseStructure.TIM_Period = (CHARGER_CLOCK / 1000UL) / freqKHz;
		TIM_TimeBaseStructure.TIM_Prescaler = (SystemCoreClock / CHARGER_CLOCK) - 1;
		TIM_TimeBaseStructure.TIM_ClockDivision = 0;
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
		TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

		TIM_TimeBaseInit(CHARGER_TIM, &TIM_TimeBaseStructure);
	}
}

/**
  * @brief  Set duty
  * @param  from 0 (off) to 100 (on)
  * @retval
  */
void ChargerDuty(uint8_t dutyPercent)
{
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	if (dutyPercent >= 100)
	{
		ChargerOn();
	}
	else if (dutyPercent > 0)
	{
		TIM_Cmd(CHARGER_TIM, DISABLE);

		CHARGER_PIN_TIMER();

		TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
		TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
		TIM_OCInitStructure.TIM_Pulse = CHARGER_TIM->ARR * ((uint16_t)dutyPercent) / 100;
		TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
		TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCPolarity_High;
		TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
		TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
		
		TIM_OC1Init(CHARGER_TIM, &TIM_OCInitStructure);

		TIM_OC1PreloadConfig(CHARGER_TIM, TIM_OCPreload_Enable);
		TIM_ARRPreloadConfig(CHARGER_TIM, ENABLE);

		TIM_Cmd(CHARGER_TIM, ENABLE);
		TIM_CtrlPWMOutputs(CHARGER_TIM, ENABLE);
	}
	else
	{
		ChargerOff();
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void EnableBALAST(void)
{
	PIN_SET(BALAST);
	DEBUG("BALAST On\n");
}

void DisableBALAST(void)
{
	PIN_RESET(BALAST);
	DEBUG("BALAST Off\n");
}

/**
  * @brief	Check and enter to precharge mode
  *
  */
void ChargerPrecharge(void)
{
	uint8_t current_duty = 0;
	uint8_t new_duty = 0;
	uint16_t delay;

	AdcDisplay();
	if (AdcUbat() < ConfigHost.PreChargeEnter)
	{
		DEBUG("PreCharge mode\n");
		while (1)
		{
			if (AdcUin() >= 1200)
			{
				if (IsPowerSolar())	new_duty = 25;
				else				new_duty = 50;
				if (current_duty != new_duty)
				{
					current_duty = new_duty;
					ChargerDuty(current_duty);
				}
			}
			else
			{
				DEBUG("No External/Solar Power\n");
				ChargerOff();
			}
			while (Debug_IsOutput())
			{
				vTaskDelay(10);
			}
			for (delay = 0; delay < 10; delay++)
			{
				vTaskDelay(1000);
			}
			AdcDisplay();
			if (AdcUbat() >= ConfigHost.PreChargeExit)
				break;
		}
		ChargerDuty(0);
		DEBUG("Precharge mode exit\n");
	}
}

/**
  * @brief	Check ADC values after data collected (from ADC DMA handler)
  *
  */
void ChargerCheck(void)
{
	if (AdcIbat_P() >= ConfigHost.ChargerMaxCurrent)
	{
		ChargerOff();
	}
}
