#include <string.h>

#include "common.h"
#include "board.h"
#include "AT45.h"
#include "FM24.h"
#include "fw_update.h"
#include "MemoryLayout.h"
#include "version.h"

uint8_t FwBuffer[FW_BLOCK_SIZE];
FM24_FW_Info_t FM24_FW_Info_0;
FM24_FW_Info_t FM24_FW_Info_1;

/**
  * @brief	Calculate CRC
  * @param	*src		pointer to data
  *			length		number of uint32_t words
  *			*exclude	exclude address from CRC
  * @retval CRC
  */
uint32_t BlockCRC(void *src, uint16_t length, void *exclude)
{
	register uint32_t *pu32 = (uint32_t *)src;

	CRC_ResetDR();
	while (length != 0)
	{
		length--;
		CRC_CalcCRC((pu32 == (uint32_t *)exclude) ? 0 : *pu32);
		pu32++;
	}
	return CRC_GetCRC();
}

/**
  * @brief	Select valid FW Info block from (fws[0] or fws[1])
  * @param	pointer to array
  * @retval NULL or pointer to actual info block
  */
FM24_FW_Info_t * FwInfoSelect( FM24_FW_Info_t fws[] )
{
	FM24_FW_Info_t * fw = NULL;
	if (BlockCRC(&fws[0], FW_INFO_SIZE_U32, &fws[0].InfoCRC) == fws[0].InfoCRC)
	{
		fw = &fws[0];
		if (BlockCRC(&fws[1], FW_INFO_SIZE_U32, &fws[1].InfoCRC) == fws[1].InfoCRC)
		{
			if (fws[1].InfoVersion > fws[0].InfoVersion)
				fw = &fws[1];
		}
	}
	else
	if (BlockCRC(&fws[1], FW_INFO_SIZE_U32, &fws[1].InfoCRC) == fws[1].InfoCRC)
		fw = &fws[1];

	if (fw != NULL)
	{
		if (fw->FwBlocks > FW_BLOCK_COUNT
		||	fw->FwSize > FLASH_SIZE
		||	(fw->FwFails != 0xFF && fw->FwFails > 0x10)
			)
			fw = NULL;
	}
	return fw;
}

/**
  * @brie
  * @param
  * @retval NO_ERROR or error
  */
void FWDisplayInfo(FM24_FW_Info_t *fw, bool active)
{
	if (fw != NULL)
	{
		if (active)
		{
			DEBUG("* ");
		}
		else
		{
			DEBUG("  ");
		}
		PRINTF("FW-AT45: Version: %d Fails: %d Blocks: %d Size: %d\n",
			fw->FwVersion,
			fw->FwFails,
			fw->FwBlocks,
			fw->FwSize
		);
	}
}

uint32_t * FWFlashBase(void)
{
	return (uint32_t *)FW_BASE;
}

/**
  * @brief	Flash Unlock Bank 1/2
  * @param	address	flash address
  * @retval	none
  */
void FwFlashUnlock(uint32_t address)
{
#ifdef STM32F103VG
	if (address & 0x80000)
		FLASH_UnlockBank2();
	else
		FLASH_UnlockBank1();
#else
	#error "Unknown MCU"
#endif
}

/**
  * @brief	Flash Lock Bank 1/2
  * @param	address	flash address
  * @retval	none
  */
void FwFlashLock(uint32_t address)
{
#ifdef STM32F103VG
	if (address & 0x80000)
		FLASH_LockBank2();
	else
		FLASH_LockBank1();
#else
	#error "Unknown MCU"
#endif
}

/**
  * @brief	Check FW from AT45
  * @param	fw		pointer to descriptor
  *			block0	true for AT45 block 0
  *					false for AT45 block 1
  * @retval NO_ERROR or error
  */
ERROR_t FwWriteFlash(uint32_t address, void * src, uint16_t length)
{
	FLASH_Status status;
	register uint32_t * pu32 = (uint32_t *)src;
	register uint32_t flash_address = address;

	// Compare flash and buffer, skip if equals
	if (memcmp((void *)address, src, length) == 0)
	{
		PRINTF("Skip AT45 to FW 0x%X\n", address);
		return NO_ERROR;
	}

	FwFlashUnlock(address);
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
	if (FLASH_COMPLETE == FLASH_ErasePage(address))
	{
		FwFlashLock(address);
		PRINTF("Erase fail   FW 0x%X\n", address);
		return SYS_FW_ERROR;
	}

	while (length != 0)
	{
		status = FLASH_ProgramWord(flash_address, *pu32++);
		if (status != FLASH_COMPLETE)
			break;
		flash_address += sizeof(uint32_t);
		if (length >= sizeof(uint32_t))
			length = sizeof(uint32_t);
		else
			length = 0;
	}
	FwFlashLock(address);
	if (status == FLASH_COMPLETE
	&&	memcmp((void *)address, src, length) == 0
		)
	{
		PRINTF("Copy AT45 to FW 0x%X\n", address);
		return NO_ERROR;
	}
	PRINTF("Fail AT45 to FW 0x%X\n", address);
	return SYS_FW_ERROR;
}

/**
  * @brief	Check FW from AT45
  * @param	fw		pointer to descriptor
  *			block0	true for AT45 block 0
  *					false for AT45 block 1
  * @retval NO_ERROR or error
  */
ERROR_t FwCheckAT45(FM24_FW_Info_t *fw, bool block0)
{
	register uint32_t address = block0 ? AT45_FW_0_BASE : AT45_FW_1_BASE;
	register uint8_t * flash = (uint8_t *)FWFlashBase();
	register uint16_t length;
	register uint32_t fw_size = fw->FwSize;
	register int idx = 0;

	while (fw_size != 0)
	{
		if (idx >= fw->FwBlocks)
			return SYS_FW_ERROR;

		length = FW_BLOCK_SIZE;
		if (fw_size < length)
			length = fw_size;

		if (NO_ERROR != LogError2("FW-AT45", AT45Read(FwBuffer, address, length)))
			return SYS_FW_ERROR;

		if (fw->FwBlockCRC[idx] != BlockCRC(FwBuffer, length / 4, NULL))
		{
			PRINTF("AT45 Block %d CRC error\n", idx);
			return SYS_FW_ERROR;
		}

		if (NO_ERROR != FwWriteFlash((uint32_t)flash, (uint32_t *)FwBuffer, length))
			return SYS_FW_ERROR;
		idx++;
		fw_size -= length;
		address += length;
		flash += length;
	}
	return NO_ERROR;
}

bool memIsZero(void * src, int size)
{
	register uint8_t * pd = (uint8_t *) src;
	while (size != 0)
	{
		size--;
		if (*pd++ != 0)
			return false;
	}
	return true;
}

/**
  * @brief	Check Main FW with firmware descriptor
  *			Call only from booloader
  * @param
  * @retval NO_ERROR or error
  */
ERROR_t FWCheckUpdate(void)
{
	// Check for Main FW loaded via debugger and clear FW Info 0/1, signature
	if (*FW_NEW_FLAG == 0xBEEFBEEF)
	{	// Main FW loaded from debugger
		DEBUG("Main FW debug signature\n");
		// memset(&FM24_FW_Info_0, 0, sizeof(FM24_FW_Info_t));
		// memset(&FM24_FW_Info_1, 0, sizeof(FM24_FW_Info_t));
		if (NO_ERROR == FM24Write(FM24_OFFSET(FwInfo_0), &FM24_FW_Info_0, sizeof(FM24_FW_Info_t))
		&&	NO_ERROR == FM24Read ( FM24_OFFSET(FwInfo_0), &FM24_FW_Info_0, sizeof(FM24_FW_Info_t))
		&&	NO_ERROR == FM24Write(FM24_OFFSET(FwInfo_1), &FM24_FW_Info_1, sizeof(FM24_FW_Info_t))
		&&	NO_ERROR == FM24Read (FM24_OFFSET(FwInfo_1), &FM24_FW_Info_1, sizeof(FM24_FW_Info_t))
		&&	memIsZero(&FM24_FW_Info_0, sizeof(FM24_FW_Info_t))
		&&	memIsZero(&FM24_FW_Info_1, sizeof(FM24_FW_Info_t))
			)
		{
			FwFlashUnlock((uint32_t)FW_NEW_FLAG);
			FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);	
			FLASH_Status status = FLASH_ProgramWord((uint32_t)FW_NEW_FLAG, 0);
			FwFlashLock((uint32_t)FW_NEW_FLAG);
			if (FLASH_COMPLETE == status && *FW_NEW_FLAG == 0)
				return NO_ERROR;
			DEBUG("FW Debug flag not clear\n");
		}
		return SYS_FW_INVALID;
	}

	register VECTORS_t * fw_vectors = (VECTORS_t *)FWFlashBase();
	if (fw_vectors->Stack <= SRAM_END
	&&	fw_vectors->Stack >  SRAM_BASE
	&&	fw_vectors->Entry >= FW_BASE
	&&	fw_vectors->Entry <  FW_END
		)
	{
		// Simple check for FW validation
		return NO_ERROR;
	}
	return SYS_FW_INVALID;
}

#ifndef BOOTLOADER
#ifdef __CC_ARM
	extern uint32_t __FWRegionTableBase;
	uint32_t FWSize(void)
	{
		register RegionTable_t * table = (RegionTable_t *)__FWRegionTableBase;
		register uint32_t fw_size = (uint32_t)(table->LR_IROM1_Limit);
		fw_size = fw_size - (uint32_t)FWFlashBase();
		return fw_size;
	}
#else
	#error "Not implemented"
#endif
/*
ERROR_t FWCheckCopy(void)
{
	if (NO_ERROR == LogError2("FW-FM24", FM24_Read(FM24_FW_INFO, (uint8_t *)&FM24_FW_InfoAll, sizeof(FM24_FW_InfoAll_t))))
	{
		FM24_FW_Info_t *fw0 = FwInfoSelect(FM24_FW_InfoAll.Fw0);
		FM24_FW_Info_t *fw1 = FwInfoSelect(FM24_FW_InfoAll.Fw1);

		if (fw0 == NULL && fw1 == NULL)
		{
			uint32_t fw_size = FWSize();

			DEBUG("Copy FW to AT45\n");
			PRINTF("FW Size:%d (0x%X)\n", fw_size, fw_size);

			// Copy FW to AT45
			register uint8_t * flash = (uint8_t *)FWFlashBase();
			register uint32_t address = AT45_FW_0_BASE;
			register uint16_t length;
			register FM24_FW_Info_t * fw = &FM24_FW_InfoAll.Fw0[0];

			fw->FwBlocks = 0;
			fw->FwCRC = BlockCRC(flash, fw_size / 4, NULL);
			fw->FwVersion = FW_VERSION_NUMERIC;
			fw->FwFails = 0xFF;
			fw->FwSize = fw_size;
			fw->InfoVersion = 0;
			fw->InfoCRC = 0;

			while (fw_size != 0)
			{
				length = FW_BLOCK_SIZE;
				if (fw_size < length)
					length = fw_size;

				if (NO_ERROR != LogError2("FW-AT45", AT45_Read(FwBuffer, address, length)))
					return SYS_FW_ERROR;

				if (memcmp(FwBuffer, flash, length) != 0)
				{
					if (NO_ERROR != LogError2("FW-AT45", AT45_Write(flash,    address, length))
					||	NO_ERROR != LogError2("FW-AT45", AT45_Read (FwBuffer, address, length))
					||	memcmp(FwBuffer, flash, length) != 0
						)
					{
						DEBUG("Main FW to AT45 copy fail\n");
						return SYS_FW_ERROR;
					}
				}

				PRINTF("FW Block: %d\n", fw->FwBlocks);
				fw->FwBlockCRC[fw->FwBlocks] = BlockCRC(flash, length / 4, NULL);
				fw->FwBlocks++;

				fw_size -= length;
				address += length;
				flash += length;
			}
			fw->InfoCRC = BlockCRC(fw, FW_INFO_SIZE_U32, NULL);

			if (NO_ERROR == LogError2("FW-FM24", FM24_Write(FM24_FW_INFO, (uint8_t *)fw, sizeof(FM24_FW_Info_t))))
				return NO_ERROR;
		}
		else
			return NO_ERROR;
	}
	return SYS_FW_ERROR;
}
*/
#endif
