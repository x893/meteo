#include <stdio.h>
#include <string.h>

#include "common.h"
#include "board.h"
#include "charger.h"
#include "config.h"

#include "FreeRTOS.h"
#include "task.h"

#define ADC_DMA_COUNT	sizeof(AdcValues_t) / sizeof(uint16_t)
AdcContext_t AdcContext;

/**
  * @brief	Calculate UIN voltage in mV
  */
uint16_t AdcUin(void)
{
	uint32_t value = (uint32_t)ConfigLocal.Adc_Uin_Mul;
	value *= (uint32_t)AdcContext.AvgValues.Adc_Uin;
	return (uint16_t)(value / (uint32_t)ConfigLocal.Adc_Uin_Div);
}

/**
  * @brief	Calculate UBAT voltage in mV
  */
uint16_t AdcUbat(void)
{
	uint32_t value = (uint32_t)ConfigLocal.Adc_Ubat_Mul;
	value *= (uint32_t)AdcContext.AvgValues.Adc_Ubat;
	return (uint16_t)(value / (uint32_t)ConfigLocal.Adc_Ubat_Div);
}

/**
  * @brief	Calculate IBAT discharge current in mA
  */
uint16_t AdcIbat_M(void)
{
	uint32_t value = (uint32_t)ConfigLocal.Adc_Ibat_M_Mul;
	value *= (uint32_t)AdcContext.AvgValues.Adc_Ibat_M;
	return (uint16_t)(value / (uint32_t)ConfigLocal.Adc_Ibat_M_Div);
}

/**
  * @brief	Calculate IBAT charge current in mA
  */
uint16_t AdcIbat_P(void)
{
	uint32_t value = (uint32_t)ConfigLocal.Adc_Ibat_P_Mul;
	value *= (uint32_t)AdcContext.AvgValues.Adc_Ibat_P;
	return (uint16_t)(value / (uint32_t)ConfigLocal.Adc_Ibat_P_Div);
}

/**
  * @brief	Get pointer to last ADC values
  * @param
  * @retval	pointer to values
  */
AdcValues_t * AdcLastValue(void)
{
	int idx = AdcContext.AvgIndex;
	if (idx == 0)
		idx = ADC_AVERAGE_COUNT - 1;
	else
		--idx;
	return &AdcContext.Values[idx];
}

/**
  * @brief	Calculate average values for ADC
  * @param
  * @retval	pointer to avarage values
  */
AdcValues_t * AdcAverage(void)
{
	register AdcValues_t * values = &AdcContext.Values[0];
	register AdcValues_t * avg_vals = &AdcContext.AvgValues;

	register uint32_t avg_U_IN = 0;
	register uint32_t avg_I_IN = 0;
	register uint32_t avg_UBAT = 0;
	register uint32_t avg_IBAT_P = 0;
	register uint32_t avg_IBAT_M = 0;

	while (values != &AdcContext.Values[ADC_AVERAGE_COUNT])
	{
		avg_U_IN += (uint32_t)values->Adc_Uin;
		avg_I_IN += (uint32_t)values->Adc_Iin;
		avg_UBAT += (uint32_t)values->Adc_Ubat;
		avg_IBAT_P += (uint32_t)values->Adc_Ibat_P;
		avg_IBAT_M += (uint32_t)values->Adc_Ibat_M;
		values++;
	}

	avg_vals->Adc_Uin = avg_U_IN / ADC_AVERAGE_COUNT;
	avg_vals->Adc_Iin = avg_I_IN / ADC_AVERAGE_COUNT;
	avg_vals->Adc_Ubat = avg_UBAT / ADC_AVERAGE_COUNT;
	avg_vals->Adc_Ibat_P = avg_IBAT_P / ADC_AVERAGE_COUNT;
	avg_vals->Adc_Ibat_M = avg_IBAT_M / ADC_AVERAGE_COUNT;

	return avg_vals;
}

/**
  * @brief	Start ADC conversion - DMA store results to RAM
  * @param
  * @retval
  */
void AdcStart(void)
{
	// Stop DMA
	DMA_Cmd(ADC_DMA_CH, DISABLE);
	AdcContext.Ready = false;

	// Set memory address for DMA
	ADC_DMA_CH->CNDTR = ADC_DMA_COUNT;
	ADC_DMA_CH->CMAR = (uint32_t)&AdcContext.Values[AdcContext.AvgIndex++];
	if (AdcContext.AvgIndex >= ADC_AVERAGE_COUNT)
		AdcContext.AvgIndex = 0;

	DMA_Cmd(ADC_DMA_CH, ENABLE);
	ADC_SoftwareStartConvCmd(ADC_PORT, ENABLE);
}

/**
  * @brief	DMA IRQ Handler. Complete after all ADC channels stored to RAM
  * @param
  * @retval
  */
void ADC_DMA_IRQHandler(void)
{
	if (DMA_GetITStatus(ADC_DMA_IT_TC))
	{
		DMA_ClearITPendingBit(ADC_DMA_IT_GL);
		DMA_Cmd(ADC_DMA_CH, DISABLE);
		AdcContext.Ready = true;

		AdcAverage();
		ChargerCheck();

		if (AdcContext.Count != 0)
			AdcContext.Count--;
	}
}

const PinInit32_t AdcDmaInit32[] =
{
	{	&(ADC_DMA->IFCR),	0,	(DMA_ISR_GIF1 | DMA_ISR_TCIF1 | DMA_ISR_HTIF1 | DMA_ISR_TEIF1)	},
	{	&(ADC_DMA_CH->CCR),	0,	(	0
									| DMA_DIR_PeripheralSRC
									| DMA_Mode_Normal
									| DMA_PeripheralInc_Disable
									| DMA_MemoryInc_Enable
									| DMA_PeripheralDataSize_HalfWord
									| DMA_MemoryDataSize_HalfWord
									| DMA_Priority_High
									| DMA_M2M_Disable
									| DMA_IT_TC			// Enable ADC_DMA_CH Transfer Complete interrupt
								)
	},
	{	&(ADC_DMA_CH->CNDTR),	0,	0			},
	{	&(ADC_DMA_CH->CPAR),	0,	ADC_PORT_DR	},
	{	&(ADC_DMA_CH->CMAR),	0,	0			},
	NULL
};

/**
  * @brief	Initialize ADC and DMA
  * @param
  * @retval
  */
/*
void AdcDmaInit(void)
{
	DMA_InitTypeDef DMA_InitStructure;

	DMA_DeInit(ADC_DMA_CH);

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(ADC_PORT->DR);
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(ADC_DMA_CH, &DMA_InitStructure);
	
	DMA_ITConfig(ADC_DMA_CH, DMA_IT_TC, ENABLE);

}
*/

/**
  * @brief	Initialize ADC and DMA
  * @param
  * @retval
  */
ERROR_t AdcInit(void)
{
	ADC_InitTypeDef ADC_InitStructure;
//
//	ADC_U_IN		PA1		ADC1_IN1
//	ADC_I_IN		PA2		ADC1_IN2
//	ADC_UBAT		PB0		ADC1_IN8
//	ADC_IBAT_P		PB1		ADC1_IN9
//	ADC_IBAT_M		PC4		ADC1_IN14

	// DMA Channel Configuration
	PinInit32(&AdcDmaInit32[0]);
	
	// ADC configuration
	ADC_InitStructure.ADC_Mode = ADC_Mode_RegSimult;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 5;
	ADC_Init(ADC_PORT, &ADC_InitStructure);

	// ADC regular channels configuration
	ADC_RegularChannelConfig(ADC_PORT, ADC_U_IN_CH,   1, ADC_SAMPLING);
	ADC_RegularChannelConfig(ADC_PORT, ADC_I_IN_CH,   2, ADC_SAMPLING);
	ADC_RegularChannelConfig(ADC_PORT, ADC_UBAT_CH,   3, ADC_SAMPLING);
	ADC_RegularChannelConfig(ADC_PORT, ADC_IBAT_P_CH, 4, ADC_SAMPLING);
	ADC_RegularChannelConfig(ADC_PORT, ADC_IBAT_M_CH, 5, ADC_SAMPLING);

	ADC_Cmd(ADC_PORT, ENABLE);

	// Enable ADC reset calibration register and wait complete
	ADC_ResetCalibration(ADC_PORT);
	while (ADC_GetResetCalibrationStatus(ADC_PORT))
		;

	// Start ADC calibration and wait complete
	ADC_StartCalibration(ADC_PORT);
	while (ADC_GetCalibrationStatus(ADC_PORT))
		;

	ADC_DMACmd(ADC_PORT, ENABLE);

	NVIC_ClearPendingIRQ(ADC_DMA_IRQn);
	NVIC_EnableIRQ(ADC_DMA_IRQn);

	//	ADC1 clocks already set. See board.h
	AdcContext.AvgIndex = 0;
	do
	{
		int timeout = 1000;
		AdcStart();
		while (AdcContext.Ready == false && timeout != 0)
			--timeout;
		if (timeout == 0)
		{	// Error on ADC/DMA
			AdcContext.Disable = true;
			DMA_Cmd(ADC_DMA_CH, DISABLE);
			ADC_DMACmd(ADC_PORT, DISABLE);
			ADC_Cmd(ADC_PORT, DISABLE);
			return SYS_ADC_ERROR;
		}
	} while (AdcContext.AvgIndex != 0);

	return NO_ERROR;
}

/**
  * @brief	Display ADC values
  * @param
  * @retval
  */
void AdcDisplay(void)
{
	// ADC already disabled (failure)
	if (AdcContext.Disable == false)
	{
		register AdcValues_t * values = &AdcContext.AvgValues;

		PRINTF("ADC  Avg: U_IN=%d (%f V) I_IN=%d UBAT=%d (%f V) IBAT_P=%d (%d mA) IBAT_M=%d (%d mA)\n",
			values->Adc_Uin,
			((float)AdcUin()) / 100.0,
			values->Adc_Iin,
			values->Adc_Ubat,
			((float)AdcUbat()) / 100.0,
			values->Adc_Ibat_P,
			AdcIbat_P(),
			values->Adc_Ibat_M,
			AdcIbat_M()
		);
		values = AdcLastValue();
		PRINTF("ADC Last: U_IN=%d I_IN=%d UBAT=%d IBAT_P=%d IBAT_M=%d\n",
			values->Adc_Uin,
			values->Adc_Iin,
			values->Adc_Ubat,
			values->Adc_Ibat_P,
			values->Adc_Ibat_M
		);
	}
}

/**
  * @brief	Measure ADC samples
  * @param
  * @retval
  */
ERROR_t AdcMeasure(void)
{
	register int16_t delay = 10;

	AdcContext.Count = ADC_AVERAGE_COUNT;
	while (AdcContext.Count != 0)
	{
		if (delay == 0)
		{
			LogError2("ADC", SYS_ADC_FAILED);
			return SYS_ADC_FAILED;
		}
		delay--;
		vTaskDelay(100);
	}
	AdcDisplay();
	return NO_ERROR;
}

void DisableHEAT(void);
void DisableBALAST(void);

/**
  * @brief	Calibrate ADC values
  * @param
  * @retval
  */
void AdcCalibrate(void)
{
	if (!AdcContext.Disable)	// ADC failure
	{
		DEBUG("Calibration mode\n");

		ChargerOff();
		DisableHEAT();
		DisableBALAST();

		vTaskDelay(5000);
		while (IsCalibrate())
		{
			if (NO_ERROR != AdcMeasure())
				return;

			AdcContext.Disable = true;
			// Save voltage value for Uin calibration
			ConfigLocal.Adc_Uin_Mul = ADC_CALIBRATE_UIN;
			ConfigLocal.Adc_Uin_Div = AdcContext.AvgValues.Adc_Uin;
			ConfigLocal.Adc_Ubat_Mul = ADC_CALIBRATE_UBAT;
			ConfigLocal.Adc_Ubat_Div = AdcContext.AvgValues.Adc_Ubat;
			ConfigChanged();
			AdcContext.Disable = false;

			LEDS_Toggle(LED_2G);
		}
		if (NO_ERROR == ConfigSave())
		{
			DEBUG("Calibration complete\n");
			LEDS_Set(LED_ALL);
		}
		return;
	}
	LogError2("ADC", SYS_ADC_FAILED);
}
