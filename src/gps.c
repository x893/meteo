#include <string.h>
#include <time.h>
#include <math.h>

#include "common.h"
#include "board.h"
#include "rtc.h"
#include "RingBuffer.h"
#include "config.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

enum
{
	GPS_MODE_IGNORE	= 0,
	GPS_MODE_NMEA,
	GPS_MODE_PREFIX,
	GPS_MODE_EOL,
	GPS_MODE_READY,
};

typedef struct
{
	uint32_t	IntegerPart;
	uint32_t	DecimalPart;
	uint32_t	DecimalMul;
	char		NSEW;
} GpsDecimal_t;

typedef struct
{
	xSemaphoreHandle ReadySemaphore;
	QueueHandle_t	TaskQueue;

			double	LatitudeAvg;
			double	LongitudeAvg;
	GpsDecimal_t	Latitude;
	GpsDecimal_t	Longitude;

			time_t	UTC;
		uint16_t	RxIndex;
			uint8_t	Count;
			uint8_t	CountValid;
			uint8_t	State;
			uint8_t	Mode;
			bool	Valid;
		struct tm	UTC_Time;
} GpsContext_t;

const PinInit32_t GpsPinInit[] =	{	GPS_INIT		{ NULL }};
const PinInit16_t GpsUartInit[] =	{	GPS_UART_INIT	{ NULL }};
const PinInit32_t GpsPinDeinit[] =	{	GPS_DEINIT		{ NULL }};

const char GPS_GPRMC[] = "$GPRMC,";

char GpsBufferRx[256];
GpsTask_t GpsTaskCurrent;
GpsContext_t GpsContext;

/**
  * @brief
  * @param
  * @retval
  */
void GpsInit(register GpsContext_t *gc)
{
	gc->State = GPS_MODE_IGNORE;
	PinInit32(GpsPinInit);
	PinInit16(GpsUartInit);
}

/**
  * @brief
  * @param
  * @retval
  */
void GpsDeinit(register GpsContext_t *gc)
{
	gc->State = GPS_MODE_IGNORE;
	PinInit32(GpsPinDeinit);
}

/**
  * @brief
  * @param	
  * @retval
  */
char * SkipToComma(register char * token)
{
	if (token != NULL)
	{
		register char ch;
		while ((ch = *token++) != '\0')
		{
			if (ch == ',' || ch == '*')
				return token;
		}
		token = NULL;
	}
	return token;
}

/**
  * @brief
  * @param	
  * @retval
  */
bool GpsParseDecimal(register char * token, register GpsDecimal_t *value)
{
	register char ch;
	register int mult = 0;
	value->IntegerPart = value->DecimalMul = value->DecimalPart = 0;
	while (1)
	{
		ch = *token++;
		if (ch == ',' || ch == '\0')
			break;
		if (mult == 0 && ch == '.')
		{
			mult = 1;
			continue;
		}
		if (ch >= '0' && ch <= '9')
		{
			ch -= '0';
			if (mult == 0)
			{
				value->IntegerPart = (value->IntegerPart * 10) + ch;
			}
			else
			{
				mult = mult * 10;
				value->DecimalPart = (value->DecimalPart * 10) + ch;
			}
		}
		else
			return false;
	}
	value->DecimalMul = mult;
	return true;
}

/**
  * @brief
  * @param	
  * @retval
  */
bool GpsParseTime(register char * token, register GpsContext_t *gc)
{
	GpsDecimal_t time;
	if (GpsParseDecimal(token, &time))
	{
		gc->UTC_Time.tm_sec  = (time.IntegerPart % 100);
		gc->UTC_Time.tm_min  = (time.IntegerPart / 100) % 100;
		gc->UTC_Time.tm_hour = (time.IntegerPart / 10000);
		return true;
	}
	return false;
}

/**
  * @brief
  * @param	
  * @retval
  */
bool GpsParseDate(register char * token, register GpsContext_t *gc)
{
	GpsDecimal_t time;
	if (GpsParseDecimal(token, &time))
	{
		gc->UTC_Time.tm_year = (time.IntegerPart % 100) + 100;
		gc->UTC_Time.tm_mon  = ((time.IntegerPart / 100) % 100) - 1;
		gc->UTC_Time.tm_mday = (time.IntegerPart / 10000);
		gc->UTC = mktime(&gc->UTC_Time);
		return true;
	}
	return false;
}

/**
  * @brief
  * @param	
  * @retval
  */
bool GpsParseValid(register char * token, register GpsContext_t *gc)
{
	gc->Valid = false;
	if (*token == 'A')
		gc->Valid = true;
	else if (*token != 'V')
		return false;
	return true;
}

/**
  * @brief
  * @param	
  * @retval
  */
bool GpsParseMode(register char * token, register GpsContext_t *gc)
{
	if (*token == 'N'
	||	*token == 'A'
	||	*token == 'D')
	{
		gc->Mode = *token;
		return true;
	}
	return false;
}

/**
  * @brief
  * @param	
  * @retval
  */
bool GpsParseNSEW(register char * token, register GpsDecimal_t *value)
{
	if (*token == 'N'
	||	*token == 'S'
	||	*token == 'E'
	||	*token == 'W'
		)
	{
		value->NSEW = *token;
		return true;
	}
	return false;
}

/**
  * @brief
  * @param	
  * @retval
  */
uint32_t atoi(register char * src)
{
	register uint32_t value = 0;
	register char ch;
	while ((ch = *src++) != '\0')
	{
		if (ch >= '0' && ch <= '9')
			value = (value << 4) | (ch - '0');
		else if (ch >= 'A' && ch <= 'F')
			value = (value << 4) | (ch - 'A' + 0x0A);
		else if (ch >= 'a' && ch <= 'f')
			value = (value << 4) | (ch - 'a' + 0x0A);
		else
			break;
	}
	return value;
}

/**
  * @brief	Check GPS checksum
  * @param	
  * @retval
  */
bool GpsCheckCsum(register char *src)
{
	register uint8_t csum = (uint8_t)(0 ^ '$');
	register char ch;

	while ((ch = *src++) != '*')
	{
		if (ch == '\0')
			return false;
		csum ^= ch;
	}
	return (csum == (uint8_t)atoi(src));
}

/**
  * @brief	Parse GPS RMC data
  * @param	*token	pointer to data
  * @retval	success
  */
bool GpsParse(register char *token, register GpsContext_t *gc)
{
	register uint8_t ntoken = 1;
	gc->Valid = false;
	if (!GpsCheckCsum(token))
		return false;

	while (ntoken < 14)
	{
		if (*token != ',')
		{
			if ((ntoken == 2 && !GpsParseTime(token, gc))
			||	(ntoken == 3 && !GpsParseValid(token, gc))
			||	(ntoken == 4 && !GpsParseDecimal(token, &gc->Latitude ))
			||	(ntoken == 5 && !GpsParseNSEW(token,  &gc->Latitude ))
			||	(ntoken == 6 && !GpsParseDecimal(token, &gc->Longitude))
			||	(ntoken == 7 && !GpsParseNSEW(token,  &gc->Longitude))
			||	(ntoken == 10 && !GpsParseDate(token,  gc))
			||	(ntoken == 13 && !GpsParseMode(token,  gc))
				)
				return false;
		}
		if ((token = SkipToComma(token)) == NULL)
			return false;
		++ntoken;
	}
	return true;
}

/**
 * @brief	Add GPS Task
 * @param
 * @retval
 */
ERROR_t GpsAddTask(GPS_COMMAND_t cmd, const char * data, pf_GPS_Callback pfCallback)
{
	if (GpsContext.TaskQueue != NULL)
	{
		GpsTask_t task;
		task.pfCallback = pfCallback;
		task.TaskData.Command = cmd;
		task.TaskData.Data = data;
		task.TaskData.Error = SYS_OS_TASK_ADD;
		if (xQueueSend( GpsContext.TaskQueue, &task, 500 ) == pdTRUE)
			return NO_ERROR;
	}
	return GPS_TASK_FAIL;
}

#define NMEA_PI				(3.141592653589793)
#define NMEA_PI180			(NMEA_PI / 180.0)
#define NMEA_EARTHRADIUS_KM	(6378.0)
#define NMEA_EARTHRADIUS_M	(NMEA_EARTHRADIUS_KM * 1000.0)

double GpsDegree2Radian(double val)
{
	return (val * NMEA_PI180);
}


/**
  * @brief	Calculate distance between two points
  * @param	pointers to positions
  * @retval	Distance in meters
  */
double GpsDistance(
	register double * from_lat,
	register double * from_lon,
	register double * to_lat,
	register double * to_lon
	)
{
	register double tlat = GpsDegree2Radian(*from_lat);
	register double flat = GpsDegree2Radian(*to_lat);
    register double dist = ((double)NMEA_EARTHRADIUS_M)
		* acos(
			sin(tlat) * sin(flat) +
			cos(tlat) * cos(flat) * cos(GpsDegree2Radian(*to_lon) - GpsDegree2Radian(*from_lon))
        );
    return dist;
}

/**
  * @brief	Convert fractional degree to NDEG (NMEA degree)
  * @param	pointer to GpsDecimal structure
  * @retval
  */
double GpsNDeg2Degree(register GpsDecimal_t * point)
{
	double deg;
	if (point->DecimalMul == 0)
	{
		point->DecimalMul = 1;
		point->DecimalPart = 0;
	}

	deg = ((double)point->DecimalPart) / ((double)point->DecimalMul);
	deg += (double)(point->IntegerPart % 100);
	deg /= 60.0;
	deg += (double)(point->IntegerPart / 100);
	if (point->NSEW == 'S'
	||	point->NSEW == 'W'
		)
		deg = -deg;
	return deg;
}

/**
  * @brief
  * @param
  * @retval
  */
void GpsTask(void *pvArgs)
{
	register GpsContext_t *gc = &GpsContext;
	(void)pvArgs;

	if (NULL == (gc->TaskQueue = xQueueCreate(GPS_TASK_QUEUE_SIZE, sizeof(GpsTask_t)))
	||	NULL == (gc->ReadySemaphore = xSemaphoreCreateBinary())
	||	NO_ERROR != GpsAddTask(GPS_START, NULL, NULL)
		)
	{
		LogError2("GPS", GPS_TASK_FAIL);
		while (1)
			vTaskSuspend(NULL);
	}

	while (1)
	{
		if (xQueueReceive(gc->TaskQueue, &GpsTaskCurrent, GPS_TASK_TIMEOUT) != pdPASS)
		{	// No task or STOP, Power off GPS
			GpsDeinit(gc);
			continue;
		}

		register GpsTaskData_t * pd = &GpsTaskCurrent.TaskData;
		if (pd->Command == GPS_STOP)
		{
			GpsDeinit(gc);
			pd->Error = NO_ERROR;
		}
		else if (pd->Command == GPS_START)
		{
			GpsInit(gc);
			gc->Count = 0;
			gc->CountValid = 0;
			while  (gc->Count++ < GPS_SAMPLES_TOTAL)
			{
				gc->State = GPS_MODE_NMEA;
				if (xSemaphoreTake(gc->ReadySemaphore, 5000) == pdFALSE)
				{
					pd->Error = LogError2("GPS no data", GPS_NO_DATA);
					break;
				}
				if (gc->State == GPS_MODE_READY)
				{
					if (GpsParse(&GpsBufferRx[0], gc))
					{
						if (gc->Valid)
						{

							RtcSetDatetime(gc->UTC + GPS_TIMEZONE_OFFSET);
							if (gc->CountValid == 0)
							{	// Store first fix point
								gc->LatitudeAvg  = GpsNDeg2Degree(&gc->Latitude);
								gc->LongitudeAvg = GpsNDeg2Degree(&gc->Longitude);
								++gc->CountValid;
							}
							else if (gc->CountValid < GPS_SAMPLES_VALID)
							{
								gc->LatitudeAvg  *= (double)gc->CountValid;
								gc->LongitudeAvg *= (double)gc->CountValid;
								gc->LatitudeAvg  += GpsNDeg2Degree(&gc->Latitude);
								gc->LongitudeAvg += GpsNDeg2Degree(&gc->Longitude);
								++gc->CountValid;
								gc->LatitudeAvg  /= (double)gc->CountValid;
								gc->LongitudeAvg /= (double)gc->CountValid;
							}
							else
								break;
						}
					}
					else
					{
						DEBUG("GPS Parse error\n");
						DEBUG(GpsBufferRx);
						DEBUG("\n");
					}
				}
			}

			if (gc->CountValid != 0)
			{
				double dist = GpsDistance(
									&gc->LatitudeAvg,
									&gc->LongitudeAvg,
									&Parameters.Latitude,
									&Parameters.Longitude
								);
				if (fabs(dist) >= GPS_DISTANCE_CHANGE)
				{	// Position change, add to log and save new position
					Parameters.Latitude = gc->LatitudeAvg;
					Parameters.Longitude = gc->LongitudeAvg;
					ConfigChanged();
					ConfigSave();
				}
			}
			else
			{	// Position not fixed
				pd->Error = LogError2("GPS", GPS_NO_FIX);
			}
		}
		// Call task callback (if not NULL)
		if (GpsTaskCurrent.pfCallback != NULL)
			(GpsTaskCurrent.pfCallback)(pd);
	}
}

void GPS_IRQHandle(void)
{
	register USART_TypeDef *usart = GPS_UART;
	register GpsContext_t  *gc = &GpsContext;

	if (usart->SR & USART_SR_RXNE)
	{
		register uint8_t ch = usart->DR;
		if (usart->SR & (USART_SR_PE | USART_SR_FE | USART_SR_NE | USART_SR_ORE))
		{
			ch = usart->DR;
			if (gc->State != GPS_MODE_IGNORE)
				gc->State = GPS_MODE_NMEA;
		}
		else if (gc->State == GPS_MODE_EOL)
		{
			if (ch != '\r')
			{
				if (ch == '\n')
					ch = 0;
				GpsBufferRx[gc->RxIndex++] = ch;

				if (gc->RxIndex >= (sizeof(GpsBufferRx) - 2))
				{	// GPS string too long, ignore it
					gc->State = GPS_MODE_NMEA;
				}
				else if (ch == '\0')
				{
					portBASE_TYPE RxNeedTaskSwitch = pdFALSE;
					gc->State = GPS_MODE_READY;
					xSemaphoreGiveFromISR( gc->ReadySemaphore, &RxNeedTaskSwitch );
					portEND_SWITCHING_ISR( RxNeedTaskSwitch );
				}
			}
		}
		else if (gc->State == GPS_MODE_NMEA)
		{
			if (ch == '$')
			{
				gc->State = GPS_MODE_PREFIX;
				gc->RxIndex = 0;
				GpsBufferRx[gc->RxIndex++] = ch;
			}
		}
		else if (gc->State == GPS_MODE_PREFIX)
		{
			GpsBufferRx[gc->RxIndex++] = ch;
			if (gc->RxIndex == STRING_SIZE(GPS_GPRMC))
			{
				if (strncmp(GpsBufferRx, GPS_GPRMC, STRING_SIZE(GPS_GPRMC)) == 0)
					gc->State = GPS_MODE_EOL;
				else
					gc->State = GPS_MODE_NMEA;
			}
		}
	}
}
