#include "common.h"
#include "board.h"
#include "AT45.h"
#include "FM24.h"
#include "rtc.h"
#include "fw_update.h"

void HardwareInit(void);
void SystemInit(void);

typedef void (*pFunction)(void);

/**
  * @brief
  * @param
  * @retval
  */
int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	HardwareInit();
	DelayMilli(1000);

	LEDS_Off(LED_ALL);

	DEBUG("\nBL "BL_VERSION"\n");

	LogError2("AT45", AT45Init());
	LogError2("LOG", LogInit());
	LogError2("FM24", FM24Init());
	LogError2("RTC", RtcInit());

	LogAdd(NO_ERROR, RtcGetDatetime(), "BL "BL_VERSION);

	LogError2("HSE", ((RCC->CR & RCC_CR_HSERDY)  == RCC_CR_HSERDY   ) ? NO_ERROR : SYS_FATAL_ERR);
	LogError2("PLL", ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL) ? NO_ERROR : SYS_FATAL_ERR);

	if (NO_ERROR == LogError2("FW", FWCheckUpdate()))
	{
		register VECTORS_t * vectors = (VECTORS_t *)(FW_BASE);
		SCB->VTOR = (uint32_t)vectors;
		__set_MSP(vectors->Stack);			// Initialize user application's Stack Pointer
		((pFunction)(vectors->Entry))();	// Jump to application
	}

	DEBUG("FW fail, rebot after 30 seconds\n");
	LEDS_Set(LED_ALL);
	DelayMilli(1000);
	LEDS_Off(LED_ALL);
	DelayMilli(30000);

	while(1)
	{
		// PWR_EnterSTANDBYMode();
	}
}
