#include "common.h"
#include "board.h"
#include "AT45.h"

#define AT45_CHIPID_mask	0x0000FFFF
#define AT45_CHIPID			0x0000261F	/*	Manufacturer ID:1F Atmel		*/
										/*	DevideID (1):	20 DataFlash	*/
										/*					06 16 M-Bit		*/
										/*					06 16 M-Bit		*/
										/*	DevideID (2):	ignored			*/
										/*	DevideID (3):	ignored			*/

#define AT45_CMD_SRRD		0xD7	/* Status Register Read				*/
#define AT45_CMD_MDID		0x9F	/* Manufacturer and Device ID Read	*/
#define AT45_CMD_DPD		0xB9	/* Deep Power-down					*/
#define AT45_CMD_RDPD		0xAB	/* Resume from Deep Power-down		*/

#define AT45_CMD_MTB1		0x53
#define AT45_CMD_MTB2		0x55

#define AT45_CMD_CARL		0xE8
#define AT45_CMD_CARH		0x0B
#define AT45_CMD_RDID		0x03
#define AT45_CMD_MMPR		0xD2
#define AT45_CMD_BF1R		0xD4
#define AT45_CMD_BF2R		0xD6
#define AT45_CMD_BF1W		0x84
#define AT45_CMD_BF2W		0x87
#define AT45_CMD_B1TMW		0x83
#define AT45_CMD_B2TMW		0x86
#define AT45_CMD_B1TMO		0x88
#define AT45_CMD_B2TMO		0x89
#define AT45_CMD_ERPG		0x81
#define AT45_CMD_ERBL		0x50
#define AT45_CMD_ERSC		0x7C
#define AT45_CMD_APR1		0x58
#define AT45_CMD_APR2		0x59

//
//! Status Register Bits
//
#define AT45_IDLE			0x80
#define AT45_COMP			0x40
#define AT45_DENSITY_mask	0x3C
#define AT45_DENSITY		0x2C
#define AT45_PROTECT		0x02
#define AT45_PGSZ			0x01

#define AT45_IDLE_value		(AT45_IDLE | AT45_DENSITY)
#define AT45_IDLE_mask		(AT45_IDLE | AT45_DENSITY_mask)

#define AT45_PAGE_SIZE_DEFAULT	528
#define AT45_PAGE_SIZE_BINARY	512
#define AT45_PAGES				4096UL
#define AT45_BLOCKS				512
#define AT45_SECTORS			16
#define AT45_BUF1				1
#define AT45_BUF2				2

/**
  * @brief	Set Deep Power-down
  *			The Deep Power-down command allows the device to enter
  *			into the lowest power consumption mode
  * @param
  * @retval
  */
#define AT45_DeepPowerDown()	AT45_WriteByteStartEnd( AT45_CMD_DPD )

/**
  * @brief	Resume Deep Power-down
  *			The Resume from Deep Power-down command takes the device out of the 
  *			Deep Power-down mode and returns it to the normal standby mode
  * @param
  * @retval
  */
#define AT45_ResumeDeepPowerDown()	AT45_WriteByteStartEnd( AT45_CMD_RDPD )

#define AT45_ReadByte()		AT45_WriteByte(0xFF)

const uint8_t AT45_CMD_CPER[4] = { 0xC7, 0x94, 0x80, 0x9A };
const uint8_t AT45_CMD_ESSP[4] = { 0x3D, 0x2A, 0x7F, 0xA9 };
const uint8_t AT45_CMD_DSSP[4] = { 0x3D, 0x2A, 0x7F, 0x9A };
const uint8_t AT45_CMD_ESPR[4] = { 0x3D, 0x2A, 0x7F, 0xCF };
const uint8_t AT45_CMD_PSPR[4] = { 0x3D, 0x2A, 0x7F, 0xFC };
const uint8_t AT45_CMD_RSPR[4] = { 0x32, 0xFF, 0xFF, 0xFF };
const uint8_t AT45_CMD_SCLD[4] = { 0x3D, 0x2A, 0x7F, 0x30 };
const uint8_t AT45_CMD_PGCR[4] = { 0x3D, 0x2A, 0x80, 0xA6 };
const uint8_t AT45_PAR_0000[4] = { 0x00, 0x00, 0x00, 0x00 };

const SPI_InitTypeDef AT45_SPI_Init =
{
	SPI_Direction_2Lines_FullDuplex,
	SPI_Mode_Master,
	SPI_DataSize_8b,
	SPI_CPOL_High,
	SPI_CPHA_2Edge,
	SPI_NSS_Soft,
	SPI_BaudRatePrescaler_8,
	SPI_FirstBit_MSB,
	7
};

/**
  * @brief
  * @param
  * @retval
  */
uint8_t AT45_WriteByte(uint8_t data)
{
	while ((AT45_SPI->SR & SPI_SR_TXE) == RESET)
		;
	AT45_SPI->DR = data;

	while ((AT45_SPI->SR & SPI_SR_RXNE) == RESET)
		;
	return AT45_SPI->DR;
}

/**
  * @brief
  * @param
  * @retval
  */
uint8_t AT45_ReadByteEnd(void)
{
	uint8_t data = AT45_ReadByte();
	AT45_CS_HIGH();
	return data;
}

/**
  * @brief
  * @param
  * @retval
  */
uint8_t AT45_WriteByteStart(uint8_t data)
{
	(void)AT45_SPI->DR;
	AT45_CS_LOW();
	return AT45_WriteByte(data);
}

/**
  * @brief
  * @param
  * @retval
  */
uint8_t AT45_WriteByteStartEnd(uint8_t data)
{
	data = AT45_WriteByteStart(data);
	AT45_CS_HIGH();
	return data;
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_SendPageAddress(uint8_t cmd, uint16_t page, uint16_t offset)
{
	AT45_CS_LOW();
	AT45_WriteByteStart(cmd);
	AT45_WriteByte(page >> 6);
	AT45_WriteByte(((page & 0x3F) << 2) | (offset >> 8));
	AT45_WriteByte(offset & 0xFF0);
}

/**
  * @brief	Read AT45DB161 state register
  * @param
  * @retval	status register or 0
  */
uint8_t AT45_GetState(void)
{
	AT45_WriteByteStart(AT45_CMD_SRRD);
	return AT45_ReadByteEnd();
}

/**
  * @brief	To check and wait device ready
  *			This function is to check and wait device ready. If the device still don't get
  *			ready after 300 cycles of retry, the function will return 0 for not ready,
  *			otherwise return 1 for ready.
  * @param
  * @retval
  */
ERROR_t AT45_WaitReadyEx(register uint16_t retry)
{
	while ((AT45_GetState() & AT45_IDLE_mask) != AT45_IDLE_value && retry != 0)
		retry--;
   	return (retry == 0) ? AT45_NOT_READY : NO_ERROR;
}

ERROR_t AT45_WaitReady(void)
{
	return AT45_WaitReadyEx(500);
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_ReadBytes(uint8_t * dst, uint16_t length)
{
	while (length != 0)
	{
		length--;
		*dst++ = AT45_ReadByte();
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_ReadBytesEnd(uint8_t * dst, uint16_t length)
{
	AT45_ReadBytes(dst, length);
	AT45_CS_HIGH();
}

/**
  * @brief	Read AT45DB161 chip ID
  * @param
  * @retval
  */
uint32_t AT45_GetChipID(void)
{
	uint32_t chipId;
	AT45_WriteByteStart(AT45_CMD_MDID);
	AT45_ReadBytesEnd((uint8_t *)&chipId, 4);
	return chipId;
}

/**
  * @brief
  * @param
  * @retval
  */
ERROR_t AT45Init(void)
{
	SPI_I2S_DeInit(AT45_SPI);
	SPI_Init(AT45_SPI, (SPI_InitTypeDef *)&AT45_SPI_Init);
	SPI_Cmd(AT45_SPI, ENABLE);

	AT45_CS_HIGH();
	AT45_ReadByte();
	DelayMicro(10);

	AT45_ResumeDeepPowerDown();

	// Check AT45DB161 state register to get density
	// Check AT45DB161 Chip ID
	if (NO_ERROR == AT45_WaitReady()
	&&	AT45_DENSITY == (AT45_GetState()  & AT45_DENSITY_mask)
	&&	AT45_CHIPID  == (AT45_GetChipID() & AT45_CHIPID_mask)
		)
		return NO_ERROR;

	// AT45 failed, change output frequency to 2 MHz (from 50 MHz)
	PIN_MODE_SET(PIN_OUT_PP_2, AT45_NSS);
	return AT45_FAILURE;
}

/**
  * @brief
  * @param
  * @retval
  */
bool AT45_Found(void)
{
	return (PIN_MODE_GET(AT45_NSS) == PIN_MODE(PIN_OUT_PP_50, AT45_NSS));
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_WriteBytes(const uint8_t * src, uint16_t length)
{
	while (length != 0)
	{
		length--;
		AT45_WriteByte( *src++ );
	}
}

/**
  * @brief	Read data from specified page address
  *			This function is to read data from specified page address.
  *			The function use main memory page read command to read
  *			data directly form main memory
  * @param	*dst		destination buffer
  * @param	pageAddress
  * @retval	ERROR
  */
ERROR_t AT45_PageRead(uint8_t *dst, uint16_t page, uint16_t bytes)
{
	ERROR_t result = AT45_PARAM_ERROR;

	if (page < AT45_PAGES
	&&	bytes <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendPageAddress(AT45_CMD_MMPR, page, 0);
		AT45_WriteBytes(AT45_PAR_0000, 4);
		AT45_ReadBytesEnd(dst, bytes);
	}
	return result;
}

void AT45_SendPageAddressHigh(uint8_t cmd, uint16_t page)
{
	AT45_SendPageAddress(cmd, page, 0);
	AT45_CS_HIGH();
}

/**
  *
  *	@brief	Main Memory Page to Buffer Transfer
  * @param	bufferNum specify the buffer 1 or 2 as the destination.
  * @param	page specify the page address which you want to read.
  * @return	None
  *
  *	This function is to transfer data from specified page address of main memory
  *	to specified AT45DB161 internal buffer.
  */
ERROR_t AT45_Memory2Buffer(uint8_t bufferNum, uint16_t page)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (page < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendPageAddressHigh((bufferNum == AT45_BUF1) ? AT45_CMD_MTB1 : AT45_CMD_MTB2, page);
	}
	return result;
}

/**
  * @brief	Buffer to Main Memory Page Program with Built-in Erase
  * @param	bufferNum specify the buffer 1 or 2 as the source.
  * @param	pageAddr specify the page address which you want to write.
  * @return	ERROR

	This function is to write data from specified internal buffer to the
	specified main memory page address with built-in erase.
  */
ERROR_t AT45_Buffer2Memory(uint8_t bufferNum, uint16_t page)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (page < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendPageAddressHigh((bufferNum == AT45_BUF1) ? AT45_CMD_B1TMW : AT45_CMD_B2TMW, page);
	}
	return result;
}

/**
  * @brief	Read  and write a data element from and to the SPI interface.
  * @param	data is the data that was transmitted over the SPI interface.
  * @return	the data that was received over the SPI interface.

	This function send transmitted data to the SPI interface of the specified
	SPI module and gets received data from the SPI interface of the specified
	SPI module and return that data.

	Only the lower N bits of the value written to \e pulData contain
	valid data, where N is the data width as configured by
	SPIConfig().  For example, if the interface is configured for
	8-bit data width, only the lower 8 bits of the value written to \e pulData
	contain valid data.
  */
uint8_t AT45_SPI_Write(uint8_t data)
{
	// Wait until there is space.
	while ((AT45_SPI->SR & SPI_SR_TXE) == 0)
		;
	AT45_SPI->DR = data;

	// Wait until there is data to be read.
	while ((AT45_SPI->SR & SPI_SR_RXNE) == 0)
		;
	data = AT45_SPI->DR;
	return data;
}

uint8_t AT45_SPI_WriteLow(uint8_t data)
{
	(void)AT45_SPI->DR;
	AT45_CS_LOW();
	return AT45_SPI_Write(data);
}

void AT45_SendBufferAddress(uint8_t cmd, uint16_t readAddress)
{
	AT45_SPI_WriteLow(cmd);
	AT45_SPI_WriteLow(0);
	AT45_SPI_Write((readAddress & 0x300) >> 8);
	AT45_SPI_Write(readAddress & 0xFF);
}

/**
  * @brief	Write data to specified AT45DB161 internal buffer
  * @param	ucBufferNum specify the buffer 1 or 2 as the destination.
  * @param	pucBuffer to store the data need to be written.
  * @param	ulWriteAddr specify the start address in the buffer which you want to write.
  * @param	bytes specify how many bytes to write.
  * @return ERROR

	This function is to write data to specified internal buffer. If you have
	just a little data to be stored(less than AT45_PageSize), you can temporarily
	store them in the AT45DB161 internal buffer.It can be more fast than read write from
	main memory.This write function doesn't affect the content in main memory.
  */
ERROR_t AT45_WriteBuffer(uint8_t bufferNum, uint8_t *buffer, uint16_t writeAddress, uint16_t bytes)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if ((writeAddress + bytes) <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendBufferAddress((bufferNum == AT45_BUF1) ? AT45_CMD_BF1W : AT45_CMD_BF2W, writeAddress);
		while (bytes != 0)
		{
			AT45_SPI_Write(*buffer++);
			--bytes;
		}
		AT45_CS_HIGH();
	}
	return result;
}

/**
  * @brief
  * @param	none
  * @retval true if initialized 
  */
bool AT45_Initialized(void)
{
	return ((AT45_SPI->CR1 & SPI_CR1_SPE) != 0);
}

/**
  * @brief	Read  and write a data element from and to the SPI interface.
  * @param	data is the data that was transmitted over the SPI interface.
  * @return	the data that was received over the SPI interface.
  */
ERROR_t AT45Write(uint8_t *src, uint32_t address, uint16_t bytes)
{
	ERROR_t result = AT45_PARAM_ERROR;
    uint16_t usLeftForward, usLeftBehind, usPages;
    uint16_t ulBytesWritten = 0;

	if (!AT45_Initialized() || !AT45_Found())
		return AT45_FAILURE;

	if (address < (AT45_PAGE_SIZE * AT45_PAGES))
	{
		uint16_t usPageAddr = address / AT45_PAGE_SIZE;
		uint16_t usOffAddr  = address % AT45_PAGE_SIZE;

		if ((usOffAddr + bytes) > AT45_PAGE_SIZE)
		{
			usLeftForward = AT45_PAGE_SIZE - usOffAddr;
			usLeftBehind = (bytes - usLeftForward) % AT45_PAGE_SIZE;
			usPages = (bytes - usLeftForward) / AT45_PAGE_SIZE;
		}
		else
		{
			usLeftForward = bytes;
			usLeftBehind = 0;
			usPages = 0;
		}

		// Read the first page from usPageAddr to buffer
		if (NO_ERROR == (result = AT45_Memory2Buffer(AT45_BUF1, usPageAddr))
		&&	NO_ERROR == (result = AT45_WaitReady())
			)
		{
			if (usPages)

				result = AT45_Memory2Buffer(AT45_BUF2, usPageAddr + 1);

			if (NO_ERROR == result
			&&	NO_ERROR == (result = AT45_WriteBuffer(AT45_BUF1, src, usOffAddr, usLeftForward))
			// Write the data of the buffer back to the same page address in main memory
			&&	NO_ERROR == (result = AT45_Buffer2Memory(AT45_BUF1, usPageAddr))
			&&	NO_ERROR == (result = AT45_WaitReadyEx(4000))
				)
			{
				ulBytesWritten += usLeftForward;
				usPageAddr++;
				while(usPages)
				{
					if (NO_ERROR == (result = AT45_WriteBuffer(AT45_BUF1, &src[ulBytesWritten], 0, AT45_PAGE_SIZE))
					&&	NO_ERROR == (result = AT45_Buffer2Memory(AT45_BUF1, usPageAddr))
						)
					{
						ulBytesWritten += AT45_PAGE_SIZE;
						usPageAddr++;
						usPages--;
						result = AT45_WaitReadyEx(4000);
					}
					if (result != NO_ERROR)
						break;
				}
				if (NO_ERROR == result
				&&	usLeftBehind != 0
				&&	NO_ERROR == (result = AT45_Memory2Buffer(AT45_BUF2, usPageAddr))
				&&	NO_ERROR == (result = AT45_WaitReady())
				&&	NO_ERROR == (result = AT45_WriteBuffer(AT45_BUF2, &src[ulBytesWritten], 0, usLeftBehind))
				&&	NO_ERROR == (result = AT45_Buffer2Memory(AT45_BUF2, usPageAddr))
					)
					result = AT45_WaitReadyEx(4000);
			}
		}
	}
	return result;
}

/**
  * @brief	Read data from main memory
  * @param	pucBuffer specifies the destination buffer pointer to store data.
  * @param	address specifies the byte address which data will be read (0 to AT45_PAGE_SIZE*AT45_PAGES)
  * @param	bytes specifies the length of data will be read.
  * @return	ERROR

	This function is to read more than one byte from the main memory of AT45DB161.
	The appointed byte length data will be read to appointed address.
  */
ERROR_t AT45Read(uint8_t *dst, uint32_t address, uint16_t bytes)
{
	ERROR_t result = AT45_PARAM_ERROR;
    uint16_t usOffAddr, usPageAddr;
    uint16_t usLeftForward, usLeftBehind, usPages;
    uint16_t ulBytesRead = 0;

	if (!AT45_Initialized() || !AT45_Found())
		return AT45_FAILURE;

    if (address < (AT45_PAGE_SIZE * AT45_PAGES)
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		usPageAddr = address / AT45_PAGE_SIZE;
		usOffAddr = address % AT45_PAGE_SIZE;
		if ((usOffAddr + bytes) > AT45_PAGE_SIZE)
		{
			usLeftForward = AT45_PAGE_SIZE - usOffAddr;
			usLeftBehind = (bytes - usLeftForward) % AT45_PAGE_SIZE;
			usPages = (bytes - usLeftForward) / AT45_PAGE_SIZE;
		}
		else
		{
			usLeftForward = bytes;
			usLeftBehind = 0;
			usPages = 0;
		}
		AT45_SendPageAddress(AT45_CMD_MMPR, usPageAddr, usOffAddr);
		AT45_WriteBytes(AT45_PAR_0000, 4);
		AT45_ReadBytesEnd(dst, usLeftForward);

		usPageAddr++;
		ulBytesRead += usLeftForward;

		while (usPages)
		{
			if (NO_ERROR != (result = AT45_PageRead(&dst[ulBytesRead], usPageAddr, AT45_PAGE_SIZE)))
				break;
			usPageAddr++;
			ulBytesRead += AT45_PAGE_SIZE;
			usPages--;
		}
		if (NO_ERROR == result && usLeftBehind != 0)
		{
			result = AT45_PageRead(&dst[ulBytesRead], usPageAddr, usLeftBehind);
		}
	}
    return result;
}

/**
  * @brief	Read  and write a data element from and to the SPI interface.
  * @param	data is the data that was transmitted over the SPI interface.
  * @return	the data that was received over the SPI interface.
  */
void AT45_SPI_WritesLowHigh(const uint8_t *dst, uint16_t bytes)
{
	AT45_CS_LOW();
	while (bytes != 0)
	{
		bytes--;
		AT45_SPI_Write(*dst++);
	}
	AT45_CS_HIGH();
}

/**
  * @brief	Erase whole chip memory with build-in erase program
  * @param	None
  * @return	ERROR code
  *
  *
  *	This function is to Erase whole chip with build-in erase program. the value of
  *	the erased data will be 0xFF;
  */
ERROR_t AT45_EraseChip(void)
{
	register int retry = 10000;
	ERROR_t result = AT45_WaitReady();
	if (NO_ERROR == result)
	{
		do
		{
			AT45_SPI_WritesLowHigh(AT45_CMD_CPER, 4);
			result = AT45_WaitReady();
			retry--;
		} while (retry != 0 && result != NO_ERROR);
	}
	return result;
}


#if 0

/*	Local Function	*/

//*****************************************************************************
//
//! \brief  Erase specified page
//!
//! \param usPageAddr specifies the page address.(page address < 4096)
//!
//! This function is to Erase the specified page
//!
//! \return None
//!
//*****************************************************************************
ERROR_t AT45_PageErase(uint16_t page)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (page < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendPageAddressHigh(AT45_CMD_ERPG, page, 0);
	}
	return result;
}

//*****************************************************************************
//
//! \brief Read data in specified AT45DB161 internal buffer
//!
//! \param ucBufferNum specify the buffer 1 or 2 as the source.
//! \param pucBuffer to store the data read out from internal buffer
//! \param address specify the start address in the buffer which you want to read.
//! \param bytes specify how many bytes to read.
//!
//! This function is to read data from specified internal buffer. If you have
//! just a little data to be stored(less than AT45_PageSize), you can temporarily
//! store them in the AT45DB161 internal buffer.It can be more fast than read write from
//! main memory
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45_ReadBuffer(uint8_t bufferNum, uint8_t *buffer, uint16_t address, uint16_t byteToRead)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if ((address + byteToRead) <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendBufferAddress((bufferNum == AT45_BUF1) ? AT45_CMD_BF1R : AT45_CMD_BF2R, address);
		AT45_SPI_ReadHigh(buffer, byteToRead);
	}
	return result;
}

uint8_t AT45_SPI_WriteHigh(uint8_t data)
{
	uint8_t data_out = AT45_SPI_Write(data);
	AT45_CS_HIGH();
	return data_out;
}

uint8_t AT45SPIReadByteHigh()
{
	return AT45_SPI_WriteHigh(0xFF);
}

//*****************************************************************************
//
//! \brief Gets a data element from the SPI interface.
//!
//! \param dst is a pointer to a storage location for data that was received over the SSI interface.
//! \param length specifies the length of data will be read.
//!
//! This function gets received data from the interface of the specified
//! SPI module and places that data into the location specified by the
//! \e pulData parameter.
//!
//! \note Only the lower N bits of the value written to \e pulData contain
//! valid data, where N is the data width as configured by
//! SPIConfig().  For example, if the interface is configured for
//! 8-bit data width, only the lower 8 bits of the value written to \e pulData
//! contain valid data.
//!
//! \return None.
//
//*****************************************************************************
void AT45_SPI_ReadHigh(uint8_t *buffer, uint16_t length)
{
	AT45_SPI_Read(buffer, length);
	AT45_CS_HIGH();
}

void AT45_SPI_Read(uint8_t *buffer, uint16_t length)
{
	while (length != 0)
	{
		length--;
		*buffer++ = AT45_SPI_Write(0xFF);
	}
}

#endif

#ifdef BOOTLOADER
//	#include "AT45-bl.c"
#else
	#include "AT45-fw.c"
#endif
