#if 0

#include "common.h"
#include "board.h"
#include "AT45.h"

/**
  * @brief
  * @param
  * @retval
  */
void AT45_Transfer(void)
{
	AT45_CS_LOW();
	while (AT45_Context.AT45_TX_COUNT-- != 0)
	{
		AT45_Context.RxBuffer[AT45_Context.AT45_RX_INDEX++] =
			AT45_WriteByte(
				AT45_Context.TxBuffer[AT45_Context.AT45_TX_INDEX++]
			);
	}
	AT45_CS_HIGH();
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_InitInt(void)
{
}

//*****************************************************************************
//
//! \brief To check and wait device ready
//!
//! \param None
//!
//! This function is to check and wait device ready. If the device still don't get
//! ready after 300 cycles of retry, the function will return 0 for not ready,
//! otherwise return 1 for ready.
//!
//! \return 0: not ready  ;  1: ready
//
//*****************************************************************************
ERROR_t AT45_WaitReady(void)
{
    register int retry = 300;
    while (((AT45_GetState() & AT45_IDLE) == 0) && (retry != 0))
    	--retry;
   	return (retry == 0) ? AT45_NOT_READY : NO_ERROR;
}

void AT45_SendPageAddress(uint8_t cmd, uint16_t pageAddress)
{
	AT45_SPI_WriteLow(cmd);
	AT45_SPI_Write(pageAddress >> 6);
	AT45_SPI_Write((pageAddress & 0x3F) << 2);
	AT45_SPI_Write(0);
}

void AT45_SendPageAddressHigh(uint8_t cmd, uint16_t pageAddress)
{
	AT45_Send_Page_Address(cmd, pageAddress);
	AT45_CS_HIGH();
}

//*****************************************************************************
//
//! \brief Read data from specified page address
//!
//! \param pucBuffer to store the data read out from page.
//! \param ulPageAddr specify the page address in the main memory of AT45DB161.
//! \param ulNumByteToRead specify how many bytes to write.
//!
//! This function is to read data from specified page address.
//! The function use main memory page read command to read data directly form main memory
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45_PageRead(uint8_t *buffer, uint16_t pageAddress, uint16_t byteToRead)
{
	ERROR_t result = AT45_PARAM_ERROR;

    if (pageAddress < AT45_PAGES
	&&	byteToRead <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
    {
    	AT45SendPageAddress(
    		AT45_CMD_MMPR
    		, pageAddress
    		);
        AT45_SPI_Write(0);
        AT45_SPI_Write(0);
        AT45_SPI_Write(0);
        AT45_SPI_Write(0);
        AT45_SPI_ReadHigh(buffer, byteToRead);
    }
    return result;
}

//*****************************************************************************
//
//! \brief  Erase specified page
//!
//! \param usPageAddr specifies the page address.(page address < 4096)
//!
//! This function is to Erase the specified page
//!
//! \return None
//!
//*****************************************************************************
ERROR_t AT45PageErase(uint16_t pageAddress)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (pageAddress < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
    	)
    	AT45SendPageAddressHigh(AT45_CMD_ERPG, pageAddress);
	return result;
}

//*****************************************************************************
//
//! \brief Main Memory Page to Buffer Transfer
//!
//! \param bufferNum specify the buffer 1 or 2 as the destination.
//! \param pageAddr specify the page address which you want to read.
//!
//! This function is to transfer data from specified page address of main memory
//! to specified AT45DB161 internal buffer.
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45Memory2Buffer(uint8_t bufferNum, uint16_t pageAddress)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (pageAddress < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
    	)
    {
    	AT45SendPageAddressHigh(
    		(bufferNum == AT45_BUF1)
    			? AT45_CMD_MTB1
    			: AT45_CMD_MTB2
    		, pageAddress);
    }
    return result;
}

//*****************************************************************************
//
//! \brief Buffer to Main Memory Page Program with Built-in Erase
//!
//! \param bufferNum specify the buffer 1 or 2 as the source.
//! \param pageAddr specify the page address which you want to write.
//!
//! This function is to write data from specified internal buffer to the
//! specified main memory page address with built-in erase.
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45Buffer2Memory(uint8_t bufferNum, uint16_t pageAddress)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (pageAddress < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45SendPageAddressHigh(
    		(bufferNum == AT45_BUF1)
				? AT45_CMD_B1TMW
				: AT45_CMD_B2TMW
    		, pageAddress);
    }
    return result;
}

void AT45_SendBufferAddress(uint8_t cmd, uint16_t readAddress)
{
	AT45_SPI_WriteLow(cmd);
	AT45_SPI_WriteLow(0);
	AT45_SPI_Write((readAddress & 0x300) >> 8);
	AT45_SPI_Write(readAddress & 0xFF);
}

//*****************************************************************************
//
//! \brief Read data in specified AT45DB161 internal buffer
//!
//! \param ucBufferNum specify the buffer 1 or 2 as the source.
//! \param pucBuffer to store the data read out from internal buffer
//! \param ulReadAddr specify the start address in the buffer which you want to read.
//! \param ulNumByteToRead specify how many bytes to read.
//!
//! This function is to read data from specified internal buffer. If you have
//! just a little data to be stored(less than AT45_PageSize), you can temporarily
//! store them in the AT45DB161 internal buffer.It can be more fast than read write from
//! main memory
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45ReadBuffer(uint8_t bufferNum, uint8_t *buffer, uint16_t readAddress, uint16_t byteToRead)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if ((readAddress + byteToRead) <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendBufferAddress(
			(bufferNum == AT45_BUF1)
				? AT45_CMD_BF1R
				: AT45_CMD_BF2R
			, readAddress
			);
		AT45_SPI_ReadHigh(buffer, byteToRead);
	}
	return result;
}

//*****************************************************************************
//
//! \brief Write data to specified AT45DB161 internal buffer
//!
//! \param ucBufferNum specify the buffer 1 or 2 as the destination.
//! \param pucBuffer to store the data need to be written.
//! \param ulWriteAddr specify the start address in the buffer which you want to write.
//! \param ulNumByteToRead specify how many bytes to write.
//!
//! This function is to write data to specified internal buffer. If you have
//! just a little data to be stored(less than AT45_PageSize), you can temporarily
//! store them in the AT45DB161 internal buffer.It can be more fast than read write from
//! main memory.This write function doesn't affect the content in main memory.
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45_WriteBuffer(uint8_t bufferNum, uint8_t *buffer, uint16_t writeAddress, uint16_t byteToWrite)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if ((writeAddress + byteToWrite) <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendBufferAddress(
			(bufferNum == AT45_BUF1)
				? AT45_CMD_BF1W
				: AT45_CMD_BF2W
			, writeAddress
			);
		while (byteToWrite != 0)
		{
			AT45_SPI_Write(*buffer++);
			--byteToWrite;
		}
		AT45_CS_HIGH();
	}
	return result;
}

//*****************************************************************************
//
//! \brief Erase whole chip memory with build-in erase program
//!
//! \param None
//!
//! This function is to Erase whole chip with build-in erase program. the value of
//! the erased data will be 0xFF;
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45_EraseChip()
{
	ERROR_t result;
    if (NO_ERROR == (result = AT45_WaitReady()))
    {
    	AT45_SPI_WritesLowHigh(AT45_CMD_CPER, 4);
    }
    return result;
}

//*****************************************************************************
//
//! \brief Read  and write a data element from and to the SPI interface.
//!
//! \param data is the data that was transmitted over the SPI interface.
//!
//! This function send transmitted data to the SPI interface of the specified
//! SPI module and gets received data from the SPI interface of the specified
//! SPI module and return that data.
//!
//! \note Only the lower N bits of the value written to \e pulData contain
//! valid data, where N is the data width as configured by
//! SPIConfig().  For example, if the interface is configured for
//! 8-bit data width, only the lower 8 bits of the value written to \e pulData
//! contain valid data.
//!
//! \return the data that was received over the SPI interface.
//
//*****************************************************************************
uint8_t AT45_SPI_Write(uint8_t data)
{
#ifdef BOOTLOADER
    // Wait until there is space.
	while ((AT45_SPI->SR & SPI_SR_TXE) == 0)
		;
    AT45_SPI->DR = data;

    // Wait until there is data to be read.
	while ((AT45_SPI->SR & SPI_SR_RXNE) == 0)
		;
    data = AT45_SPI->DR;
#else
	// TODO: Implement interrupt service
#endif
    return data;
}

uint8_t AT45_SPI_WriteLow(uint8_t data)
{
	AT45_CS_LOW();
	return AT45_SPI_Write(data);
}

uint8_t AT45_SPI_WriteHigh(uint8_t data)
{
	uint8_t data_out = AT45_SPI_Write(data);
	AT45_CS_HIGH();
	return data_out;
}

uint8_t AT45_SPI_ReadByteHigh()
{
	return AT45_SPI_WriteHigh(0xFF);
}

void AT45_SPI_WritesHigh(const uint8_t *buffer, uint16_t length)
{
	while (length-- != 0)
		AT45_SPI_Write(*buffer++);
	AT45_CS_HIGH();
}

void AT45_SPI_WritesLowHigh(const uint8_t *bytes, uint16_t length)
{
	AT45_CS_LOW();
	AT45_SPI_WritesHigh(bytes, length);
}

//*****************************************************************************
//
//! \brief Gets a data element from the SPI interface.
//!
//! \param dst is a pointer to a storage location for data that was received over the SSI interface.
//! \param length specifies the length of data will be read.
//!
//! This function gets received data from the interface of the specified
//! SPI module and places that data into the location specified by the
//! \e pulData parameter.
//!
//! \note Only the lower N bits of the value written to \e pulData contain
//! valid data, where N is the data width as configured by
//! SPIConfig().  For example, if the interface is configured for
//! 8-bit data width, only the lower 8 bits of the value written to \e pulData
//! contain valid data.
//!
//! \return None.
//
//*****************************************************************************
void AT45_SPI_Read(uint8_t *buffer, uint16_t length)
{
	while (length-- != 0)
		*buffer++ = AT45_SPI_Write(0xFF);
}
void AT45_SPI_ReadHigh(uint8_t *buffer, uint16_t length)
{
	AT45_SPI_Read(buffer, length);
	AT45_CS_HIGH();
}
#endif
