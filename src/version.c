#include "stm32f10x.h"
#include "version.h"
#include "fm24.h"

/**
  * @brief	Calculate CRC32 for bytes
  * @param	src		pointer to data
  *			length	data size in bytes
  *			exclude	address to exclude (only for uint32_t align)
  * @retval	CRC32
  */
uint32_t CalculateCRC(void * src, uint32_t length, void *exclude)
{
	register uint32_t * pu32 = (uint32_t *)src;

	CRC_ResetDR();

	while (length >= sizeof(uint32_t))
	{
		CRC_CalcCRC((pu32 == exclude) ? 0 : *pu32);
		pu32++;
		length -= sizeof(uint32_t);
	}

	if (length != 0)
	{
		register uint32_t last = 0;
		register uint8_t * pu8 = (uint8_t *)pu32;
		while (length != 0)
		{
			last <<= 8;
			last |= *(pu8 + length - 1);
			length--;
		}
		CRC_CalcCRC(last);
	}
	return CRC_GetCRC();
}

/**
  * @brief	Fill version info
  *			data MUST BE started with Version_t
  * @param
  * @retval
  */
ERROR_t VersionFill(void * data, uint16_t size, uint32_t version, uint16_t address)
{
	register uint8_t *pu8 = (uint8_t *)data;
	Version_t * pv = (Version_t *)pu8;

	pv->SizeX = size;
	pv->Version = version;
	pv->AddressX = address;
	pv->CRC32 = CalculateCRC(data, size, data);
	return NO_ERROR;
}

/**
  * @brief	Check version info
  *			data MUST BE started with Version_t
  * @param
  * @retval
  */
ERROR_t VersionCheck(Version_t * src)
{
	if (src->CRC32 == CalculateCRC(src, src->SizeX, src))
		return NO_ERROR;
	return BAD_CRC32;
}

/**
  * @brief	Save versioning data to FM24
  *			data MUST BE started with Version_t
  * @param
  * @retval
  */
ERROR_t VersionSave(void *src, uint16_t address, uint16_t size)
{
	return FM24Write(address, src, size);
}

/**
  * @brief	Save versioning data to FM24 other version data
  *			data MUST BE started with Version_t
  * @param
  * @retval
  */
ERROR_t VersionSaveEx(void *src, uint16_t address, uint16_t size)
{
	if (((Version_t *)src)->AddressX == 0)
	{
		VersionFill(src, size, ((Version_t *)src)->Version, address);
		if (NO_ERROR == FM24Write(address, src, size))
		{
			VersionFill(src, size, ((Version_t *)src)->Version, address + size);
			return FM24Write(address, src, size);
		}
		return SYS_VERSION_FAIL;
		
	}
	else if (((Version_t *)src)->AddressX == address)
	{
		address += size;
	}
	VersionFill(src, size, ((Version_t *)src)->Version, address);
	return FM24Write(address, src, size);
}

/**
  * @brief	Load versioning data from FM24
  *			data MUST BE started with Version_t
  * @param
  * @retval
  */
ERROR_t VersionLoad(void *dst, uint16_t address, uint16_t size)
{
	register ERROR_t result = FM24Read(address, dst, size);
	if (NO_ERROR == result)
	{
		register Version_t *pv = (Version_t *)dst;
		if (pv->SizeX == size
		&&	pv->CRC32 == CalculateCRC(dst, size, dst)
			)
			return NO_ERROR;
		else
			result = BAD_CRC32;
	}
	return result;
}

/**
  * @brief	Load versioning data from FM24 with lastest version
  *			data MUST BE started with Version_t
  * @param
  * @retval
  */
ERROR_t VersionLoadEx(void *dst, uint16_t address, uint16_t size)
{
	register uint32_t version;
	if (NO_ERROR == VersionLoad(dst, address, size))
	{
		version = ((Version_t *)dst)->Version;
		if (NO_ERROR == VersionLoad(dst, address + size, size)
		&&	version <= ((Version_t *)dst)->Version
			)
		{
			((Version_t *)dst)->Version++;
			return NO_ERROR;
		}
		if (NO_ERROR == VersionLoad(dst, address, size))
		{
			((Version_t *)dst)->Version++;
			return NO_ERROR;
		}
		return SYS_CFG_CHANGE;
	}
	if (NO_ERROR == VersionLoad(dst, address + size, size))
	{
		((Version_t *)dst)->Version++;
		return NO_ERROR;
	}
	return SYS_CFG_CHANGE;
}
