#if defined( __GNUC__ )

#include <sys/stat.h>
#include <errno.h>
#include <stddef.h>

#include "debug.h"

#define __weak __attribute__((weak))

__weak void _exit(int exitcode)
{
    while (1)
        ;
}

__weak int _write(int fd, const char *buf, size_t cnt)
{
    int i;
    for (i = 0; i < cnt; i++)
        debug_putc(buf[i]);

    return cnt;
}

__weak int _read(int fd, char *buf, size_t cnt) {
    *buf = debug_getc();

    return 1;
}

__weak int _lseek(int fd, off_t pos, int whence) {
    return -1;
}

__weak int isatty(int fd) {
    return 1;
}

__weak int _isatty(int fd) {
    return 1;
}

__weak int _close(int fd) {
    return 0;
}

__weak int _fstat(int fd, struct stat *st) {
    st->st_mode = S_IFCHR;
    return 0;
}

__weak int _open(const char *path, int flags, ...) {
    return 1;
}

void *_sbrk(int incr) {
	errno = ENOMEM;
	return (void *)-1;
}

#endif
