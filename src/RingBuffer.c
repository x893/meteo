#include "RingBuffer.h"

bool RBPut(RingBuffer_t *RB, uint8_t c)
{
	uint8_t *next = RB->Head + 1;
	if (next == RB->End)
		next = RB->Start;
	if (next == RB->Tail)
		return false;
	*RB->Head = c;
	RB->Head = next;
	return true;
}

bool RBGet(RingBuffer_t *RB, uint8_t * c)
{
	uint8_t *next = RB->Tail;
	if (RB->Tail == RB->Head)	// No data in ring
	{
		*c = 0;
		return false;
	}
	*c = *next;				// Get data and move to next
	next++;
	if (next == RB->End)		// Check for ring
		next = RB->Start;
	RB->Tail = next;
	return true;
}

void RBInit(RingBuffer_t *RB, uint8_t * buffer, int size)
{
	RB->Tail = RB->Head = RB->Start = buffer;
	RB->End = buffer + size;
}
