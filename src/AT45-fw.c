#if 0

#include "common.h"
#include "board.h"
#include "AT45.h"

#define AT45_RX_TX			U32.U32
#define AT45_RX_INDEX		U32.U8[0]
#define AT45_RX_COUNT		U32.U8[1]
#define AT45_TX_INDEX		U32.U8[2]
#define AT45_TX_COUNT		U32.U8[3]

typedef struct AT45_Context_s
{
	uint8_t RxBuffer[16];
	uint8_t TxBuffer[16];

	UU32_t U32;

} AT45_Context_t;

extern AT45_Context_t AT45_Context;

/**
  * @brief
  * @param
  * @retval
  */
void AT45_InitInt(void)
{
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_ReadBytes(uint8_t * dst, uint16_t length)
{
	while (length != 0)
	{
		length--;
		*dst++ = AT45_ReadByte();
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_WriteBytes(const uint8_t * src, uint16_t length)
{
	while (length != 0)
	{
		length--;
		AT45_WriteByte( *src++ );
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void AT45_ReadBytesEnd(uint8_t * dst, uint16_t length)
{
	AT45_ReadBytes(dst, length);
	AT45_CS_HIGH();
}


#define AT45_CHIPID_mask	0x0000FFFF
#define AT45_CHIPID			0x0000261F	/*	Manufacturer ID:1F Atmel		*/
										/*	DevideID (1):	20 DataFlash	*/
										/*					06 16 M-Bit		*/
										/*					06 16 M-Bit		*/
										/*	DevideID (2):	ignored			*/
										/*	DevideID (3):	ignored			*/

#define AT45_CMD_SRRD		0xD7	/* Status Register Read				*/
#define AT45_CMD_MDID		0x9F	/* Manufacturer and Device ID Read	*/
#define AT45_CMD_DPD		0xB9	/* Deep Power-down					*/
#define AT45_CMD_RDPD		0xAB	/* Resume from Deep Power-down		*/

#define AT45_CMD_MTB1		0x53
#define AT45_CMD_MTB2		0x55

#define AT45_CMD_CARL		0xE8
#define AT45_CMD_CARH		0x0B
#define AT45_CMD_RDID		0x03
#define AT45_CMD_MMPR		0xD2
#define AT45_CMD_BF1R		0xD4
#define AT45_CMD_BF2R		0xD6
#define AT45_CMD_BF1W		0x84
#define AT45_CMD_BF2W		0x87
#define AT45_CMD_B1TMW		0x83
#define AT45_CMD_B2TMW		0x86
#define AT45_CMD_B1TMO		0x88
#define AT45_CMD_B2TMO		0x89
#define AT45_CMD_ERPG		0x81
#define AT45_CMD_ERBL		0x50
#define AT45_CMD_ERSC		0x7C
#define AT45_CMD_APR1		0x58
#define AT45_CMD_APR2		0x59

const uint8_t AT45_CMD_CPER[4] = { 0xC7, 0x94, 0x80, 0x9A };
const uint8_t AT45_CMD_ESSP[4] = { 0x3D, 0x2A, 0x7F, 0xA9 };
const uint8_t AT45_CMD_DSSP[4] = { 0x3D, 0x2A, 0x7F, 0x9A };
const uint8_t AT45_CMD_ESPR[4] = { 0x3D, 0x2A, 0x7F, 0xCF };
const uint8_t AT45_CMD_PSPR[4] = { 0x3D, 0x2A, 0x7F, 0xFC };
const uint8_t AT45_CMD_RSPR[4] = { 0x32, 0xFF, 0xFF, 0xFF };
const uint8_t AT45_CMD_SCLD[4] = { 0x3D, 0x2A, 0x7F, 0x30 };
const uint8_t AT45_CMD_PGCR[4] = { 0x3D, 0x2A, 0x80, 0xA6 };

//
//! Status Register Bits
//
#define AT45_IDLE			0x80
#define AT45_COMP			0x40
#define AT45_DENSITY_mask	0x3F
#define AT45_DENSITY		0x2C
#define AT45_PROTECT		0x02
#define AT45_PGSZ			0x01

#define AT45_PAGE_SIZE_DEFAULT	528
#define AT45_PAGE_SIZE_BINARY	512
#define AT45_PAGES				4096UL
#define AT45_BLOCKS				512
#define AT45_SECTORS			16
#define AT45_BUF1				1
#define AT45_BUF2				2

uint8_t	AT45_SPI_Write (uint8_t data);
uint8_t	AT45_SPI_WriteLow (uint8_t data);
uint8_t	AT45_SPI_WriteHigh (uint8_t data);
uint8_t	AT45_SPI_ReadByteHigh (void);

void	AT45_SPI_Read (uint8_t *dst, uint16_t length);
void	AT45_SPI_ReadHigh (uint8_t *dst, uint16_t length);
void	AT45_SPI_WritesLowHigh (const uint8_t *bytes, uint16_t length);

const PinInit16_t AT45PinInit[] =
{
	AT45_INIT
	{ NULL }
};
//*****************************************************************************
//
//! \brief Initialize AT45DB161 and SPI
//!
//! \param ulSpiClock specifies the SPI Clock Rate
//!
//! This function initialize the mcu SPI as master and specified SPI port.
//! After SPI and port was configured, the mcu send a AT45_CMD_SRRD command
//! to get the page size of AT45DB161 to get prepareed for the followed read and
//! write operations.
//!
//! \return ErrorCode.
//
//*****************************************************************************
ERROR_t AT45Init(void)
{
	SPI_InitTypeDef SPI_InitStructure;

	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(AT45_SPI, &SPI_InitStructure);
	SPI_Cmd(AT45_SPI, ENABLE);

	AT45_CS_HIGH();
	AT45_SPI_ReadByteHigh();
	DelayMicro(10);

	AT45_ResumeDeepPowerDown();

    // Check AT45DB161 state register to get density
    // Check AT45DB161 chip id
	if (NO_ERROR == AT45_WaitReady())
	{
		if (AT45_DENSITY == (AT45_GetState()  & AT45_DENSITY_mask)
		&&	AT45_CHIPID == (AT45_GetChipID() & AT45_CHIPID_mask)
			)
		{
			return NO_ERROR;
		}
		return AT45_BAD_CHIP;
	}
	return AT45_NOT_FOUND;
}

//*****************************************************************************
//
//! \brief Read AT45DB161 state register
//! \param None
//!
//! This function is to Read AT45DB161 state register
//!
//! \return the value of state register
//
//*****************************************************************************
uint8_t AT45_GetState(void)
{
	AT45_SPI_WriteLow(AT45_CMD_SRRD);
	return AT45_SPI_ReadByteHigh();
}

//*****************************************************************************
//
//! \brief To check and wait device ready
//!
//! \param None
//!
//! This function is to check and wait device ready. If the device still don't get
//! ready after 300 cycles of retry, the function will return 0 for not ready,
//! otherwise return 1 for ready.
//!
//! \return 0: not ready  ;  1: ready
//
//*****************************************************************************
ERROR_t AT45WaitReady(void)
{
    register int retry = 300;
    while (((AT45_GetState() & AT45_IDLE) == 0) && (retry != 0))
    	--retry;
   	return (retry == 0) ? AT45_NOT_READY : NO_ERROR;
}

//*****************************************************************************
//
//! \brief Read AT45DB161 chip ID
//!
//! \param pucBuf contain the ID information
//!
//! This function is to read AT45DB161 chip ID, the Manufacturer and Device ID
//! will be put into pucBuf.
//!
//! \return None
//
//*****************************************************************************
uint32_t AT45GetChipID(void)
{
	uint32_t chipId;
    AT45_SPI_WriteLow(AT45_CMD_MDID);
    AT45_SPI_ReadHigh((uint8_t *)&chipId, 4);
    return chipId;
}

//*****************************************************************************
//
//! \brief	Set Deep Power-down
//! \param
//!
//! The Deep Power-down command allows the device to enter
//!	into the lowest power consumption mode
//!
//! \return None
//
//*****************************************************************************
void AT45_DeepPowerDown(void)
{
    AT45_SPI_WriteLow(AT45_CMD_DPD);
    AT45_CS_HIGH();
}

//*****************************************************************************
//
//! \brief	Resume Deep Power-down
//!
//! \param
//!
//!	The Resume from Deep Power-down command takes the device
//!	out of the Deep Power-down mode and returns it to the normal
//!	standby mode
//!
//! \return None
//
//*****************************************************************************
void AT45_ResumeDeepPowerDown(void)
{
    AT45_SPI_WriteLow(AT45_CMD_RDPD);
    AT45_CS_HIGH();
}

void AT45_SendPageAddress(uint8_t cmd, uint16_t pageAddress)
{
	AT45_SPI_WriteLow(cmd);
	AT45_SPI_Write(pageAddress >> 6);
	AT45_SPI_Write((pageAddress & 0x3F) << 2);
	AT45_SPI_Write(0);
}

void AT45_SendPageAddressHigh(uint8_t cmd, uint16_t pageAddress)
{
	AT45_SendPageAddress(cmd, pageAddress);
	AT45_CS_HIGH();
}

//*****************************************************************************
//
//! \brief Read data from specified page address
//!
//! \param pucBuffer to store the data read out from page.
//! \param ulPageAddr specify the page address in the main memory of AT45DB161.
//! \param ulNumByteToRead specify how many bytes to write.
//!
//! This function is to read data from specified page address.
//! The function use main memory page read command to read data directly form main memory
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45PageRead(uint8_t *buffer, uint16_t pageAddress, uint16_t byteToRead)
{
	ERROR_t result = AT45_PARAM_ERROR;

    if (pageAddress < AT45_PAGES
	&&	byteToRead <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45WaitReady())
		)
    {
    	AT45_SendPageAddress(
    		AT45_CMD_MMPR
    		, pageAddress
    		);
        AT45_SPI_Write(0);
        AT45_SPI_Write(0);
        AT45_SPI_Write(0);
        AT45_SPI_Write(0);
        AT45_SPI_ReadHigh(buffer, byteToRead);
    }
    return result;
}

//*****************************************************************************
//
//! \brief  Erase specified page
//!
//! \param usPageAddr specifies the page address.(page address < 4096)
//!
//! This function is to Erase the specified page
//!
//! \return None
//!
//*****************************************************************************
ERROR_t AT45_PageErase(uint16_t pageAddress)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (pageAddress < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
    	)
    {
    	AT45_SendPageAddressHigh(
    		AT45_CMD_ERPG
    		, pageAddress
    		);
    }
	return result;
}

//*****************************************************************************
//
//! \brief Main Memory Page to Buffer Transfer
//!
//! \param bufferNum specify the buffer 1 or 2 as the destination.
//! \param pageAddr specify the page address which you want to read.
//!
//! This function is to transfer data from specified page address of main memory
//! to specified AT45DB161 internal buffer.
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45_Memory2Buffer(uint8_t bufferNum, uint16_t pageAddress)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (pageAddress < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
    	)
    {
    	AT45_SendPageAddressHigh(
    		(bufferNum == AT45_BUF1)
    			? AT45_CMD_MTB1
    			: AT45_CMD_MTB2
    		, pageAddress);
    }
    return result;
}

//*****************************************************************************
//
//! \brief Buffer to Main Memory Page Program with Built-in Erase
//!
//! \param bufferNum specify the buffer 1 or 2 as the source.
//! \param pageAddr specify the page address which you want to write.
//!
//! This function is to write data from specified internal buffer to the
//! specified main memory page address with built-in erase.
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45_Buffer2Memory(uint8_t bufferNum, uint16_t pageAddress)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if (pageAddress < AT45_PAGES
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendPageAddressHigh(
    		(bufferNum == AT45_BUF1)
				? AT45_CMD_B1TMW
				: AT45_CMD_B2TMW
    		, pageAddress);
    }
    return result;
}

void AT45_SendBufferAddress(uint8_t cmd, uint16_t readAddress)
{
	AT45_SPI_WriteLow(cmd);
	AT45_SPI_Write(0);
	AT45_SPI_Write((readAddress & 0x300) >> 8);
	AT45_SPI_Write(readAddress & 0xFF);
}

//*****************************************************************************
//
//! \brief Read data in specified AT45DB161 internal buffer
//!
//! \param ucBufferNum specify the buffer 1 or 2 as the source.
//! \param pucBuffer to store the data read out from internal buffer
//! \param ulReadAddr specify the start address in the buffer which you want to read.
//! \param ulNumByteToRead specify how many bytes to read.
//!
//! This function is to read data from specified internal buffer. If you have
//! just a little data to be stored(less than AT45_PageSize), you can temporarily
//! store them in the AT45DB161 internal buffer.It can be more fast than read write from
//! main memory
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45ReadBuffer(uint8_t bufferNum, uint8_t *buffer, uint16_t readAddress, uint16_t byteToRead)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if ((readAddress + byteToRead) <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendBufferAddress(
			(bufferNum == AT45_BUF1)
				? AT45_CMD_BF1R
				: AT45_CMD_BF2R
			, readAddress
			);
		AT45_SPI_ReadHigh(buffer, byteToRead);
	}
	return result;
}

//*****************************************************************************
//
//! \brief Write data to specified AT45DB161 internal buffer
//!
//! \param ucBufferNum specify the buffer 1 or 2 as the destination.
//! \param pucBuffer to store the data need to be written.
//! \param ulWriteAddr specify the start address in the buffer which you want to write.
//! \param ulNumByteToRead specify how many bytes to write.
//!
//! This function is to write data to specified internal buffer. If you have
//! just a little data to be stored(less than AT45_PageSize), you can temporarily
//! store them in the AT45DB161 internal buffer.It can be more fast than read write from
//! main memory.This write function doesn't affect the content in main memory.
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45WriteBuffer(uint8_t bufferNum, uint8_t *buffer, uint16_t writeAddress, uint16_t byteToWrite)
{
	ERROR_t result = AT45_PARAM_ERROR;
	if ((writeAddress + byteToWrite) <= AT45_PAGE_SIZE
	&&	NO_ERROR == (result = AT45_WaitReady())
		)
	{
		AT45_SendBufferAddress(
			(bufferNum == AT45_BUF1)
				? AT45_CMD_BF1W
				: AT45_CMD_BF2W
			, writeAddress
			);
		while (byteToWrite != 0)
		{
			AT45_SPI_Write(*buffer++);
			--byteToWrite;
		}
		AT45_CS_HIGH();
	}
	return result;
}

//*****************************************************************************
//
//! \brief Erase whole chip memory with build-in erase program
//!
//! \param None
//!
//! This function is to Erase whole chip with build-in erase program. the value of
//! the erased data will be 0xFF;
//!
//! \return None
//
//*****************************************************************************
ERROR_t AT45EraseChip()
{
	ERROR_t result;
    if (NO_ERROR == (result = AT45_WaitReady()))
    {
    	AT45_SPI_WritesLowHigh(AT45_CMD_CPER, 4);
    }
    return result;
}

//*****************************************************************************
//
//! \brief Read  and write a data element from and to the SPI interface.
//!
//! \param data is the data that was transmitted over the SPI interface.
//!
//! This function send transmitted data to the SPI interface of the specified
//! SPI module and gets received data from the SPI interface of the specified
//! SPI module and return that data.
//!
//! \note Only the lower N bits of the value written to \e pulData contain
//! valid data, where N is the data width as configured by
//! SPIConfig().  For example, if the interface is configured for
//! 8-bit data width, only the lower 8 bits of the value written to \e pulData
//! contain valid data.
//!
//! \return the data that was received over the SPI interface.
//
//*****************************************************************************
uint8_t AT45_SPI_Write(uint8_t data)
{
#ifdef BOOTLOADER
    // Wait until there is space.
	while ((AT45_SPI->SR & SPI_SR_TXE) == 0)
		;
    AT45_SPI->DR = data;

    // Wait until there is data to be read.
	while ((AT45_SPI->SR & SPI_SR_RXNE) == 0)
		;
    data = AT45_SPI->DR;
#else
	// TODO: Implement interrupt service
#endif
    return data;
}

uint8_t AT45_SPI_WriteLow(uint8_t data)
{
	AT45_CS_LOW();
	return AT45_SPI_Write(data);
}

uint8_t AT45_SPI_WriteHigh(uint8_t data)
{
	uint8_t data_out = AT45_SPI_Write(data);
	AT45_CS_HIGH();
	return data_out;
}

uint8_t AT45_SPI_ReadByteHigh()
{
	return AT45_SPI_WriteHigh(0xFF);
}

void AT45_SPI_WritesHigh(const uint8_t *buffer, uint16_t length)
{
	while (length != 0)
	{
		length--;
		AT45_SPI_Write(*buffer++);
	}
	AT45_CS_HIGH();
}

void AT45_SPI_WritesLowHigh(const uint8_t *bytes, uint16_t length)
{
	AT45_CS_LOW();
	AT45_SPI_WritesHigh(bytes, length);
}

//*****************************************************************************
//
//! \brief Gets a data element from the SPI interface.
//!
//! \param dst is a pointer to a storage location for data that was received over the SSI interface.
//! \param length specifies the length of data will be read.
//!
//! This function gets received data from the interface of the specified
//! SPI module and places that data into the location specified by the
//! \e pulData parameter.
//!
//! \note Only the lower N bits of the value written to \e pulData contain
//! valid data, where N is the data width as configured by
//! SPIConfig().  For example, if the interface is configured for
//! 8-bit data width, only the lower 8 bits of the value written to \e pulData
//! contain valid data.
//!
//! \return None.
//
//*****************************************************************************
void AT45_SPI_ReadHigh(uint8_t *buffer, uint16_t length)
{
	AT45_SPI_Read(buffer, length);
	AT45_CS_HIGH();
}

void AT45_SPI_Read(uint8_t *buffer, uint16_t length)
{
	while (length != 0)
	{
		length--;
		*buffer++ = AT45_SPI_Write(0xFF);
	}
}
#endif
