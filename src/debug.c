#include "common.h"
#include "board.h"
#include "RingBuffer.h"

const PinInit32_t DebugInit32[] = {	DEBUG_INIT		{ NULL }};
const PinInit16_t DebugInit16[] = {	DEBUG_UART_INIT	{ NULL }};

#ifdef BOOTLOADER
#else
	#include "FreeRTOS.h"
	#include "task.h"
	uint8_t TxBuffer[256];
	uint8_t RxBuffer[16];
	RingBuffer_t TxRingBuffer;
	RingBuffer_t RxRingBuffer;
#endif

void DebugInit(void)
{
#ifndef BOOTLOADER
	RBInit(&TxRingBuffer, TxBuffer, sizeof(TxBuffer));
	RBInit(&RxRingBuffer, RxBuffer, sizeof(TxBuffer));
#endif
	PinInit32(DebugInit32);
	PinInit16(DebugInit16);
}

void debug_putc(uint8_t ch)
{
	if (ch == '\n')
		debug_putc('\r');
#ifdef BOOTLOADER
	USART_SendData(DEBUG_UART, ch);
	while (USART_GetFlagStatus(DEBUG_UART, USART_FLAG_TC) == RESET)
		;
#else
	while (RBPut(&TxRingBuffer, ch) == false)
	{
		DEBUG_TX_INT_ENABLE();
		Delay(5);
	}
#endif
}

/**
  * @brief  Put string to DEBUG post
  * @param  String pointer
  * @retval None
  */
void debug(const char *msg)
{
	char ch;
	while ((ch = *msg++) != '\0')
		debug_putc(ch);
	DEBUG_TX_INT_ENABLE();
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	debug_putc(ch);
	DEBUG_TX_INT_ENABLE();
	return ch;
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
char debug_getc_nowait(void)
{
#ifdef BOOTLOADER
	if (USART_GetFlagStatus(DEBUG_UART, USART_FLAG_RXNE) != RESET)
		return (uint8_t)USART_ReceiveData(DEBUG_UART);
	return 0;
#else
	uint8_t ch;
	RBGet(&RxRingBuffer, &ch);
	return ch;
#endif
}

char debug_getc(void)
{
#ifdef BOOTLOADER
	while (USART_GetFlagStatus(DEBUG_UART, USART_FLAG_RXNE) == RESET)
		;
	return (uint8_t)USART_ReceiveData(DEBUG_UART);
#else
	uint8_t ch;
	while (!RBGet(&RxRingBuffer, &ch))
		;
	return ch;
#endif
}

#ifdef BOOTLOADER

bool Debug_IsOutput(void)
{
	return false;
}

#else
bool Debug_IsOutput(void)
{
	return (TxRingBuffer.Head == TxRingBuffer.Tail && (DEBUG_UART->SR & USART_SR_TC) != 0);
}

void DEBUG_IRQHandler(void)
{
	uint8_t ch;
	register USART_TypeDef *usart = DEBUG_UART;
	
	if (usart->SR & USART_SR_RXNE)
	{
		ch = usart->DR;
		if (usart->SR & (USART_SR_PE | USART_SR_FE | USART_SR_NE | USART_SR_ORE))
			ch = usart->DR;
		else
			RBPut(&RxRingBuffer, ch);
	}

	if (usart->SR & USART_SR_TXE)
	{
		if (RBGet(&TxRingBuffer, &ch))
			usart->DR = ch;
		else
			DEBUG_TX_INT_DISABLE();
	}
}
#endif

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif
