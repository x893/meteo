#include "common.h"
#include "board.h"
#include "FM24.h"

const PinInit32_t FM24PinI2C[]		= {	FM24_PIN_I2C_INIT	{ NULL } };
const PinInit32_t FM24PinOut[]		= {	FM24_PIN_OUT_INIT	{ NULL } };

const I2C_InitTypeDef FM24_I2C_InitData =
{
	400000,				// I2C_ClockSpeed
	I2C_Mode_I2C,		// I2C_Mode
	I2C_DutyCycle_2,	// I2C_DutyCycle
	0,					// I2C_OwnAddress1
	I2C_Ack_Enable,		// I2C_Ack
	I2C_AcknowledgedAddress_7bit
};

/**
  * @brief	Check I2C Busy
  * @param
  * @retval NO_ERROR or FM24_FAILURE
  */
ERROR_t FM24_CheckBusy(void)
{
	register int timeout = FM24_I2C_TIMEOUT_BUSY;
	while (I2C_GetFlagStatus(FM24_I2C, I2C_FLAG_BUSY) == SET)
		if (timeout-- == 0)
			return FM24_FAILURE;
	return NO_ERROR;
}

/**
  * @brief	Initiate START
  * @param
  * @retval NO_ERROR or FM24_TIMEOUT
  */
ERROR_t FM24_Start(void)
{
    register int timeout = FM24_I2C_TIMEOUT_SB;

    I2C_ClearFlag(FM24_I2C, I2C_FLAG_AF);
    I2C_GenerateSTART(FM24_I2C, ENABLE);
	while (I2C_GetFlagStatus(FM24_I2C, I2C_FLAG_SB) == RESET)
		if (timeout-- == 0)
			return FM24_TIMEOUT;
    return NO_ERROR;
}

/**
  * @brief	Write data
  * @param
  * @retval NO_ERROR or error
  */
ERROR_t FM24_WriteByte(uint8_t data)
{
    register int timeout = FM24_I2C_TIMEOUT_TXE;

	I2C_SendData(FM24_I2C, data);

	// Wait until the byte is transmitted
	while (	(I2C_GetFlagStatus(FM24_I2C, I2C_FLAG_TXE) == RESET)
		&&	(I2C_GetFlagStatus(FM24_I2C, I2C_FLAG_BTF) == RESET)
			)
		if (timeout-- == 0)
			return FM24_TIMEOUT;
    return NO_ERROR;
}

/**
  * @brief	Write Slave address
  * @param
  * @retval NO_ERROR or error
  */
ERROR_t FM24_WriteSlave(uint8_t slave)
{
	if (NO_ERROR == FM24_Start())
	{
		register int timeout = FM24_I2C_TIMEOUT_ADDR;
		I2C_SendData(FM24_I2C, slave);
		while (timeout != 0)
		{
			timeout--;
			if (slave & 0x01)
			{
				if (I2C_CheckEvent(FM24_I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) == ERROR)
					continue;
			}
			else if (I2C_CheckEvent(FM24_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) == ERROR)
				continue;

			return NO_ERROR;
		}
		I2C_GenerateSTOP(FM24_I2C, ENABLE);
		return FM24_TIMEOUT;
	}
	return FM24_FAILURE;
}

/**
  * @brief	Write memory address
  * @param
  * @retval NO_ERROR or error
  */
ERROR_t FM24_WriteAddress(uint16_t address)
{
	if (NO_ERROR == FM24_WriteSlave(FM24_ADDRESS_WRITE))
	{
		if (NO_ERROR == FM24_WriteByte(address >> 8)
		&&	NO_ERROR == FM24_WriteByte(address)
			)
			return NO_ERROR;
		I2C_GenerateSTOP(FM24_I2C, ENABLE);
	}
	return FM24_FAILURE;
}

/**
  * @brief
  * @param	none
  * @retval NO_ERROR or FM24_FAIL 
  */
bool FM24_Found(void)
{
	return ((FM24_I2C->CR1 & I2C_CR1_PE) != 0);
}

/**
  * @brief	Read data from memory
  * @param
  * @retval NO_ERROR or error
  */
ERROR_t FM24Read(uint16_t address, void * dst, uint16_t bytes)
{
	if (FM24_Found()
	&&	NO_ERROR == FM24_CheckBusy()
	&&	NO_ERROR == FM24_WriteAddress(address)
	&&	NO_ERROR == FM24_WriteSlave(FM24_ADDRESS_READ)
		)
	{
		register uint8_t *pu8 = (uint8_t *)dst;
		register int timeout;
        I2C_AcknowledgeConfig(FM24_I2C, ENABLE);

		while (bytes != 0)
		{
			bytes--;
			if (bytes == 0)		// Last byte, disable ACK
				I2C_AcknowledgeConfig(FM24_I2C, DISABLE);

			timeout = FM24_I2C_TIMEOUT_RXNE;
			while (I2C_GetFlagStatus(FM24_I2C, I2C_FLAG_RXNE) == RESET)
			{
				if (timeout-- == 0)
				{
					I2C_GenerateSTOP(FM24_I2C, ENABLE);
					return FM24_FAILURE;
				}
			}
			*pu8++ = I2C_ReceiveData(FM24_I2C);
		}
		I2C_GenerateSTOP(FM24_I2C, ENABLE);
		return NO_ERROR;
	}
	return FM24_FAILURE;
}

/**
  * @brief	Write data to memory
  * @param
  * @retval NO_ERROR or error
  */
ERROR_t FM24Write(uint16_t address, void *src, uint16_t bytes)
{
	register uint8_t * pu8 = (uint8_t *)src;

	if (FM24_Found()
	&&	NO_ERROR == FM24_CheckBusy()
	&&	NO_ERROR == FM24_WriteAddress(address)
		)
	{
		while (bytes != 0)
		{
			bytes--;
			if (NO_ERROR != FM24_WriteByte(*pu8++))
			{
				I2C_GenerateSTOP(FM24_I2C, ENABLE);
				return FM24_FAILURE;
			}
		}
		I2C_GenerateSTOP(FM24_I2C, ENABLE);
		return NO_ERROR;
	}
	return FM24_FAILURE;
}

/**
  * @brief	Wait until target device is ready for communication
  * @param
  * @retval NO_ERROR or FM24_NOT_FOUND
  */
ERROR_t FM24_IsPrecent(void)
{
	if (NO_ERROR == FM24_CheckBusy()
	&&	NO_ERROR == FM24_WriteAddress(0)
		)
	{
		I2C_GenerateSTOP(FM24_I2C, ENABLE);
		return NO_ERROR;
	}
	return FM24_FAILURE;
}

/**
  * @brief	Initialize the peripheral and all related clocks, GPIOs, DMA and 
  *			Interrupts according to the specified parameters in the 
  *			FM24_Context_t structure.
  * @param
  * @retval NO_ERROR or FM24_FAIL 
  */
ERROR_t FM24Init(void)
{
	register int retry_count_clk;
	register int retry_count;

	// Set output state for SDA/SCL and reset I2C
	PinInit32(FM24PinOut);

	for (retry_count = 80; retry_count != 0; retry_count--)
	{
		PIN_RESET(FM24_SCL);
		DelayMicro(2);
		PIN_SET(FM24_SCL);
		DelayMicro(2);
	}

	retry_count_clk = 10;
	while (PIN_READ(FM24_SDA) == 0 && retry_count_clk != 0)
	{
		retry_count_clk--;
		DelayMicro(2);
		// Set clock high and wait for any clock stretching to finish.
		PIN_SET(FM24_SCL);

		retry_count = 10;
		while (PIN_READ(FM24_SCL) == 0 && retry_count != 0)
		{
			retry_count--;
			DelayMicro(1);
		}

		DelayMicro(2);
		PIN_RESET(FM24_SCL);
		DelayMicro(2);
		PIN_SET(FM24_SCL);
	}

	// Generate a start then stop condition
	DelayMicro(2);
	PIN_RESET(FM24_SDA);
	DelayMicro(2);
	PIN_SET(FM24_SDA);
	DelayMicro(2);

	retry_count = 10;
	while (PIN_READ(FM24_SCL) == 0 && retry_count != 0)
	{
		retry_count--;
		DelayMicro(1);
	}
	if (retry_count != 0)
	{
		retry_count = 10;
		while (PIN_READ(FM24_SDA) == 0 && retry_count != 0)
		{
			retry_count--;
			DelayMicro(1);
		}
	}
	if (retry_count != 0)
	{
		I2C_Init(FM24_I2C, (I2C_InitTypeDef *)&FM24_I2C_InitData);
		I2C_Cmd(FM24_I2C, ENABLE);

		PinInit32(FM24PinI2C);
		if (NO_ERROR == FM24_IsPrecent())
			return NO_ERROR;

		PinInit32(FM24PinOut);
		I2C_DeInit(FM24_I2C);
	}
	return FM24_I2C_ERROR;
}

#ifndef BOOTLOADER
	#include "FM24-fw.c"
#endif

#if 0
	
	FM24_HAL_DISABLE_DEV();
	FM24_I2C_HAL_GPIODeInit();
	FM24_I2C_HAL_DMADeInit();
	I2C_RCC_RESET(FM24_I2C_CLK);		/* Reset I2Cx device clock in order to avoid non-cleared error flags */

	FM24_I2C_HAL_GPIOInit();
	I2C_Init(FM24_I2C, (I2C_InitTypeDef *)&FM24_I2C_InitData);
	FM24_HAL_ENABLE_DEV();

	FM24_InitInt();

	ds->State = FM24_STATE_READY;


#define FM24_HAL_ENABLE_DEV()			i2c->CR1 |=  I2C_CR1_PE
#define FM24_HAL_DISABLE_DEV()			i2c->CR1 &= ~I2C_CR1_PE
#define I2C_RCC_RESET(clk)				RCC_APB1PeriphResetCmd((clk),ENABLE);	\
										RCC_APB1PeriphResetCmd((clk),DISABLE)

// #define FM24_HAL_ENABLE_ACK()			i2c->CR1 |= I2C_CR1_ACK
// #define FM24_HAL_DISABLE_ACK()			i2c->CR1 &= ~I2C_CR1_ACK

#define FM24_HAL_ENABLE_ERRIT()			i2c->CR2 |=  I2C_CR2_ITERREN
#define FM24_HAL_ENABLE_EVTIT()			i2c->CR2 |=  I2C_CR2_ITEVTEN
#define FM24_HAL_DISABLE_ERRIT()		i2c->CR2 &= ~I2C_CR2_ITERREN
#define FM24_HAL_DISABLE_EVTIT()		i2c->CR2 &= ~I2C_CR2_ITEVTEN

#define FM24_HAL_START()				i2c->CR1 |= I2C_CR1_START
#define FM24_HAL_CLEAR_AF()				i2c->SR1 = ~I2C_SR1_AF
#define FM24_HAL_SEND(value)			i2c->DR = (uint8_t)((value))
#define FM24_HAL_RECEIVE()				(uint8_t)(i2c->DR)
#define FM24_HAL_STOP()					i2c->CR1 |= I2C_CR1_STOP 

#define FM24_HAL_GET_EVENT()			(uint16_t)(i2c->SR1 & FM24_I2C_STATUS1_EVT_MASK)
#define FM24_HAL_GET_ERROR()			(uint16_t)(i2c->SR1 & FM24_I2C_STATUS_ERR_MASK)
#define FM24_HAL_GET_SB()				(uint16_t)(i2c->SR1 & I2C_SR1_SB)
#define FM24_HAL_GET_ADDR()				(uint16_t)(i2c->SR1 & I2C_SR1_ADDR)
#define FM24_HAL_GET_ADD10()			(uint16_t)(i2c->SR1 & I2C_SR1_ADD10)
#define FM24_HAL_GET_STOPF()			(uint16_t)(i2c->SR1 & I2C_SR1_STOPF)
#define FM24_HAL_GET_BTF()				(uint16_t)(i2c->SR1 & I2C_SR1_BTF)
#define FM24_HAL_GET_TXE()				(uint16_t)(i2c->SR1 & I2C_SR1_TXE)
#define FM24_HAL_GET_RXNE()				(uint16_t)(i2c->SR1 & I2C_SR1_RXNE)
#define FM24_HAL_GET_BUSY()				(uint16_t)(i2c->SR2 & I2C_SR2_BUSY)
#define FM24_HAL_GET_GENCALL()			(uint16_t)(i2c->SR2 & I2C_SR2_GENCALL)
#define FM24_HAL_GET_DUALF()			(uint16_t)(i2c->SR2 & I2C_SR2_DUALF)
#define FM24_HAL_GET_TRA()				(uint16_t)(i2c->SR2 & I2C_SR2_TRA)
#define FM24_HAL_GET_OVR()				(uint16_t)(i2c->SR1 & I2C_SR1_OVR)
#define FM24_HAL_GET_AF()				(uint16_t)(i2c->SR1 & I2C_SR1_AF)
#define FM24_HAL_GET_ARLO()				(uint16_t)(i2c->SR1 & I2C_SR1_ARLO)
#define FM24_HAL_GET_BERR()				(uint16_t)(i2c->SR1 & I2C_SR1_BERR)

FM24_Context_t FM24_Conext;

/**
  * @brief
  * @param
  * @retval
  */

/**
  * @brief	Initialize the peripheral and all related clocks, GPIOs, DMA and 
  *			Interrupts according to the specified parameters in the 
  *			FM24_Context_t structure.
  * @param
  * @retval NO_ERROR or FM24_FAIL 
  */
ERROR_t FM24_Init(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;
	register FM24_Context_t *ds = &FM24_Conext;

	FM24_HAL_DISABLE_DEV();
	FM24_I2C_HAL_GPIODeInit();
	FM24_I2C_HAL_DMADeInit();
	I2C_RCC_RESET(FM24_I2C_CLK);		/* Reset I2Cx device clock in order to avoid non-cleared error flags */

	FM24_I2C_HAL_GPIOInit();
	I2C_Init(FM24_I2C, (I2C_InitTypeDef *)&FM24_I2C_InitData);
	FM24_HAL_ENABLE_DEV();

	FM24_InitInt();

	ds->State = FM24_STATE_READY;

	return FM24_IsDeviceReady();
}

/**
  * @brief  Deinitialize the IO pins used by the I2C device 
  *         (configured to their default state).
  * @param
  * @retval
  */
#define FM24_I2C_HAL_GPIODeInit()	PinInit32(FM24PinDeInit)

/**
  * @brief  Configure the IO pins used by the I2C device.
  * @param
  * @retval
  */
void FM24_I2C_HAL_GPIOInit(void)
{
	register int retry_count_clk;
	register int retry_count;

	PinInit32(FM24PinOut);

	retry_count_clk = 10;
	while (PIN_READ(FM24_SDA) == 0 && retry_count_clk != 0)
	{
		retry_count_clk--;
		retry_count = 10;
		/* Set clock high and wait for any clock stretching to finish. */
		PIN_SET(FM24_SCL);
		while (PIN_READ(FM24_SCL) == 0 && retry_count != 0)
		{
			retry_count--;
			DelayMicro(1);
		}
		DelayMicro(2);

		PIN_RESET(FM24_SCL);
		DelayMicro(2);

		PIN_SET(FM24_SCL);
		DelayMicro(2);
	}

	/* Generate a start then stop condition */
	DelayMicro(2);
	
	PIN_RESET(FM24_SDA);
	DelayMicro(2);

	PIN_SET(FM24_SDA);
	DelayMicro(2);

	retry_count = 10;
	while (PIN_READ(FM24_SCL) == 0 && retry_count != 0)
	{
		retry_count--;
		DelayMicro(1);
	}

	retry_count = 10;
	while (PIN_READ(FM24_SDA) == 0 && retry_count != 0)
	{
		retry_count--;
		DelayMicro(1);
	}

	PinInit32(FM24PinI2C);
}

/**
  * @brief  Deinitialize the DMA channel used by I2C Device(configured to their default state).
  *         DMA clock is not disabled. 
  * @param
  * @retval None.
  */
void FM24_I2C_HAL_DMADeInit(void)
{
	DMA_DeInit(FM24_DMA_TX_Channel);  
	DMA_DeInit(FM24_DMA_RX_Channel);  
}

/**
  * @brief	Wait until target device is ready for communication (This function is used with Memory devices).
  * @param
  * @retval	NO_ERROR or FM24_FAIL
  */
ERROR_t FM24_IsDeviceReady(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;
	register FM24_Context_t * ds = &FM24_Conext;
	register uint32_t timeout;

	ds->State = FM24_STATE_BUSY;

	FM24_HAL_DISABLE_ERRIT();
	FM24_HAL_DISABLE_DEV();
	FM24_HAL_ENABLE_DEV();

	timeout = FM24_I2C_TIMEOUT_SB;
	FM24_HAL_START();
	while (FM24_HAL_GET_SB() == 0 && timeout != 0)
	{
		--timeout;
	}
	if (timeout == 0)
	{
		ds->State = FM24_STATE_ERROR;
	}
	else
	{
		FM24_HAL_SEND(FM24_ADDRESS_WRITE);
		while (FM24_HAL_GET_ADDR() == 0 && timeout != 0)
		{
			--timeout;
		}
		if (timeout == 0)
			ds->State = FM24_STATE_ERROR;

	}

	FM24_HAL_CLEAR_AF();
	FM24_HAL_STOP();
	while (FM24_HAL_GET_BUSY());

	FM24_HAL_DISABLE_DEV();
	FM24_HAL_ENABLE_DEV();

	FM24_HAL_ENABLE_ACK();
	FM24_HAL_ENABLE_ERRIT();

	ds->State = FM24_STATE_READY;

	return (timeout == 0)  ? FM24_NOT_FOUND : NO_ERROR;
}

#endif
