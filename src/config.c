#include <string.h>
#include "FM24.h"
#include "config.h"
#include "version.h"
#include "MemoryLayout.h"

ConfigHost_t	ConfigHost;
ConfigLocal_t	ConfigLocal;
Parameters_t	Parameters;

#define CPUID_BASE	((uint8_t *)0x1FFFF7E8)

void ConfigChanged(void)
{
	Parameters.Status |= PARAMS_STATUS_CHANGE;
}

bool CheckDS18B20(uint8_t * id)
{
	if (memcmp(&Parameters.DS18B20Id, id, sizeof(DS18B20Id_t)) != 0)
	{
		memcpy (&Parameters.DS18B20Id, id, sizeof(DS18B20Id_t));
		ConfigChanged();
		return false;
	}
	return true;
}

void GetCpuId(CpuId_t *dst)
{
	memcpy(dst, (void *)CPUID_BASE, sizeof(CpuId_t));
}

bool CheckCpuId(void)
{
	if (memcmp(&Parameters.CpuId, CPUID_BASE, sizeof(CpuId_t)) != 0)
	{
		memcpy (&Parameters.CpuId, CPUID_BASE, sizeof(CpuId_t));
		ConfigChanged();
		return false;
	}
	return true;
}

ERROR_t ConfigInit(void)
{
	register ConfigHost_t  * h_cfg	= &ConfigHost;
	register ConfigLocal_t * l_cfg	= &ConfigLocal;
	register Parameters_t  * param	= &Parameters;

	if (NO_ERROR == VersionLoadEx(h_cfg, FM24_OFFSET(ConfigHost), sizeof(ConfigHost_t))
	&&	NO_ERROR == VersionLoadEx(l_cfg, FM24_OFFSET(ConfigLocal), sizeof(ConfigLocal_t))
	&&	NO_ERROR == VersionLoadEx(param, FM24_OFFSET(Parameters), sizeof(Parameters_t))
		)
	{
		Parameters.Status &= ~PARAMS_STATUS_CHANGE;
		return NO_ERROR;
	}

	DEBUG("Set default configuration\n");

	h_cfg->Version.AddressX = 0;

	h_cfg->PreChargeEnter = DEF_PRECHARGE_ENTER;
	h_cfg->PreChargeExit  = DEF_PRECHARGE_EXIT;
	h_cfg->ChargerMaxCurrent = DEF_CHARGER_MAX_CURRENT;
	h_cfg->AdcPeriodTicks = DEF_ADC_PERIOD_TICKS;

	l_cfg->Version.AddressX = 0;
	
	l_cfg->Adc_Uin_Mul = DEF_ADC_UIN_MUL;
	l_cfg->Adc_Uin_Div = DEF_ADC_UIN_DIV;

	l_cfg->Adc_Ubat_Mul = DEF_ADC_UBAT_MUL;
	l_cfg->Adc_Ubat_Div = DEF_ADC_UBAT_DIV;

	l_cfg->Adc_Ibat_M_Mul = DEF_ADC_IBAT_M_MUL;
	l_cfg->Adc_Ibat_M_Div = DEF_ADC_IBAT_M_DIV;

	l_cfg->Adc_Ibat_P_Mul = DEF_ADC_IBAT_P_MUL;
	l_cfg->Adc_Ibat_P_Div = DEF_ADC_IBAT_P_DIV;

	GetCpuId(&param->CpuId);
	ConfigChanged();

	param->Version.AddressX = 0;
	return SYS_CFG_CHANGE;
}

ERROR_t ConfigSave(void)
{
	if (Parameters.Status & PARAMS_STATUS_CHANGE)
	{
		if (NO_ERROR == LogError2("CFG", VersionSaveEx(&ConfigHost, FM24_OFFSET(ConfigHost), sizeof(ConfigHost_t)))
		&&	NO_ERROR == LogError2("CFG", VersionSaveEx(&ConfigLocal, FM24_OFFSET(ConfigLocal), sizeof(ConfigLocal_t)))
		&&	NO_ERROR == LogError2("CFG", VersionSaveEx(&Parameters, FM24_OFFSET(Parameters), sizeof(Parameters_t)))
			)
		{
			Parameters.Status &= ~PARAMS_STATUS_CHANGE;
			DEBUG("Configuration save\n");
			return NO_ERROR;
		}
		return SYS_CFG_CHANGE;
	}
	return NO_ERROR;
}
