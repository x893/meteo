#include <string.h>

#include "common.h"
#include "board.h"
#include "RingBuffer.h"
#include "config.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define GSM_GPRS_TIMEOUT	90000
#define GSM_TCP_TIMEOUT		90000

enum
{
	GSM_STATUS_POWEROFF = 0,
	GSM_STATUS_REGISTERED = 1,
	GSM_STATUS_GPRS = 2,
};

enum
{
	GSM_MODE_IGNORE = 0,
	GSM_MODE_SAVE,
	GSM_MODE_READY,
	GSM_MODE_ERROR,
};

typedef struct GsmContext_s
{
		  char *RxBuffer;
	const char *TxBuffer;
	SemaphoreHandle_t	ReadySemaphore;
	QueueHandle_t		TaskQueue;

	uint16_t RxCount;
	uint16_t RxStart;
	uint8_t Status;
	bool	Online;
	uint8_t State;
	char	IMEI[24];

} GsmContext_t;

char GsmRxBuffer[4096];
char GsmTxBuffer[256];
GsmTask_t GsmTaskCurrent;
GsmContext_t GsmContext;

const char GSM_AT[]			= "AT\r";
const char GSM_E0[]			= "ATE0\r";
const char GSM_CMEE[]		= "AT+CMEE=2\r";
const char GSM_CFUN1[]		= "AT+CFUN=1\r";
const char GSM_GSN[]		= "AT+GSN\r";
const char GSM_GET_CPIN[]	= "AT+CPIN?\r";
const char GSM_CPIN_RDY[]	= "+CPIN: READY";
const char GSM_PSSTKI[]		= "AT*PSSTKI=0\r";
const char GSM_CMGF1[]		= "AT+CMGF=1\r";
const char GSM_CSDH[]		= "AT+CSDH=0\r";
const char GSM_CIMI[]		= "AT+CIMI\r";

const char GSM_CMR[]		= "AT+GMR\r";

const char GSM_CREG[]		= "AT+CREG?\r";
const char GSM_CREG01[]		= "+CREG: 0,1";
const char GSM_CREG03[]		= "+CREG: 0,3";
const char GSM_CREG05[]		= "+CREG: 0,5";

const char GSM_SMS_LIST[]	= "AT+CMGL=\"ALL\"\r";

const char GSM_GPRS0[]		= "AT+WIPCFG=1\r";
const char GSM_GPRS1[]		= "AT+WIPBR=1,6\r";
const char GSM_GPRS2[]		= "AT+WIPBR=2,6,11,\"";
const char GSM_GPRS3[]		= "AT+WIPBR=2,6,0,\"";
const char GSM_GPRS4[]		= "AT+WIPBR=2,6,1,\"";
const char GSM_GPRS5[]		= "AT+WIPBR=4,6,0\r";
const char GSM_GPRS6[]		= "AT+WIPCREATE=2,1,\""HTTP_SERVER_ADDRESS"\","HTTP_SERVER_PORT"\r";
const char GSM_GPRS7[]		= "AT+WIPDATA=2,1,1\r";
const char GSM_GRPS8[]		= "AT+WIPCLOSE=2,1\r";
const char GSM_GPRS9[]		= "AT+WIPBR=5,6\r";

//	GPRS settings for mobile operators
const char GSM_MEGAFON_APN[]	= "internet";
const char GSM_MEGAFONE_USER[]	= "gdata";
const char GSM_MEGAFONE_PASS[]	= "gdata";
/*
const char GSM_MTS_APN[]		= "internet.mts.ru";
const char GSM_MTS_USER[]		= "mts";
const char GSM_MTS_PASS[]		= "mts";

const char GSM_MTSM2M_APN[]		= "m2m.msk";
const char GSM_MTSM2M_USER[]	= "mts";
const char GSM_MTSM2M_PASS[]	= "mts";

const char GSM_BEELINE_APN[]	= "internet.beeline.ru";
const char GSM_BEELINE_USER[]	= "beeline";
const char GSM_BEELINE_PASS[]	= "beeline";

const char GSM_BEELINEM2M_APN[]	= "m2m.beeline.ru";
const char GSM_BEELINEM2M_USER[]= "beeline";
const char GSM_BEELINEM2M_PASS[]= "beeline";

const char GSM_TELE2_APN[]		= "internet.tele2.ru";
const char GSM_TELE2_USER[]		= "";
const char GSM_TELE2_PASS[]		= "";
*/

const char GSM_LINE_END[]	= "\"\r";

const char GSM_RESP_OK[]		= "OK";
const char GSM_RESP_ERROR[]		= "ERROR";
const char GSM_RESP_CME_ERROR[]	= "+CME ERROR:";
const char GSM_RESP_CMS_ERROR[]	= "+CMS ERROR:";
const char GSM_RESP_CONNECT[]	= "CONNECT";
const char GSM_RESP_SHUTDOWN[]	= "SHUTDOWN";
const char GSM_RESP_NO_CARRIER[]= "NO CARRIER";
const char GSM_RESP_READY[]		= "+WIPREADY: 2,1";

const char GSM_GPRS_SET[]		= "AT+WIPBR=1,6\r";
const char GSM_GPRS_APN[]		= "AT+WIPBR=2,6,11,\"";
const char GSM_GPRS_USER[]		= "AT+WIPBR=2,6,0,\"";
const char GSM_GPRS_PASS[]		= "AT+WIPBR=2,6,1,\"";

const PinInit32_t ModemPinInit[] =		{	MODEM_INIT			{ NULL }	};
const PinInit32_t ModemPinDeinit[] = 	{	MODEM_DEINIT		{ NULL }	};
const PinInit16_t ModemUartInit[] = 	{	MODEM_UART_INIT		{ NULL }	};

const char GSM_DATA_PING[]		=
"GET /stationfw/v1/fwinfoall.json HTTP/1.1\r\n"
"Host: wind.openweathermap.org:8080\r\n"
"Connection: Close\r\n"
"User-Agent: "HTTP_USER_AGENT"\r\n"
"\r\n"
;

/**
  * @brief
  * @param
  * @retval
  */
void GsmFlushRx(register GsmContext_t *gc)
{
	gc->State = GSM_MODE_IGNORE;
	gc->RxBuffer = GsmRxBuffer;
	gc->RxCount = 0;
	gc->RxStart = 0;
	while (xSemaphoreTake(gc->ReadySemaphore, 1) == pdTRUE)
		;
}

/**
  * @brief
  * @param
  * @retval
  */
void GsmSendString( const char *cmd )
{
	if (cmd != NULL)
	{
		DEBUG(cmd);
		while (GsmContext.TxBuffer != NULL)
			vTaskDelay(2);
		GsmContext.TxBuffer = cmd;
		MODEM_TX_INT_ENABLE();
	}
}

/**
  * @brief	Send command to GSM and wait response
  * @param	*cmd	command to send
  *			timeout	millisonds to wait
  * @retval	error code
  */
ERROR_t GsmSendTimeout( const char *cmd, uint32_t timeout )
{
	register int idx = 0;
	register GsmContext_t *gc = &GsmContext;
	register ERROR_t result = GSM_TIMEOUT;
	register char *line;

	GsmFlushRx(gc);
	gc->State = GSM_MODE_SAVE;

	GsmSendString(cmd);
	debug_putc('\n');

	while (result == GSM_TIMEOUT)
	{
		if (xSemaphoreTake(gc->ReadySemaphore, timeout) == pdFALSE)
		{
			// No line from modem within timeout
			break;
		}
		line = &GsmRxBuffer[idx];
		DEBUG(line);
		DEBUG("\n");

		if (0 == strcmp( line, GSM_RESP_OK )
		||	0 == strcmp( line, GSM_RESP_CONNECT )
			)
		{
			result = NO_ERROR;
		}
		else
		if (0 == strcmp(line, GSM_RESP_ERROR )
		||	0 == strncmp(line, GSM_RESP_CME_ERROR, STRING_SIZE(GSM_RESP_CME_ERROR))
		||	0 == strncmp(line, GSM_RESP_CMS_ERROR, STRING_SIZE(GSM_RESP_CMS_ERROR))
			)
		{
			result = GSM_ERROR;
		}
		else
		if (0 == strcmp(line, GSM_RESP_SHUTDOWN))
		{
			result = GSM_ERROR;
		}
		else
		{
			// Move index to tnd of line
			while (GsmRxBuffer[idx++] != '\0')
				;
		}
	}
	gc->State = GSM_MODE_IGNORE;
	return result;
}

ERROR_t GsmSend( const char *cmd )
{
	return GsmSendTimeout( cmd, 1000 );
}

ERROR_t GsmSend3( const char *cmd, const char *param1, const char *param2 )
{
	GsmSendString(cmd);
	GsmSendString(param1);
	return GsmSendTimeout( param2, 1000 );
}

/**
  * @brief	Deinitialize GSM hardware
  * @param	context pointer
  * @retval
  */
void GsmDeinit( register GsmContext_t *gc )
{
	int delay = 120;

	gc->Status = GSM_STATUS_POWEROFF;
	GsmSendTimeout("AT+CPOF\r\n", 15000);

	PinInit32(ModemPinDeinit);		// Deinitialize modem pins
	while (delay != 0 && PIN_READ(MODEM_READY) != 0)
	{
		delay--;
		vTaskDelay(100);
	}
	MODEM_PWR_OFF();
}

/**
  * @brief	Fast deinitialize GSM hardware
  * @param
  * @retval
  */
void GsmOff(void)
{
	PinInit32(ModemPinDeinit);		// Deinitialize modem pins
	MODEM_PWR_OFF();
}

/**
  * @brief	Initialize GSM hardware
  * @param	context pointer
  * @retval
  */
ERROR_t GsmInit( register GsmContext_t *gc )
{
	register int16_t delay;

	MODEM_PWR_ON();
	vTaskDelay(300);				// Wait for VBATT stable
	MODEM_ONOFF_LOW();				// Set ON/~OFF low
	vTaskDelay(700);				// 685 ms delay
	delay = 800;
	while (PIN_READ(MODEM_READY) == 0)	// Wait for MODEM_READY high
	{
		if (delay == 0)				// 800 * 10 ms = 8000 ms timeout
		{							// No MODEM_READY high
			MODEM_ONOFF_HIGH();		// Set high on ON/~OFF
			MODEM_PWR_OFF();		// Power off
			return GSM_NOT_FOUND;
		}
		--delay;
		vTaskDelay(10);
	}
	MODEM_ONOFF_HIGH();				// Set high on ON/~OFF
	PinInit32(ModemPinInit);		// Initialize modem pins
	PinInit16(ModemUartInit);

	for (delay = 10; delay != 0; --delay)
	{
		if (NO_ERROR == GsmSend( GSM_AT ))
			break;
	}
	if (delay != 0
	&&	NO_ERROR == GsmSend( GSM_AT )
	&&	NO_ERROR == GsmSend( GSM_E0 )
	&&	NO_ERROR == GsmSend( GSM_CMEE )
	&&	NO_ERROR == GsmSend( GSM_CFUN1 )
	&&	NO_ERROR == GsmSend( GSM_GSN )
	&&	strlen(GsmRxBuffer) == 15
		)
	{
		strcpy(&gc->IMEI[0], GsmRxBuffer);
		GsmSend( GSM_CMR );
		return NO_ERROR;
	}

	GsmDeinit(gc);
	return GSM_NOT_FOUND;
}

/**
  * @brief
  * @param
  * @retval
  */
bool GsmFindToken( const char *pattern, register GsmContext_t *gc )
{
	register char *src = gc->RxBuffer;
	if (gc->RxCount > 0)
		while ((src - gc->RxBuffer) < gc->RxCount)
		{
			if (strcmp(pattern, src) == 0)
				return true;
			while (*src++ != '\0')
				;
		}
	return false;
}

/**
  * @brief
  * @param
  * @retval
  */
bool GsmFindTokenStartWith( const char *pattern, register GsmContext_t *gc )
{
	register char *src = gc->RxBuffer;
	if (gc->RxCount > 0)
		while ((src - gc->RxBuffer) < gc->RxCount)
		{
			if (strncmp(pattern, src, strlen(pattern)) == 0)
				return true;
			while (*src++ != '\0')
				;
		}
	return false;
}

/**
  * @brief	Initialize GSM hardware
  * @param	context pointer
  * @retval
  */
ERROR_t GsmCheckSIM( register GsmContext_t *gc )
{
	register int retry = 20;
	for (retry = 0; retry < 20; ++retry)
	{
		if (NO_ERROR == GsmSend(GSM_GET_CPIN)
		&&	GsmFindToken(GSM_CPIN_RDY, gc)
		&&	NO_ERROR == GsmSend( GSM_PSSTKI )
		&&	NO_ERROR == GsmSend( GSM_CMGF1 )
		&&	NO_ERROR == GsmSend( GSM_CSDH )
			)
			return NO_ERROR;
		vTaskDelay(1000);
	}
	return GSM_SIM_FAIL;
}

/**
 * @brief
 * @param
 * @retval
 */
ERROR_t GsmRegister(register GsmContext_t * gc)
{
	register int16_t retry;

	if (gc->Status == GSM_STATUS_REGISTERED)
		return NO_ERROR;

	if (gc->Status == GSM_STATUS_POWEROFF)
	{
		if (NO_ERROR != GsmInit( gc ))
			return LogError2("Gsm Power Fail", GSM_NOT_FOUND);
		if (NO_ERROR != GsmCheckSIM(gc))
			return LogError2("GSM SIM fail", GSM_SIM_FAIL);
	}

	if (NO_ERROR == GsmSend(GSM_AT))
	{
		for (retry = 90; retry != 0; --retry)
		{
			if (NO_ERROR == GsmSend(GSM_CREG)
			&&	(GsmFindToken("+CREG: 0,1", gc) || GsmFindToken("+CREG: 0,5", gc))
				)
				break;
			vTaskDelay(1000);
		}
		if (retry != 0)
		{
			gc->Status = GSM_STATUS_REGISTERED;
			return NO_ERROR;
		}
	}

	GsmDeinit(gc);
	return LogError2("GSM Not Registered", GSM_NOT_REGISTER);
}

/**
 * @brief
 * @param
 * @retval
 */
ERROR_t GsmGprsStop(register GsmContext_t * gc)
{
	if (gc->Status == GSM_STATUS_GPRS)
	{
		vTaskDelay(1000);
		if (NO_ERROR == GsmSendTimeout("+++", 10000)
		||	NO_ERROR == GsmSend(GSM_AT)
			)
		{
			if (NO_ERROR == GsmSend(GSM_GRPS8)
			&&	NO_ERROR == GsmSend(GSM_GPRS9)
				)
			{
				gc->Status = GSM_STATUS_REGISTERED;
				return NO_ERROR;
			}
		}
		return GsmRegister(gc);
	}
	return NO_ERROR;
}

/**
 * @brief
 * @param
 * @retval
 */
ERROR_t GsmGprsStart(register GsmContext_t * gc)
{
	if (gc->Status == GSM_STATUS_REGISTERED)
	{
		if (NO_ERROR == GsmSend( GSM_CIMI ))
		{
			if (GsmFindTokenStartWith("25002", gc))
			{
				DEBUG("MegaFon OJSC\n");
				if (NO_ERROR == GsmSend(GSM_GPRS0)
				&&	NO_ERROR == GsmSend(GSM_GPRS1)
				&&	NO_ERROR == GsmSend3(GSM_GPRS2, GSM_MEGAFON_APN, GSM_LINE_END)
				&&	NO_ERROR == GsmSend3(GSM_GPRS3, GSM_MEGAFONE_USER, GSM_LINE_END)
				&&	NO_ERROR == GsmSend3(GSM_GPRS4, GSM_MEGAFONE_PASS, GSM_LINE_END)
					)
				{
				}
			}
			else
			{
				DEBUG("Unknown provider\n");
				return GSM_SIM_FAIL;
			}
		}

		if (NO_ERROR == GsmSendTimeout(GSM_GPRS5, GSM_GPRS_TIMEOUT)
		&&	NO_ERROR == GsmSendTimeout(GSM_GPRS6, GSM_TCP_TIMEOUT )
			)
		{
			gc->Online = true;
			if (NO_ERROR == GsmSendTimeout(GSM_GPRS7, GSM_TCP_TIMEOUT))
			{
				gc->Online = false;
				gc->Status = GSM_STATUS_GPRS;
				return NO_ERROR;
			}
			gc->Online = false;
		}
		return GSM_NO_GPRS;
	}
	return GSM_NOT_REGISTER;
}

/**
 * @brief	Add GSM Task
 * @param
 * @retval
 */
ERROR_t GsmAddTask(GSM_COMMAND_t cmd, const char * data, pf_GSM_Callback pfCallback)
{
	if (GsmContext.TaskQueue != NULL)
	{
		GsmTask_t task;
		task.pfCallback = pfCallback;
		task.TaskData.Command = cmd;
		task.TaskData.Data = data;
		task.TaskData.Error = SYS_OS_TASK_ADD;
		if (xQueueSend( GsmContext.TaskQueue, &task, 500 ) == pdTRUE)
			return NO_ERROR;
	}
	return GSM_TASK_FAIL;
}

/**
 * @brief	GSM Task
 * @param
 * @retval
 */
void GsmTask(void *pvArgs)
{
	register GsmContext_t *gc = &GsmContext;
	(void)pvArgs;

	gc->ReadySemaphore = xSemaphoreCreateBinary();
	gc->TaskQueue = xQueueCreate(GSM_TASK_QUEUE_SIZE, sizeof(GsmTask_t));
	if (gc->TaskQueue == NULL
	||	gc->ReadySemaphore == NULL
//	||	NO_ERROR != GsmAddTask(GSM_CHECK_SMS, NULL, NULL)
//	||	NO_ERROR != GsmAddTask(GSM_SEND_HTTP, GSM_DATA_PING, NULL)
		)
	{
		LogError2("GSM", GSM_TASK_FAIL );
		while (1)
			vTaskSuspend(NULL);
	}

	while (1)
	{
		if (xQueueReceive(gc->TaskQueue, &GsmTaskCurrent, GSM_TASK_TIMEOUT) != pdPASS)
		{	// No task, Power off modem
			if (gc->Status != GSM_STATUS_POWEROFF)
				GsmDeinit(gc);
			continue;
		}

		register GsmTaskData_t * pd = &GsmTaskCurrent.TaskData;
		if (pd->Command == GSM_STOP)
		{
			GsmDeinit(gc);
			pd->Error = NO_ERROR;
		}
		// Process comands with network registration
		else if (NO_ERROR == (pd->Error = GsmRegister(gc)))
		{
			if (pd->Command == GSM_CHECK_SMS)
			{
				if (NO_ERROR == (pd->Error = GsmSend(GSM_SMS_LIST)))
				{
				}
			}
			else if (pd->Command == GSM_SEND_HTTP)
			{
				if (NO_ERROR == (pd->Error = GsmGprsStart(gc)))
				{	// Send data to server
					if (pd->Data != NULL
					&&	NO_ERROR != GsmSendTimeout(pd->Data, 30000)
						)
					{

					}
					GsmGprsStop(gc);
				}
			}
		}
		if (GsmTaskCurrent.pfCallback != NULL)
			(GsmTaskCurrent.pfCallback)(pd);
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void MODEM_IRQHandle(void)
{
	register char ch;
	register USART_TypeDef *usart = MODEM_UART;
	register GsmContext_t  *gc = &GsmContext;

	if (usart->SR & USART_SR_RXNE)
	{
		ch = usart->DR;
		if (usart->SR & (USART_SR_PE | USART_SR_FE | USART_SR_NE | USART_SR_ORE))
		{
			ch = usart->DR;
			gc->State = GSM_MODE_ERROR;
		}
		else if (gc->State == GSM_MODE_SAVE && ch != '\r')
		{
			if (ch == '\n')
			{
				if (gc->RxStart != gc->RxCount)
				{
					ch = '\0';
					gc->RxBuffer[gc->RxCount++] = ch;
					gc->RxStart = gc->RxCount;
				}
				else
					ch = 1;	// Ignore empty lines;
			}
			else
			{
				gc->RxBuffer[gc->RxCount++] = ch;
				if (gc->RxCount >= sizeof(GsmRxBuffer))
				{	// Response line too long
					gc->State = GSM_BAD_RESPONSE;
					ch = 0;
				}
				else if (gc->Online)
				{	// Check for CONNECT response in Online mode
					if ((gc->RxCount - gc->RxStart) == STRING_SIZE(GSM_RESP_CONNECT)
					&&	strncmp(
							&gc->RxBuffer[gc->RxStart],
							GSM_RESP_CONNECT,
							STRING_SIZE(GSM_RESP_CONNECT)
							) == 0
						)
					{
						ch = 0;
						gc->RxBuffer[gc->RxCount++] = ch;
						gc->RxStart = gc->RxCount;
					}
				}
			}

			if (ch == 0)
			{
				portBASE_TYPE RxNeedTaskSwitch = pdFALSE;
				xSemaphoreGiveFromISR( gc->ReadySemaphore, &RxNeedTaskSwitch );
				portEND_SWITCHING_ISR( RxNeedTaskSwitch );
			}
		}
	}

	if (usart->SR & USART_SR_TXE)
	{
		if (gc->TxBuffer != NULL)
		{
			usart->DR = *(gc->TxBuffer++);
			if (*(gc->TxBuffer) == '\0')
			{
				MODEM_TX_INT_DISABLE();
				gc->TxBuffer = NULL;
			}
		}
		else
		{
			MODEM_TX_INT_DISABLE();
		}
	}
}
