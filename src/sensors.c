#include "common.h"
#include "sensors.h"
#include "DS18B20.h"
#include "charger.h"
#include "adc.h"
#include "fw_update.h"
#include "config.h"
#include "rtc.h"

#include "FreeRTOS.h"
#include "task.h"

#define SdiDirectionSend()		PIN_RESET(SDI_DIR)
#define SdiDirectionReceive()	PIN_SET(SDI_DIR)

const PinInit32_t SDI_PinInit[]		= {	SDI_RS_INIT		{ NULL }};
const PinInit16_t SDI_UartInit[]	= {	SDI_UART_INIT	{ NULL }};

SdiContext_t SdiContext;

/*	External functions	*/
ERROR_t LogOsInit(void);
ERROR_t LedOsInit(void);
void GpsTask(void *pvArgs);
void GsmTask(void *pvArgs);

/**
  * @brief	SDI Power On/Off
  * @param
  * @retval
  */
void SdiPowerOn(void)	{	PIN_SET(DC_SDI);	}
void SdiPowerOff(void)	{	PIN_RESET(DC_SDI);	}

/**
  * @brief
  * @param
  * @retval
  */
void SdiBreak(void)
{
	SDI_RS_UART->CR1 &= ~USART_CR1_UE;
	PIN_MODE_SET(PIN_OUT_PP_2, SDI_RS_TX);
	vTaskDelay(15);
	PIN_MODE_SET(PIN_AF_PP_50, SDI_RS_TX);
	vTaskDelay(10);
	SDI_RS_UART->CR1 |= USART_CR1_UE;
}

/**
  * @brief	Disable all interrupts from SDI_RS
  * @param
  * @retval
  */
void SdiDisableIRQ(void)
{
	SDI_RS_UART->CR1 &= ~(USART_CR1_RXNEIE | USART_CR1_TCIE | USART_CR1_TXEIE);
}
/**
  * @brief	Enable RX, TX interrupts from SDI_RS
  * @param
  * @retval
  */
void SdiEnableIRQ(void)
{
	SDI_RS_UART->CR1 |= (USART_CR1_RXNEIE | USART_CR1_TXEIE);
}

/**
  * @brief
  * @param
  * @retval
  */
ERROR_t SdiSend(const char * cmd, register SdiContext_t  *gc)
{
	register int timeout = 5000 / 5;

	gc->TxBuffer = cmd;
	gc->Status = SDI_SEND;
	gc->RxIndex = 0;
	gc->TxIndex = 0;
	gc->Address = *cmd;

	SdiDirectionSend();
	SdiBreak();
	(void)SDI_RS_UART->DR;
	SDI_RS_UART->SR = ~(USART_SR_PE | USART_SR_FE | USART_SR_NE | USART_SR_ORE | USART_SR_IDLE | USART_SR_LBD | USART_SR_RXNE);
	NVIC_ClearPendingIRQ(SDI_RS_IRQn);

	SdiEnableIRQ();
	while (timeout != 0)
	{
		 if (gc->Status == SDI_SEND || gc->Status == SDI_RECEIVE)
			 vTaskDelay(5);
		 else
			 break;
		 --timeout;
	}
	SdiDisableIRQ();
	return gc->Status;
}

/**
  * @brief	Define power type (external or solar)
  * @param
  * @retval
  */
bool IsPowerExt(void)
{
	bool solar;
	uint32_t mode = PIN_MODE_GET(HEATER);
	PIN_MODE_SET(PIN_INPUT, HEATER);
	vTaskDelay(1);
	solar = (PIN_READ(HEATER) == 0);
	PIN_MODE_RESTORE(mode, HEATER);
	return solar;
}

/**
  * @brief
  * @param
  * @retval
  */
bool IsPowerSolar(void)
{
	return !IsPowerExt();
}

/**
  * @brief
  * @param
  * @retval
  */
bool IsCalibrate(void)
{
	return (PIN_READ(CALIBRATE_RQ) == 0);
}

/**
  * @brief
  * @param
  * @retval
  */
void EnableHEAT(void)
{
	PIN_SET(HEATER);
	PIN_MODE_SET(PIN_OUT_PP_2, HEATER);
	DEBUG("HEAT On\n");
}

/**
  * @brief
  * @param
  * @retval
  */
void DisableHEAT(void)
{
	PIN_RESET(HEATER);
	PIN_MODE_SET(PIN_INPUT, HEATER);
	DEBUG("HEAT Off\n");
}

/**
  * @brief
  * @param
  * @retval
  */
void ResetReason(void);
void LogDisplayLast(void);

void SensorsTask(void *pvArgs)
{
	(void)pvArgs;

	LogError2("LED", LedOsInit());
	LogError2("LOG", LogOsInit());
	LogDisplayLast();
	LogError2("RTC", RtcInit());

	ResetReason();	// We can save reset reason also in log

	DS18B20_Init();
	if (NO_ERROR == LogError2("DS18B20", DS18B20_GetID()))
	{
		int i;
		DEBUG("DS18B20 ID:");
		for (i = 0; i < 8; i++)
			PRINTF(" %02X", DS18B20_Data()[i]);
		DEBUG("\n");
		CheckDS18B20(DS18B20_Data());
	}
	// Read DS18B20 twice to reduce DS18B20 id change event
	else if (NO_ERROR == LogError2("DS18B20", DS18B20_GetID()))
	{
		int i;
		DEBUG("DS18B20 ID:");
		for (i = 0; i < 8; i++)
			PRINTF(" %02X", DS18B20_Data()[i]);
		DEBUG("\n");
		CheckDS18B20(DS18B20_Data());
	}

	CheckCpuId();

	if (IsCalibrate())
	{
		AdcCalibrate();
		while (1)
			;
	}

	ConfigSave();

	ChargerInit(100);
	ChargerPrecharge();

	PinInit32(SDI_PinInit);
	PinInit16(SDI_UartInit);
	SdiPowerOn();

	// Make copy of flash after program over debugger
	// LogError2("FW", FWCheckCopy());

	if (xTaskCreate(GsmTask,"GSM", GSM_STACK_SIZE, &MainContext, GSM_PRIORITY, &(MainContext.GsmTaskHandle)) != pdPASS)
		LogError2("GSM", SYS_OS_FAIL);
	if (xTaskCreate(GpsTask,"GPS", GPS_STACK_SIZE, &MainContext, GPS_PRIORITY, &(MainContext.GpsTaskHandle)) != pdPASS)
		LogError2("GPS", SYS_OS_FAIL);

	while (1)
	{
		ChargerPrecharge();
		{
			time_t time = RtcGetDatetime();
			PRINTF("RTC:%d %s\n", time, ctime(&time));
		}
/*
		register SdiContext_t  *gc = &SdiContext;

		if (NO_ERROR == LogError(DS18B20_Start()))
		{
			vTaskDelay(1000);
			if (NO_ERROR == LogError(DS18B20_Read()))
				PRINTF("Temp*10 : %d\n", (int16_t)(DS18B20_Celcius() * 10.0) );
		}

		if (IsPowerSolar())
			DEBUG("Solar Power\n");
		else
			DEBUG("External Power\n");

		if (NO_ERROR == LogError(SdiSend("b!", gc)))
		{
			if (NO_ERROR == LogError( SdiSend("bR0!", gc )))
			{
				DEBUG_LOG(SdiContext.RxBuffer);
			}
			else
			{
				DEBUG("SDI send 'bR0!' error\n");
			}
			if (NO_ERROR == LogError( SdiSend("bR1!", gc )))
			{
				DEBUG_LOG(SdiContext.RxBuffer);
			}
			else
			{
				DEBUG("SDI send 'bR1!' error\n");
			}
			if (NO_ERROR == LogError( SdiSend("bR2!", gc )))
			{
				DEBUG_LOG(SdiContext.RxBuffer);
			}
			else
			{
				DEBUG("SDI send 'bR2!' error\n");
			}
			if (NO_ERROR == LogError( SdiSend("bR3!", gc )))
			{
				DEBUG_LOG(SdiContext.RxBuffer);
			}
			else
			{
				DEBUG("SDI send 'bR3!' error\n");
			}
			if (NO_ERROR == LogError( SdiSend("bR4!", gc )))
			{
				DEBUG_LOG(SdiContext.RxBuffer);
			}
			else
			{
				DEBUG("SDI send 'bR4!' error\n");
			}
		}
		else
		{
			DEBUG("SDI send 'b!' error\n");
		}
*/
		vTaskDelay(30000);
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void SDI_RS_IRQHandle(void)
{
	register USART_TypeDef *usart = SDI_RS_UART;
	register SdiContext_t  *gc = &SdiContext;

	if (usart->SR & USART_SR_RXNE)
	{
		register char data = usart->DR & 0x7F;

		if (usart->SR & (USART_SR_PE | USART_SR_FE | USART_SR_NE | USART_SR_ORE))
		{
			gc->Status = SDI_EX_ERROR;
			data = usart->DR;
			SdiDisableIRQ();
			return;
		}
		else if (gc->Status == SDI_SEND || gc->Status == SDI_RECEIVE)
		{
			if (gc->RxIndex < gc->TxIndex && gc->TxBuffer[gc->RxIndex] != data)
			{
				gc->Status = SDI_EX_ERROR;
				SdiDisableIRQ();
				return;
			}
			if (gc->RxIndex == gc->TxIndex && data != gc->Address)
			{
				gc->Status = SDI_EX_ERROR;
				SdiDisableIRQ();
				return;
			}
			if (gc->RxIndex < (SDI_RX_BUFFER - 1))
			{
				gc->RxBuffer[gc->RxIndex++] = data;
				if (data == '\n' && gc->Status == SDI_RECEIVE)
				{
					gc->RxBuffer[gc->RxIndex] = '\0';
					gc->Status = NO_ERROR;
					SdiDisableIRQ();
				}
			}
			else
			{
				gc->Status = SDI_RESP_ERROR;
			}
		}
	}

	if (gc->Status == SDI_SEND)
	{
		if (usart->CR1 & USART_CR1_TXEIE)
		{
			if (usart->SR & USART_SR_TXE)
			{
				usart->DR = gc->TxBuffer[gc->TxIndex++];
				if (gc->TxBuffer[gc->TxIndex] == '\0')
				{
					usart->CR1 &= ~USART_CR1_TXEIE;
					usart->CR1 |=  USART_CR1_TCIE;
				}
			}
		}
		else if (usart->SR & USART_SR_TC)
		{
			SdiDirectionReceive();
			usart->CR1 &= ~USART_CR1_TCIE;
			gc->Status = SDI_RECEIVE;
		}
	}
}

void ResetReason(void)
{
	if (RCC_GetFlagStatus(RCC_FLAG_WWDGRST) != RESET)
	{
		DEBUG("RST:Window Watchdog Reset\n");
	}
	if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET)
	{
		DEBUG("RST:Independant Watchdog Reset\n");
	}
	if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET)
    {
		DEBUG("RST:Power-On Reset\n");
	}
    if (RCC_GetFlagStatus(RCC_FLAG_SFTRST) != RESET)
    {
		DEBUG("RST:Software Reset\n");
	}
    if (RCC_GetFlagStatus(RCC_FLAG_LPWRRST) != RESET)
    {
		DEBUG("RST:Low-Power Reset\n");
	}
    if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET)
	{
		DEBUG("RST:Pin Reset\r\n");
    }
    RCC_ClearFlag();
}
