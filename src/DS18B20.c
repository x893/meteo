#include "DS18B20.h"

typedef struct
{
	uint8_t Data[16];
	uint8_t	Buffer[8];
	uint8_t RxIndex;
	uint8_t TxIndex;
	uint8_t Bits;
} DS18B20Context_t;

const PinInit32_t DS18B20_PinInit[] =	{ DS18B20_INIT		{ NULL }};
const PinInit16_t DS18B20_UartInit[] =	{ DS18B20_UART_INIT	{ NULL }};

const uint8_t DS18B20_CMD_33[]	= { 0x33, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
const uint8_t DS18B20_CMD_44[]	= { 0xCC, 0x44 };
const uint8_t DS18B20_CMD_BE[]	= { 0xCC, 0xBE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

DS18B20Context_t DS18B20Context;

#define DS18B20_0	0x00
#define DS18B20_1	0xFF

/**
  * @brief  Convert one byte to 8 bytes array
  * @param
  * @param
  * @retval
  */
void DS18B20_ToBits(uint8_t data)
{
	register uint8_t i;
	for (i = 0; i < 8; i++)
	{
		DS18B20Context.Buffer[i] = (data & 0x01) ? DS18B20_1 : DS18B20_0;
		data = data >> 1;
	}
}

/**
  * @brief  Convert 8 bytes array to one byte
  * @param
  * @param
  * @retval
  */
uint8_t DS18B20_ToByte()
{
	register uint8_t data = 0, i;
	for (i = 0; i < 8; i++)
	{
		data = data >> 1;
		if (DS18B20Context.Buffer[i] == DS18B20_1)
			data |= 0x80;
	}
	return data;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
void DS18B20_Init()
{
	PinInit32(DS18B20_PinInit);
	PinInit16(DS18B20_UartInit);
	DS18B20_PIN_INIT();
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
void DS18B20_IRQHandler(void)
{
	register DS18B20Context_t * ds = &DS18B20Context;
	register USART_TypeDef *uart = DS18B20_UART;

	if (uart->SR & USART_SR_RXNE)
	{
		if (ds->RxIndex == ds->Bits)
		{
			(void)uart->DR;
			uart->CR1 &= ~USART_CR1_RXNEIE;
		}
		else
		{
			ds->Buffer[ds->RxIndex++] = uart->DR;
			if (ds->RxIndex == ds->Bits)
			{
				DS18B20_PIN_POWER();
			}
		}
	}

	if(uart->SR & USART_SR_TC)
	{
		if (ds->TxIndex == ds->Bits)
			uart->CR1 &= ~USART_CR1_TCIE;
		else
			uart->DR = ds->Buffer[ds->TxIndex++];
	}
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
ERROR_t DS18B20_SendBits(uint8_t bits)
{
	DS18B20_PIN_INIT();
	(void)DS18B20_UART->DR;

	DS18B20Context.RxIndex = DS18B20Context.TxIndex = 0;
	DS18B20Context.Bits = bits;
	DS18B20_UART->CR1 |= (USART_CR1_RXNEIE | USART_CR1_TCIE);

	vTaskDelay(5);
	DS18B20_UART->CR1 &= ~(USART_CR1_RXNEIE | USART_CR1_TCIE);
	return (DS18B20Context.RxIndex == bits) ? NO_ERROR : DS18B20_ERROR;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
ERROR_t DS18B20_Reset(void)
{
	DS18B20_UART->CR1 &= ~USART_CR1_UE;
	UartSetBaudrate(DS18B20_UART_BASE, 9600);
	DS18B20_UART->CR1 |=  USART_CR1_UE;

	DS18B20Context.Buffer[0] = 0xF0;
	DS18B20_SendBits(1);

	DS18B20_UART->CR1 &= ~USART_CR1_UE;
	UartSetBaudrate(DS18B20_UART_BASE, 115200);
	DS18B20_UART->CR1 |=  USART_CR1_UE;

	return (DS18B20Context.Buffer[0] == 0xF0) ? DS18B20_NO_DEVICE : NO_ERROR;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
ERROR_t DS18B20_Send (
	const uint8_t *command,
	uint8_t cLen,
	uint8_t *data,
	uint8_t dLen,
	uint8_t readStart
	)
{
	if (DS18B20_Reset() != NO_ERROR)
		return DS18B20_NO_DEVICE;

	while (cLen != 0)
	{
		cLen--;
		DS18B20_ToBits(*command++);
		if (NO_ERROR != DS18B20_SendBits(8))
			return DS18B20_ERROR;

		if (readStart == 0)
		{
			if (dLen != 0)
			{
				*data++ = DS18B20_ToByte();
				--dLen;
			}
		}
		else
			--readStart;
	}
	return NO_ERROR;
}

/**
  * @brief
  * @param
  * @param
  * @retval
  */
uint8_t DS18B20_Scan(uint8_t *buf, uint8_t num)
{
	uint8_t found = 0;
	uint8_t *lastDevice;
	uint8_t *curDevice = buf;
	uint8_t numBit, lastCollision, currentCollision, currentSelection;

	lastCollision = 0;
	while (found < num)
	{
		numBit = 1;
		currentCollision = 0;

		DS18B20_Send((const uint8_t *)"\xF0", 1, (void*)0, 0, 0);

		for (numBit = 1; numBit <= 64; numBit++)
		{
			DS18B20_ToBits(0xFF);
			DS18B20_SendBits(2);

			if (DS18B20Context.Buffer[0] == DS18B20_1)
			{
				if (DS18B20Context.Buffer[1] == DS18B20_1)
					return found;
				else
					currentSelection = 1;
			}
			else
			{
				if (DS18B20Context.Buffer[1] == DS18B20_1)
				{
					currentSelection = 0;
				}
				else
				{
					if (numBit < lastCollision)
					{
						if (lastDevice[(numBit - 1) >> 3] & (1 << ((numBit - 1) & 0x07)))
						{
							currentSelection = 1;
							if (currentCollision < numBit)
								currentCollision = numBit;
						} else {
							currentSelection = 0;
						}
					}
					else
					{
						if (numBit == lastCollision)
							currentSelection = 0;
						else
						{
							currentSelection = 1;
							if (currentCollision < numBit)
								currentCollision = numBit;
						}
					}
				}
			}

			if (currentSelection == 1)
			{
				curDevice[(numBit - 1) >> 3] |= 1 << ((numBit - 1) & 0x07);
				DS18B20_ToBits(0x01);
			}
			else
			{
				curDevice[(numBit - 1) >> 3] &= ~(1 << ((numBit - 1) & 0x07));
				DS18B20_ToBits(0x00);
			}
			DS18B20_SendBits(1);
		}
		found++;
		lastDevice = curDevice;
		curDevice += 8;
		if (currentCollision == 0)
			return found;

		lastCollision = currentCollision;
	}

	return found;
}

float DS18B20_Celcius(void)
{
	int16_t raw = (DS18B20Context.Data[1] << 8) | DS18B20Context.Data[0];
	switch (DS18B20Context.Data[4] & 0x60)
	{
		case 0x00:
			raw = raw & ~7;
			break;
		case 0x20:
			raw = raw & ~3;
			break;
		case 0x40:
			raw = raw & ~1;
			break;
	}
	return (float)raw / 16.0;
}

ERROR_t DS18B20_GetID(void)
{
	return DS18B20_Send(DS18B20_CMD_33, sizeof(DS18B20_CMD_33), DS18B20Context.Data, 8, 1);
}
ERROR_t DS18B20_Start(void)
{
	return DS18B20_Send(DS18B20_CMD_44, sizeof(DS18B20_CMD_44), NULL, 0, 0);
}
ERROR_t DS18B20_Read(void)
{
	return DS18B20_Send(DS18B20_CMD_BE, sizeof(DS18B20_CMD_BE), DS18B20Context.Data, 9, 2);
}
uint8_t * DS18B20_Data(void)
{
	return DS18B20Context.Data;
}
