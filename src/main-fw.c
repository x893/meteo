#include "common.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "FM24.h"
#include "charger.h"
#include "fw_update.h"
#include "config.h"
#include "adc.h"
#include "rtc.h"

MainContext_t MainContext;

/*	External functions	*/
void HardwareInit(void);
void SensorsTask(void *pvArgs);
void GsmOff(void);

/**
  * @brief
  * @param
  * @retval
  */
int main(void)
{
	SystemCoreClockUpdate();
	HardwareInit();

	LEDS_Off(LED_ALL);
	DEBUG("\nFW "FW_VERSION"\n");

	LogError2("LOG", LogInit());
	LogAdd(NO_ERROR, RtcGetDatetime(), "FW "FW_VERSION);

	LogError2("CFG", ConfigInit());
	LogError2("ADC", AdcInit());

	if (xTaskCreate(SensorsTask,"SNR", SNR_STACK_SIZE, &MainContext, SNR_PRIORITY, &(MainContext.SensorsTaskHandle)) == pdPASS)
	{
		vTaskStartScheduler();
	}

    LogError2("OS", SYS_OS_FAIL);
	ShowFatalError(SYS_OS_FAIL);
}

/**
  * @brief	Initialize led errors queue
  * @param
  * @retval	NO_ERROR or error
  */
ERROR_t LedOsInit(void)
{
	MainContext.ErrorQueue = xQueueCreate(8, sizeof(uint8_t));
	if (MainContext.ErrorQueue == NULL)
		return SYS_OS_FAIL;
	return NO_ERROR;
}

/**
  * @brief	Show error code via LED blinks
  * @param	error code
  * @retval	None
  */
void ShowError(ERROR_t error)
{
	uint8_t qe = error;
	if (MainContext.ErrorQueue != NULL)
		xQueueSend( MainContext.ErrorQueue, &qe, 10000 );
	else
		ShowErrorEx(error);
}

/**
  * @brief	Call from FreeRTOS on tick
  * @param
  * @retval
  */
void AdcStart(void);
void vApplicationTickHook(void)
{
	register MainContext_t *maincx = &MainContext;

	// Auto Start ADC conversion
	if (AdcContext.Time != 0)
	{
		--AdcContext.Time;
	}
	else
	{
		AdcContext.Time = ConfigHost.AdcPeriodTicks;
		// Check for ADC not disable and previous conversion complete
		// then start conversion
		// else wait next period
		if (!AdcContext.Disable && AdcContext.Ready)
				AdcStart();
	}

	// LED error processing
	if (maincx->ErrorState.U16 != 0)
	{
		if (maincx->ErrorTime != 0)
		{
			maincx->ErrorTime--;
		}
		else
		{
			// low byte = error code
			// high byte = state
			switch (maincx->ErrorState.U8[1])
			{
				case 0:		// Initialize error blinks
					maincx->ErrorTime = 2000;
					maincx->ErrorState.U8[1]++;
					LEDS_On(LED_2R);
					break;
				case 1:
					maincx->ErrorTime = 1000;
					maincx->ErrorState.U8[1]++;
					LEDS_Off(LED_2R);
					break;
				case 2:		// Digit
					if ((maincx->ErrorState.U8[0] & 0xF0) != 0)
					{
						LEDS_On(LED_2G);
						maincx->ErrorState.U8[1] = 3;
					}
					else
					if ((maincx->ErrorState.U8[0] & 0x0F) != 0)
					{
						LEDS_On(LED_2R);
						maincx->ErrorState.U8[1] = 4;
					}
					else
					{
						maincx->ErrorState.U8[1] = 5;
						maincx->ErrorTime = 1000;
					}
					maincx->ErrorTime += 500;
					break;
				case 3:
					LEDS_Off(LED_2G);
					maincx->ErrorTime = 500;
					maincx->ErrorState.U8[0] -= 0x10;
					maincx->ErrorState.U8[1] = 2;
					break;
				case 4:
					LEDS_Off(LED_2R);
					maincx->ErrorTime = 500;
					maincx->ErrorState.U8[0]--;
					maincx->ErrorState.U8[1] = 2;
					break;
				case 5:
					maincx->ErrorState.U16 = 0;
					break;
			}
		}
	}
}

/**
  * @brief
  * @param
  * @retval
  */
void vApplicationIdleHook(void)
{
	if (MainContext.ErrorQueue != NULL)
	{
		if (MainContext.ErrorState.U16 == 0
		&&	uxQueueMessagesWaiting(MainContext.ErrorQueue) != 0
			)
		{
			xQueueReceive(MainContext.ErrorQueue, &MainContext.ErrorState.U8[0], 1);
			MainContext.ErrorTime = 0;
		}
	}
}

/**
  * @brief	Reboot system
  * @param
  * @retval
  */
void RebootSystem(void)
{
	register uint16_t delay = 20;

	AdcContext.Disable = true;
	ChargerOff();
	GsmOff();

	// Wait 20 seconds to complete error LED notifications
	while (delay != 0)
	{
		if (MainContext.ErrorState.U16 == 0
		&&	uxQueueMessagesWaiting(MainContext.ErrorQueue) == 0
			)
			break;
		delay--;
		vTaskDelay(1000);
	}

	vTaskDelay(5000);

	NVIC_SystemReset();
}

/**
  * @brief
  * @param
  * @retval
  */
void vApplicationMallocFailedHook( void )
{
	LEDS_Set(LED_1R);
	DEBUG("vApplicationMallocFailedHook\n");
	DelayMilli(5000);
	NVIC_SystemReset();
}

/**
  * @brief
  * @param
  * @retval
  */
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	( void ) pxTask;
	( void ) pcTaskName;

	LEDS_Set(LED_1R);
	DEBUG("vApplicationStackOverflowHook\n");
	DelayMilli(5000);
	NVIC_SystemReset();
}

/**
  * @brief	Delay under RTOS
  * @param	milliseconds
  * @retval	None
  */
void Delay(uint16_t ms)
{
	if (MainContext.ErrorQueue == NULL)
		DelayMilli(ms);
	else
		vTaskDelay(ms);
}
