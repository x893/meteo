#include "common.h"
#include "board.h"
#include "system_stm32f10x.h"
#include "charger.h"

const PinInit32_t PinsInitData[] =
{
	DBG_INIT
	RCC_INIT
	PA_INIT
	PB_INIT
	PC_INIT
	PD_INIT
	PE_INIT
	{ NULL }
};

void DebugInit(void);

/**
  * @brief  Set USART baudrate
  * @param  usartxbase	USART base address
  *			baudrate	USART baud rate
  * @retval None
  */
void UartSetBaudrate( uint32_t usartxbase, uint32_t baudrate )
{
	RCC_ClocksTypeDef RCC_ClocksStatus;
	uint32_t tmpreg, apbclock;
	USART_TypeDef *usart = (USART_TypeDef *)usartxbase;
	uint32_t integerdivider;
	uint32_t fractionaldivider;

	RCC_GetClocksFreq( &RCC_ClocksStatus );
	apbclock = (usartxbase == USART1_BASE)
				? RCC_ClocksStatus.PCLK2_Frequency
				: RCC_ClocksStatus.PCLK1_Frequency;

	/* Determine the integer part */
	/* Integer part computing in case Oversampling mode is 8 Samples */
	integerdivider = (25 * apbclock) / (2 * baudrate);
	if ((usart->CR1 & USART_CR1_OVER8) == 0)
		/* Integer part computing in case Oversampling mode is 16 Samples */
		integerdivider /= 2;

	tmpreg = (integerdivider / 100) << 4;

	/* Determine the fractional part */
	fractionaldivider = integerdivider - (100 * (tmpreg >> 4));

	/* Implement the fractional part in the register */
	if ((usart->CR1 & USART_CR1_OVER8) != 0)
		tmpreg |= (((fractionaldivider * 8 + 50) / 100) & ((uint8_t)0x07));
	else /* if ((USARTx->CR1 & CR1_OVER8_Set) == 0) */
		tmpreg |= (((fractionaldivider * 16 + 50) / 100) & ((uint8_t)0x0F));

	/* Write to USART BRR */
	usart->BRR = (uint16_t)tmpreg;
}

/**
  * @brief	Hardware 32 bits register initialization
  * @param	pointer description
  * @retval	None
  */
void PinInit32(register const PinInit32_t *ports)
{
#ifdef STM32F103VG
	register volatile uint32_t *address;
	while ((address = ports->Address) != NULL)
	{
		if (address == SET_BAUD_RATE)
		{
			UartSetBaudrate(ports->Mask, ports->Value);
		}
		else if (ports->Mask == 0)
			*address = ports->Value;
		else
			*address = (*address & ports->Mask) | ports->Value;
		ports++;
	}
#else
	#error "Unknown MCU type"
#endif
}

/**
  * @brief	Hardware 16 bit register initialization
  * @param	pointer description
  * @retval	None
  */
void PinInit16(register const PinInit16_t *ports)
{
#ifdef STM32F103VG
	register volatile uint16_t *address;
	while ((address = ports->Address) != NULL)
	{
		if (ports->Mask == 0)
			*address = ports->Value;
		else
			*address = (*address & ports->Mask) | ports->Value;
		ports++;
	}
#else
	#error "Unknown MCU type"
#endif
}

/**
  * @brief  Initialize UART
  * @param	UART port
  * @retval	None
  */
void UartInit(register USART_TypeDef *uart, uint32_t baud)
{
	USART_InitTypeDef USART_InitStructure;
	USART_DeInit(uart);

	USART_InitStructure.USART_BaudRate = baud;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(uart, &USART_InitStructure);
	USART_Cmd(uart, ENABLE);
}

/**
  * @brief  Hardware initialization
  * @param  None
  * @retval None
  */
void HardwareInit(void)
{
#ifdef BOOTLOADER

	PinInit32(PinsInitData);

	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);// Enable SWJ Only
	GPIO_PinRemapConfig(GPIO_FullRemap_TIM1, ENABLE);		// Remap TIM1_CH1 to PE9 (PA8 w/o remap)
	GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);			// Remap USART2 to PD3 - PD6

#else

	NVIC_SetPriority(DS18B20_IRQn,	0);
	NVIC_SetPriority(SDI_RS_IRQn,	3);
	NVIC_SetPriority(RTC_IRQn,		4);
	NVIC_SetPriority(ADC_DMA_IRQn,	5);
	NVIC_SetPriority(MODEM_IRQn,	6);
	NVIC_SetPriority(GPS_IRQn,		7);
	NVIC_SetPriority(DEBUG_IRQn,	8);

#endif

	DebugInit();
}

/**
  * @brief  Delay microseconds via Data Watchpoint and Trace Unit
  * @param  delay in mucroseconds
  * @retval None
  */
void DelayMicro(uint16_t us)
{
	register uint32_t end = ((uint32_t) us) * (SystemCoreClock / 1000000UL);

	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
	DWT->CYCCNT = 0;

	while (DWT->CYCCNT < end)
	{ }
}

/**
  * @brief  Delay milliseconds via microseconds
  * @param  delay
  * @retval None
  */
void DelayMilli(uint16_t ms)
{
	while (ms != 0)
	{
		ms--;
		DelayMicro(1000);
	}
}

/**
  * @brief	Return mask for LEDs
  * @param
  * @retval
  */
uint32_t LEDS_Mask(LEDS_t leds)
{
	uint32_t mask = 0;
	if (leds & LED_1R)	mask |= PIN_MASK(LED1_R);
	if (leds & LED_1G)	mask |= PIN_MASK(LED1_G);
	if (leds & LED_2R)	mask |= PIN_MASK(LED2_R);
	if (leds & LED_2G)	mask |= PIN_MASK(LED2_G);
	return mask;
}

/**
  * @brief	Turn on selected LEDs
  * @param
  * @retval
  */
void LEDS_On(LEDS_t leds)
{
	PINS_SET(LEDS_PORT, LEDS_Mask(leds));
}

/**
  * @brief	Turn off selected LEDs
  * @param
  * @retval
  */
void LEDS_Off(LEDS_t leds)
{
	PINS_RESET(LEDS_PORT, LEDS_Mask(leds));
}

/**
  * @brief	Set all LEDs
  * @param
  * @retval
  */
void LEDS_Set(LEDS_t leds)
{
	PINS_SET(LEDS_PORT, (LEDS_PIN_RESET | LEDS_Mask(leds)));
}

/**
  * @brief	Toggle selected LEDs
  * @param
  * @retval
  */
void LEDS_Toggle(LEDS_t leds)
{
	PINS_OUT(LEDS_PORT, (PINS_READ_OUT(LEDS_PORT) ^ LEDS_Mask(leds)));
}
