#include "common.h"
#include "board.h"
#include "FM24.h"

#if 0

#define DMA_CCR_EN						((uint16_t)0x0001)
#define FM24_I2C_STATUS_ERR_MASK		((uint16_t)0x0F00)
#define FM24_I2C_STATUS1_EVT_MASK		((uint16_t)0x00DF)

#define I2C_GPIO_CLK_CMD(clk,cmd)		RCC_APB2PeriphClockCmd((clk),(cmd))
#define DMA_CLK_CMD(clk,cmd)			RCC_AHBPeriphClockCmd((clk),(cmd))

#define FM24_HAL_ENABLE_DMATX()			FM24_DMA_TX_Channel->CCR |= DMA_CCR_EN
#define FM24_HAL_ENABLE_DMARX()			FM24_DMA_RX_Channel->CCR |= DMA_CCR_EN
#define FM24_HAL_DISABLE_DMATX()		FM24_DMA_TX_Channel->CCR &= ~DMA_CCR_EN
#define FM24_HAL_DISABLE_DMARX()		FM24_DMA_RX_Channel->CCR &= ~DMA_CCR_EN

#define FM24_I2C_ENABLE_DMATX_TCIT()	FM24_DMA_TX_Channel->CCR |= DMA_IT_TC
#define FM24_I2C_ENABLE_DMATX_TEIT()	FM24_DMA_TX_Channel->CCR |= DMA_IT_TE
#define FM24_I2C_ENABLE_DMARX_TCIT()	FM24_DMA_RX_Channel->CCR |= DMA_IT_TC
#define FM24_I2C_ENABLE_DMARX_TEIT()	FM24_DMA_RX_Channel->CCR |= DMA_IT_TE
  
#define FM24_HAL_GET_DMATX_TCIT()		(uint32_t)(FM24_DMA->ISR & FM24_DMA_TX_TC_FLAG)
#define FM24_HAL_GET_DMATX_TEIT()		(uint32_t)(FM24_DMA->ISR & FM24_DMA_TX_TE_FLAG)
#define FM24_HAL_GET_DMARX_TCIT()		(uint32_t)(FM24_DMA->ISR & FM24_DMA_RX_TC_FLAG)
#define FM24_HAL_GET_DMARX_TEIT()		(uint32_t)(FM24_DMA->ISR & FM24_DMA_RX_TE_FLAG)
#define FM24_HAL_CLEAR_DMATX_IT()		FM24_DMA->IFCR = (FM24_DMA_TX_TC_FLAG | FM24_DMA_TX_TE_FLAG)
#define FM24_HAL_CLEAR_DMARX_IT()		FM24_DMA->IFCR = (FM24_DMA_RX_TC_FLAG | FM24_DMA_RX_TE_FLAG)

#define FM24_HAL_DMATX_GET_CNDT()		(uint32_t)(FM24_DMA_TX_Channel->CNDTR)
#define FM24_HAL_DMARX_GET_CNDT()		(uint32_t)(FM24_DMA_RX_Channel->CNDTR) 

#define FM24_HAL_SWRST()				i2c->CR1 |= I2C_CR1_SWRST; \
										i2c->CR1 &= ~I2C_CR1_SWRST

#define FM24_HAL_ENABLE_DMAREQ()		i2c->CR2 |= I2C_CR2_DMAEN
#define FM24_HAL_DISABLE_DMAREQ()		i2c->CR2 &= ~I2C_CR2_DMAEN

#define FM24_HAL_ENABLE_LAST()			i2c->CR2 |= I2C_CR2_LAST
#define FM24_HAL_DISABLE_LAST()			i2c->CR2 &= ~I2C_CR2_LAST

#define FM24_HAL_CLEAR_SB()				i2c->SR1; i2c->SR2
#define FM24_HAL_CLEAR_ADDR()			i2c->SR1; i2c->SR2
#define FM24_HAL_CLEAR_ERROR()			i2c->SR1 = ~FM24_I2C_STATUS_ERR_MASK

#define __FM24_I2C_TIMEOUT_DETECT	(	(ds->Timeout == FM24_I2C_TIMEOUT_MIN) 		\
									||	(ds->Timeout == FM24_I2C_TIMEOUT_DEFAULT)	\
									)

#define __FM24_I2C_TIMEOUT(cmd, timeout)						\
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + (timeout);				\
	while (((cmd) == 0) && (!__FM24_I2C_TIMEOUT_DETECT));		\
	if (__FM24_I2C_TIMEOUT_DETECT)								\
		return FM24_I2C_Timeout();								\
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT

#define __FM24_I2C_TIMEOUT_VOID(cmd, timeout)					\
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + (timeout);				\
	while (((cmd) == 0) && (!__FM24_I2C_TIMEOUT_DETECT));		\
	if (__FM24_I2C_TIMEOUT_DETECT)								\
	{															\
		FM24_I2C_Timeout();										\
		return;													\
	}															\
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT

ERROR_t FM24_I2C_Timeout(void);

DMA_InitTypeDef FM24_DMA_InitStructure;

/**
  * @brief
  * @param
  * @retval
  */
void FM24_InitInt(void)
{
	FM24_I2C_HAL_DMAInit();
	FM24_I2C_HAL_ITInit();
}

/**
  * @brief  Enable the DMA clock and initialize needed DMA Channels 
  *         used by the I2C device.
  * @param  Device : I2C Device instance.
  * @param  Direction : Transfer direction.
  * @param  Options :  Transfer Options.
  * @retval None. 
  */             
void FM24_I2C_HAL_DMAInit(void)
{  
	/* I2Cx Common Channel Configuration */
	FM24_DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	FM24_DMA_InitStructure.DMA_BufferSize = 0xFFFF;
	FM24_DMA_InitStructure.DMA_PeripheralInc =  DMA_PeripheralInc_Disable;
	FM24_DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	FM24_DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte ;
	FM24_DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	FM24_DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	FM24_DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	FM24_DMA_InitStructure.DMA_PeripheralBaseAddr = FM24_I2C_DR;

	FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_Init(FM24_DMA_TX_Channel, &FM24_DMA_InitStructure);

	FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_Init(FM24_DMA_RX_Channel, &FM24_DMA_InitStructure);
}

/**
  * @brief  Configure NVIC and interrupts used by I2C Device according to 
  *         enabled options
  * @param  Device : I2C Device instance.
  * @param  Options : I2C Transfer Options.
  * @retval None. 
  */
void FM24_I2C_HAL_ITInit(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;

	NVIC_EnableIRQ(FM24_EV_IRQn);	/* Enable the IRQ channel */
	NVIC_EnableIRQ(FM24_ER_IRQn);

	FM24_HAL_ENABLE_ERRIT();				/* Enable I2C Error Interrupts */

	NVIC_EnableIRQ(FM24_DMA_TX_IRQn);	/* Configure NVIC for DMA TX channel interrupt */
	FM24_I2C_ENABLE_DMATX_TCIT();			/* Enable DMA TX Channel TCIT  */
	FM24_I2C_ENABLE_DMATX_TEIT(); 

	NVIC_EnableIRQ(FM24_DMA_RX_IRQn);	/* Configure NVIC for DMA RX channel interrupt */
	FM24_I2C_ENABLE_DMARX_TCIT();  			/* Enable DMA RX Channel TCIT  */
	FM24_I2C_ENABLE_DMARX_TEIT(); 			/* Enable DMA RX Channel TEIT  */
}

/**
  * @brief	Initialize the peripheral and all related clocks, GPIOs, DMA and 
	*				 Interrupts according to the specified parameters in the 
	*				 FM24Context_t structure.
  * @param	pDevInitStruct : Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL 
  */
uint32_t FM24_I2C_Init(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;
	register FM24Context_t *ds = &FM24_Conext;

	/* If FM24_State is not BUSY */
	if (ds->State == FM24_STATE_READY
	||	ds->State == FM24_STATE_ERROR
	||	ds->State == FM24_STATE_DISABLED
		)
	{
		FM24_HAL_DISABLE_DEV();
		FM24_I2C_HAL_GPIODeInit();
		I2C_RCC_RESET(FM24_I2C_CLK);		/* Reset I2Cx device clock in order to avoid non-cleared error flags */
		FM24_I2C_HAL_DMADeInit();

		FM24_I2C_HAL_GPIOInit();
		FM24_HAL_ENABLE_DEV();
		I2C_Init(FM24_I2C, (I2C_InitTypeDef *)&FM24_I2C_InitData);

		FM24_I2C_HAL_DMAInit();
		FM24_I2C_HAL_ITInit();
		ds->State = FM24_STATE_READY;

		return FM24_NO_ERROR;
	}		
	return FM24_FAIL;
}


/**
  * @brief	Deinitialize the peripheral and all related clocks, GPIOs, DMA and NVIC 
  *			to their reset values.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL
  * @note	The Peripheral clock is disabled but the GPIO Ports clocks remains 
  *			enabled after this deinitialization. 
  */
uint32_t FM24_I2C_DeInit(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;
	register FM24Context_t *ds = &FM24_Conext;

	/* If FM24_State is not BUSY */
	if ((ds->State == FM24_STATE_READY)
	||	(ds->State == FM24_STATE_ERROR)
	||	(ds->State == FM24_STATE_DISABLED)
		)
	{
		FM24_I2C_HAL_GPIODeInit();
		FM24_HAL_DISABLE_DEV();
		I2C_RCC_RESET(FM24_I2C_CLK);	// Reset I2Cx device clock in order to avoid non-cleared error flags

		FM24_I2C_HAL_DMADeInit();		//	DMA Deinitialization : if DMA Programming model is selected

		// Deinitialize NVIC and interrupts used by I2C Device in
		NVIC_DisableIRQ(FM24_EV_IRQn);
		NVIC_DisableIRQ(FM24_ER_IRQn);
		NVIC_DisableIRQ(FM24_DMA_TX_IRQn);
		NVIC_DisableIRQ(FM24_DMA_RX_IRQn);

		ds->State	= FM24_STATE_DISABLED;
		ds->Error	= FM24_NO_ERROR;
		ds->Timeout	= FM24_I2C_TIMEOUT_DEFAULT;

		return FM24_NO_ERROR;
	}
	return FM24_FAIL; 
}

/**
  * @brief	Initialize the peripheral structure with default values according
	*			to the specified parameters in the FM24_I2CDevTypeDef structure.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval
  */
void FM24_I2C_StructInit(void)
{	 
	FM24_Conext.Buffer	= NULL;
	FM24_Conext.State	= FM24_STATE_DISABLED;
	FM24_Conext.Error	= FM24_NO_ERROR;
	FM24_Conext.Timeout	= FM24_I2C_TIMEOUT_DEFAULT;
}

/**
  * @brief	Allows to send a data or a buffer of data through the peripheral to 
	*			 a selected device in a selected location address.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval	NO_ERROR or FM24_FAIL.
  */
ERROR_t FM24_I2C_Write(const uint8_t* src, uint16_t address, uint32_t count)
{
	register I2C_TypeDef * i2c = FM24_I2C;
	register FM24Context_t *ds = &FM24_Conext;

	if ((ds->State & FM24_STATE_BUSY) != 0
	||	ds->State == FM24_STATE_READY_TX
	||	ds->State == FM24_STATE_READY_RX
	||	ds->State == FM24_STATE_DISABLED	// If FM24_State is FM24_STATE_DISABLED (device is not initialized) Exit Write function
	||	ds->State == FM24_STATE_ERROR		// If FM24_State is FM24_STATE_ERROR (Error occurred )
		)
		return FM24_FAILURE;

	ds->State = FM24_STATE_BUSY;
	ds->Count	= count;
	ds->Buffer	= src;
	ds->Address	= address;

	// Wait until Busy flag is reset
	__FM24_I2C_TIMEOUT( !(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY );

	FM24_HAL_START();
	ds->State = FM24_STATE_READY_TX;
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_SB;
	FM24_HAL_ENABLE_EVTIT();	// Enable EVENT Interrupts

	return NO_ERROR;
}

/**
  * @brief	Allows to receive a data or a buffer of data through the peripheral 
	*				 from a selected device in a selected location address.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL. 
  */
ERROR_t FM24_I2C_Read(uint8_t* pBuffer, uint16_t ReadAddr, uint32_t NumByteToRead)
{
	register I2C_TypeDef * i2c		= FM24_I2C;
	register FM24Context_t * ds	= &FM24_Conext;

	if ((ds->State & FM24_STATE_BUSY) != 0
	||	ds->State == FM24_STATE_READY_TX
	||	ds->State == FM24_STATE_READY_RX
	||	ds->State == FM24_STATE_DISABLED	/* If FM24_State is FM24_STATE_DISABLED (device is not initialized) Exit Read function */
	||	ds->State == FM24_STATE_ERROR	/* If FM24_State is FM24_STATE_ERROR (Error occurred ) */
		)
		return FM24_FAILURE;
	
   	ds->Count = NumByteToRead;
	ds->Buffer = pBuffer;

	/* Update FM24_State to FM24_STATE_BUSY */
	ds->State = FM24_STATE_BUSY;
 
	// Wait until Busy flag is reset
	__FM24_I2C_TIMEOUT( !(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY );

	FM24_HAL_START();
	__FM24_I2C_TIMEOUT( FM24_HAL_GET_SB(), FM24_I2C_TIMEOUT_SB );

	FM24_HAL_SEND(FM24_ADDRESS_WRITE);
	__FM24_I2C_TIMEOUT( FM24_HAL_GET_ADDR(), FM24_I2C_TIMEOUT_ADDR );
	FM24_HAL_CLEAR_ADDR();	// Clear ADDR flag: (Read SR1 followed by read of SR2), SR1 read operation is already done

	__FM24_I2C_TIMEOUT(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);
	FM24_HAL_SEND( ReadAddr >> 8);

	__FM24_I2C_TIMEOUT(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);
	FM24_HAL_SEND( ReadAddr );

	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_TXE;
	__FM24_I2C_TIMEOUT(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);

	// Update FM24_State to FM24_STATE_READY_RX
	ds->State = FM24_STATE_READY_RX;
	FM24_HAL_START();

	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_SB;
	FM24_HAL_ENABLE_EVTIT();	// Enable EVENT Interrupts

	return NO_ERROR;
}

/**
  * @brief	Handles Master Start condition (SB) interrupt event.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval	NO_ERROR or FM24_FAIL. 
  */
void I2C_MASTER_START_Handle(register I2C_TypeDef * i2c)
{
	register FM24Context_t * ds	= &FM24_Conext;

	if (ds->State == FM24_STATE_READY_RX)
	{
		FM24_HAL_SEND(FM24_ADDRESS_READ);
		ds->State = FM24_STATE_BUSY_RX;
	}
	else
	{
		FM24_HAL_SEND(FM24_ADDRESS_WRITE);
		ds->State = FM24_STATE_BUSY_TX;
	}
	ds->Timeout = FM24_I2C_TIMEOUT_MIN + FM24_I2C_TIMEOUT_ADDR;
}

/**
  * @brief	This function Configure I2C DMA and Interrupts before starting transfer phase.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @param	Direction : Transfer direction.
  * @retval	NO_ERROR or FM24_FAIL. 
  */
void FM24_I2C_Enable_DMA_IT (void)
{
	register I2C_TypeDef * i2c = FM24_I2C;
	register FM24Context_t * ds	= &FM24_Conext;

	FM24_HAL_DISABLE_EVTIT();
	FM24_HAL_ENABLE_DMAREQ();
	
	FM24_DMA_InitStructure.DMA_MemoryBaseAddr	= (uint32_t)(ds->Buffer);
	FM24_DMA_InitStructure.DMA_BufferSize		= ds->Count;

	if (ds->State == FM24_STATE_BUSY_TX)
	{
		/* Configure TX DMA Channels */
		FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
		DMA_Init(FM24_DMA_TX_Channel, &FM24_DMA_InitStructure);

		FM24_HAL_DISABLE_LAST(); 	/* Disable DMA automatic NACK generation */
		FM24_HAL_ENABLE_DMATX();
	}		
	else if (ds->State == FM24_STATE_BUSY_RX)
	{
		/* Configure RX DMA Channels */
		FM24_DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
		DMA_Init(FM24_DMA_RX_Channel, &FM24_DMA_InitStructure);

		FM24_HAL_ENABLE_LAST();		/* Enable DMA automatic NACK generation */
		FM24_HAL_ENABLE_DMARX();
	}
	else
		return;
}

/**
  * @brief	Handles Master address matched (ADDR) interrupt event. 
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval NO_ERROR or FM24_FAIL. 
  */
void I2C_MASTER_ADDR_Handle(register I2C_TypeDef * i2c)
{
	register FM24Context_t * ds	= &FM24_Conext;

	if (ds->State == FM24_STATE_BUSY_TX)
		ds->Timeout = FM24_I2C_TIMEOUT_MIN + ds->Count;	// Set 1ms timeout for each data transfer in case of DMA Tx mode
	else if (ds->State == FM24_STATE_BUSY_RX)
		ds->Timeout = FM24_I2C_TIMEOUT_MIN + ds->Count;	// Set 1ms timeout for each data transfer in case of DMA Rx mode
	else
		ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;			// Reinitialize Timeout Value to default (no timeout initiated)

	FM24_HAL_CLEAR_ADDR();

	if (ds->State == FM24_STATE_BUSY_TX)
	{
		FM24_HAL_SEND((ds->Address & 0xFF00) >> 8);
		__FM24_I2C_TIMEOUT_VOID(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);

		FM24_HAL_SEND(ds->Address & 0x00FF);
		__FM24_I2C_TIMEOUT_VOID(FM24_HAL_GET_TXE(), FM24_I2C_TIMEOUT_TXE);
	}

	FM24_I2C_Enable_DMA_IT();
}

/**
  * @brief  This function handles I2C1 interrupt request.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void FM24_EV_IRQHandler(void)
{  
	register I2C_TypeDef * i2c = FM24_I2C;
	register __IO uint16_t I2CFlagStatus;

	/* Read I2C1 Status Registers 1 and 2 */
	I2CFlagStatus = FM24_HAL_GET_EVENT();

	if ((I2CFlagStatus & I2C_SR1_SB ) != 0)
		I2C_MASTER_START_Handle(i2c);
	else if ((I2CFlagStatus & I2C_SR1_ADDR ) != 0)
		I2C_MASTER_ADDR_Handle(i2c);
}

/**
  * @brief
  * @param
  * @retval
  */
void FM24_ReInit(void)
{
	FM24_I2C_DeInit();
	FM24_I2C_StructInit();
	FM24_I2C_Init();
}

/**
  * @brief  This function handles I2C1 Errors interrupt.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void FM24_ER_IRQHandler(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;
	register FM24Context_t * ds	= &FM24_Conext;

	ds->Error = (FM24Error_t)FM24_HAL_GET_ERROR();
	ds->State = FM24_STATE_ERROR;
	FM24_HAL_CLEAR_ERROR();

	/* If Bus error occurred ---------------------------------------------------*/
	if ((ds->Error & FM24_I2C_ERR_BERR) != 0
	||	(ds->Error & FM24_I2C_ERR_ARLO) != 0
		)
	{
		FM24_HAL_SWRST();
	}

	ds->Timeout  = FM24_I2C_TIMEOUT_DEFAULT;
	FM24_ReInit();
}

/**
  * @brief  This function handles I2C1 TX DMA interrupt request.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void FM24_I2Cx_DMA_TX_IRQHandler(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;	
	register FM24Context_t * ds	= &FM24_Conext;

	/* Reinitialize Timeout Value to default (no timeout initiated) */
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;

	if ((FM24_HAL_GET_DMATX_TCIT()) != 0)
	{
		ds->Count = 0;

		FM24_HAL_DISABLE_DMAREQ();
		
		/* Wait until BTF flag is set */ 
		__FM24_I2C_TIMEOUT_VOID(FM24_HAL_GET_BTF(), FM24_I2C_TIMEOUT_BTF);
		FM24_HAL_STOP();
		__FM24_I2C_TIMEOUT_VOID(!(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY);

		FM24_HAL_DISABLE_DMATX();		/* Disable DMA Channel */
		FM24_HAL_DISABLE_EVTIT();	 	/* Disable EVENT Interrupt */

		ds->State = FM24_STATE_READY; 	/* Update FM24_State to FM24_STATE_READY */
	}
	else if ((FM24_HAL_GET_DMATX_TEIT()) != 0)
	{
		ds->State = FM24_STATE_ERROR;
		ds->Count = FM24_HAL_DMATX_GET_CNDT();
	}

	FM24_HAL_CLEAR_DMATX_IT();
}

/**
  * @brief  This function handles I2C1 RX DMA interrupt request.
  * @param  None. 
  * @retval NO_ERROR. 
  */
void FM24_I2Cx_DMA_RX_IRQHandler(void)
{
	register I2C_TypeDef * i2c = FM24_I2C;	
	register FM24Context_t * ds	= &FM24_Conext;

	/* Reinitialize Timeout Value to default (no timeout initiated) */
	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT; 
	
	if ((FM24_HAL_GET_DMARX_TCIT()) != 0)
	{	 
		ds->Count = 0;

		FM24_HAL_STOP();				/* Generate Stop Condition */
		FM24_HAL_DISABLE_DMAREQ();	/* Disable DMA Request and Channel */
		
		__FM24_I2C_TIMEOUT_VOID(!(FM24_HAL_GET_BUSY()), FM24_I2C_TIMEOUT_BUSY);
		
		FM24_HAL_DISABLE_DMARX();		/* Disable DMA Channel */
		FM24_HAL_DISABLE_EVTIT();		/* Disable EVENT Interrupt */
		FM24_HAL_DISABLE_LAST();		/* Disable DMA automatic NACK generation */

		ds->State = FM24_STATE_READY;
	}
	else if ((FM24_HAL_GET_DMARX_TEIT()) != 0)
	{	 
		ds->State = FM24_STATE_ERROR; 	/* Update FM24_State to FM24_STATE_ERROR */
		ds->Count = FM24_HAL_DMARX_GET_CNDT();
	}
	
	FM24_HAL_CLEAR_DMARX_IT();
}

/**
  * @brief	This function Manages I2C Timeouts when waiting for specific events.
  * @param	None
  * @retval	NO_ERROR or FM24_FAIL. 
  */
void FM24_I2C_TIMEOUT_Manager(void)
{
	register FM24Context_t * ds	= &FM24_Conext;

	/* If Timeout occurred  */
	if (ds->Timeout == FM24_I2C_TIMEOUT_DETECTED)
	{
		ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;		/* Reinitialize Timeout Value */
		ds->State = FM24_STATE_ERROR;				/* update FM24_State to FM24_STATE_ERROR */

		/* In case of Device Error Timeout_Callback should not be called */
		if (ds->Error == FM24_NO_ERROR)
		{
			ds->Error = FM24_I2C_ERR_TIMEOUT;
			FM24_ReInit();
		}
	}
	/* If Timeout is triggered (Timeout != FM24_I2C_TIMEOUT_DEFAULT)*/
	else if (ds->Timeout != FM24_I2C_TIMEOUT_DEFAULT)
	{
		ds->Timeout--;	/* Decrement the timeout value */
	}
}

/**
  * @brief	This function Manages I2C Timeouts when Timeout occurred.
  * @param	pDevInitStruct: Pointer to the peripheral configuration structure.
  * @retval	NO_ERROR or FM24_FAIL. 
  */
ERROR_t FM24_I2C_Timeout (void)
{
	register FM24Context_t * ds	= &FM24_Conext;

	ds->Timeout = FM24_I2C_TIMEOUT_DEFAULT;
	ds->State = FM24_STATE_ERROR;
	ds->Error = FM24_I2C_ERR_TIMEOUT;

	FM24_ReInit();
	return FM24_TIMEOUT;
}

/**
  * @brief
  * @param
  * @retval
  */
FM24State_t FM24_I2C_GetState(void)
{
	return FM24_Conext.State;
}

/**
  * @brief
  * @param
  * @retval
  */
FM24Error_t FM24_I2C_GetError(void)
{
	return FM24_Conext.Error;
}

#endif
