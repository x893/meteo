#include <time.h>

#include "common.h"
#include "board.h"
#include "rtc.h"

/**
  * @brief
  * @param
  * @retval
  */
ERROR_t LogError2(const char * msg, ERROR_t error)
{
	if (error != NO_ERROR)
	{
		LogAddFormat((uint32_t)error, (uint32_t)RtcGetDatetime(), "%s ERROR %02X", msg, error);
		ShowError(error);
	}
	return error;
}

#ifdef __GNUC__
	extern uint32_t g_pfnVectors;
#else
	extern uint32_t __Vectors;
#endif
/**
  * @brief	Show error code via LED blinks and restart
  * @param	error code
  * @retval	None
  */
void ShowFatalError(ERROR_t error)
{
	__disable_irq();
#ifdef __GNUC__
	__set_MSP(g_pfnVectors);
#else
	__set_MSP(__Vectors);
#endif
	ShowError(error);
	DelayMilli(5000);
	NVIC_SystemReset();
}

/**
  * @brief
  * @param
  * @retval
  */
void Fatal_Handler(void)
{
	// Initialize user application's Stack Pointer
	__disable_irq();
#ifdef __GNUC__
	__set_MSP(g_pfnVectors);
#else
	__set_MSP(__Vectors);
#endif
	ShowFatalError(SYS_FATAL_ERR);
}

/**
  * @brief	Make LED blinks with pre-delay
  * @param	number of blnks
  * @retval	None
  */
void ShowErrorCode(LEDS_t leds, int flashes)
{
	DelayMilli(1000);
	while (flashes != 0)
	{
		LEDS_On(leds);
		DelayMilli(250);
		LEDS_Off(leds);
		DelayMilli(500);
		--flashes;
	}
}

/**
  * @brief	Show error code via LED blinks
  * @param	error code
  * @retval	None
  */
void ShowErrorEx(ERROR_t error)
{
	LEDS_On(LED_2R);
	DelayMilli(1000);
	LEDS_Off(LED_2R);
	ShowErrorCode(LED_2G, (error & 0xF0) >> 4);
	ShowErrorCode(LED_2R, (error & 0x0F)     );
	DelayMilli(1000);
}
