#include "rtc.h"

/**
  * @brief
  * @param
  * @retval
  */
ERROR_t RtcInit(void)
{
	PWR_BackupAccessCmd(ENABLE);

	if (BKP_ReadBackupRegister(BKP_DR1) != 0x1963)
	{
		register int timeout;

		BKP_DeInit();

		RCC_LSEConfig(RCC_LSE_ON);
		timeout = 100;
		while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
		{
			if (timeout-- == 0)
			{
				RCC_LSEConfig(RCC_LSE_OFF);
				PWR_DeInit();
				return SYS_RTC_FAIL;
			}
#ifdef BOOTLOADER
			DelayMilli(10);
#else
			vTaskDelay(10);
#endif
		}
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

		RCC_RTCCLKCmd(ENABLE);
		RTC_WaitForSynchro();
		RTC_WaitForLastTask();

//		RTC_ITConfig(RTC_IT_SEC, ENABLE);
//		RTC_WaitForLastTask();

		// Set RTC prescaler: set RTC period to 1sec
		RTC_SetPrescaler(32767);	// RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1)
		RTC_WaitForLastTask();

		RTC_SetCounter(0);

		BKP_WriteBackupRegister(BKP_DR1, 0x1963);
	}
	else
	{
		RTC_WaitForSynchro();
//		RTC_ITConfig(RTC_IT_SEC, ENABLE);
//		RTC_WaitForLastTask();
	}

	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_EnableIRQ(RTC_IRQn);

	return NO_ERROR;
}

/**
  * @brief	RTC 1second interrupt handler
  * @param
  * @retval
  */
void RTC_IRQHandler(void)
{
	if (RTC_GetITStatus(RTC_IT_SEC) != RESET)
	{
		RTC_ClearITPendingBit(RTC_IT_SEC);	// Clear the RTC Second interrupt
		RTC_WaitForLastTask();				// Wait until last write operation on RTC registers has finished
	}
	if (RTC_GetITStatus(RTC_IT_ALR) != RESET)
	{
		RTC_ClearITPendingBit(RTC_IT_ALR);	// Clear the RTC Alarm interrupt
		RTC_WaitForLastTask();				// Wait until last write operation on RTC registers has finished
	}
}

/**
  * @brief	Get RTC counter
  * @param
  * @retval
  */
time_t RtcGetDatetime(void)
{
	uint32_t time = 0;

	if (RCC_GetFlagStatus(RCC_FLAG_LSERDY) != RESET)
	{
		NVIC_DisableIRQ(RTC_IRQn);
		time = RTC_GetCounter();
		NVIC_EnableIRQ(RTC_IRQn);
	}
	return time;
}

/**
  * @brief	Set RTC date and time
  * @param
  * @retval
  */
ERROR_t RtcSetDatetime(time_t time)
{
	if (RCC_GetFlagStatus(RCC_FLAG_LSERDY) != RESET)
	{
		NVIC_DisableIRQ(RTC_IRQn);
		RTC_WaitForLastTask();
		RTC_SetCounter(time);
		NVIC_EnableIRQ(RTC_IRQn);
	}
	return NO_ERROR;
}
