#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "common.h"
#include "at45.h"
#include "version.h"
#include "MemoryLayout.h"


#ifndef BOOTLOADER
	#include "FreeRTOS.h"
	#include "task.h"
	#include "queue.h"
	#include "semphr.h"
	#define LOG_MUTEX_TIMEOUT	( 5000 / portTICK_PERIOD_MS )
#endif

// Size of LogRecord_s must be 2**n boundary (now 64 bytes)
//	64 - 5 * sizeof (uint32_t) = 44
#define LOG_MESSAGE_SIZE	44
typedef struct LogRecord_s
{
	Version_t	Version;	// 3
	uint32_t	LogType;	// 4
	uint32_t	Time;		// 5
	char		Message[LOG_MESSAGE_SIZE];
} LogRecord_t;

typedef struct LogContext_s
{
	LogRecord_t LogRecord;
	uint32_t	AT45Address;
	uint32_t	Version;
#ifndef BOOTLOADER
	SemaphoreHandle_t xMutex;
#endif
} LogContext_t;

LogContext_t LogContext;

#ifndef BOOTLOADER
/**
  * @brief	Initialize logging system under OS
  * @param
  * @retval	NO_ERROR or error
  */
ERROR_t LogOsInit(void)
{
	register LogContext_t *lc = &LogContext;
	lc->xMutex = xSemaphoreCreateMutex();
	if (lc->xMutex == NULL)
		return SYS_OS_FAIL;
	return NO_ERROR;
}
#endif

/**
  * @brief	Initialize logging system
  * @param
  * @retval	NO_ERROR or error
  */
ERROR_t LogInit(void)
{
	ERROR_t result = AT45_FAILURE;
	register LogContext_t *lc = &LogContext;
	register uint32_t address = AT45_LOG_BASE;
	register uint32_t version = 0;

	result = NO_ERROR;
	lc->AT45Address = address;
	lc->Version = version;
	while (address < AT45_LOG_END)
	{
		if (NO_ERROR != (result = AT45Read((uint8_t *)&lc->LogRecord, address, sizeof(LogRecord_t))))
			break;

		if (lc->LogRecord.Version.SizeX != sizeof(LogRecord_t)
		||	NO_ERROR != VersionCheck(&lc->LogRecord.Version)
		||	version > lc->LogRecord.Version.Version
			)
			break;
		version = lc->LogRecord.Version.Version;
		address += sizeof(LogRecord_t);
	}

	if (result == NO_ERROR)
	{
		if (address >= AT45_LOG_END)
			address = AT45_LOG_BASE;
		lc->AT45Address = address;
		lc->Version = version;
	}
	else
		lc->AT45Address = 0;
	return result;
}

/**
  * @brief
  * @param
  * @retval	NO_ERROR or error
  */
uint32_t LogGetPrevious(uint32_t address, LogRecord_t * log)
{
	if (address == 0)
		address = LogContext.AT45Address;
	if (address != 0)
	{
		if (address == AT45_LOG_BASE)
			address = AT45_LOG_END;
		address -= sizeof(LogRecord_t);
		if (NO_ERROR != AT45Read((uint8_t *)log, address, sizeof(LogRecord_t))
		||	log->Version.SizeX != sizeof(LogRecord_t)
		||	NO_ERROR != VersionCheck(&log->Version)
			)
		{
			address = 0;
		}
	}
	return address;
}

void LogDisplayLast(void)
{
	static LogRecord_t log;
	register uint16_t count = 10;
	register uint32_t address = 0;
	while (count != 0)
	{
		count--;
		address = LogGetPrevious(address, &log);
		if (address == 0)
			break;

		PRINTF("LOG: %02X %s %s", log.LogType, log.Message, ctime(&log.Time));
	}
}

/**
  * @brief
  * @param
  * @retval	NO_ERROR or error
  */
ERROR_t LogAdd(uint32_t logType, uint32_t time, const char * message)
{
	register LogContext_t *lc = &LogContext;
	register ERROR_t result = SYS_OS_FAIL;
	if (message != NULL)
		strncpy((char *)&lc->LogRecord.Message, message, LOG_MESSAGE_SIZE-1);
	DEBUG(LogContext.LogRecord.Message);
	DEBUG("\n");
	if (lc->AT45Address != 0)
	{
		lc->LogRecord.LogType = logType;
		lc->LogRecord.Time = time;
		VersionFill(&lc->LogRecord, sizeof(LogRecord_t), lc->Version + 1, 0);
		if (NO_ERROR == (result = AT45Write((uint8_t *)&lc->LogRecord, lc->AT45Address, sizeof(LogRecord_t))))
		{
			lc->Version++;
			lc->AT45Address += sizeof(LogRecord_t);
			if (lc->AT45Address >= AT45_LOG_END)
				lc->AT45Address = AT45_LOG_BASE;
		}
	}
	else
		result = AT45_FAILURE;
	return result;
}

/**
  * @brief
  * @param
  * @retval	NO_ERROR or error
  */
ERROR_t LogAddFormat(uint32_t logType, uint32_t time, const char * fmt, ...)
{
	va_list arg_list;
	ERROR_t result = SYS_OS_FAIL;

#ifndef BOOTLOADER
	register LogContext_t *lc = &LogContext;
	if (lc->xMutex == NULL || xSemaphoreTake(lc->xMutex, LOG_MUTEX_TIMEOUT ) == pdPASS)
	{
#endif
		va_start(arg_list, fmt);
		vsnprintf((char *)&LogContext.LogRecord.Message, LOG_MESSAGE_SIZE-1, fmt, arg_list);
		va_end(arg_list);
		result = LogAdd(logType, time, NULL);

#ifndef BOOTLOADER
		if (lc->xMutex != NULL)
			xSemaphoreGive(lc->xMutex);
	}
#endif
	return result;
}
