#ifndef __DS18B20_H__
#define __DS18B20_H__

#include "common.h"
#include "board.h"

void DS18B20_Init(void);
ERROR_t DS18B20_Reset(void);
ERROR_t DS18B20_Send(const uint8_t *command, uint8_t cLen, uint8_t *data, uint8_t dLen, uint8_t readStart);
float DS18B20_Celcius(void);
uint8_t DS18B20_Scan(uint8_t *buf, uint8_t num);
ERROR_t DS18B20_GetID(void);
ERROR_t DS18B20_Start(void);
ERROR_t DS18B20_Read(void);
uint8_t * DS18B20_Data(void);

#endif /* __DS18B20_H__ */
