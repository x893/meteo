#ifndef __RTC_H__
#define __RTC_H__

#include "common.h"
#include "board.h"
#include <time.h>

ERROR_t RtcInit(void);
time_t  RtcGetDatetime(void);
ERROR_t RtcSetDatetime(time_t time);

#endif
