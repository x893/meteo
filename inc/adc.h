#ifndef __ADC_H__
#define __ADC_H__

#include "common.h"

ERROR_t AdcInit(void);
void AdcDisplay(void);

void AdcStart(void);
void AdcCalibrate(void);
void AdcAverage(void);

uint16_t AdcUbat(void);
uint16_t AdcIbat_P(void);
uint16_t AdcIbat_M(void);
uint16_t AdcIin(void);
uint16_t AdcUin(void);

#endif
