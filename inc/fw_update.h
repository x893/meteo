#ifndef __FW_UPDATE_H__
#define __FW_UPDATE_H__

#include <stdint.h>

#ifdef STM32F103VG
	#define FLASH_SIZE		((uint32_t)(256 * 1024))
	#define FW_OFFSET		0x8000
	#define FW_BASE			(FLASH_BASE + FW_OFFSET)
	#define FW_END			(FLASH_BASE + FLASH_SIZE)
	#define SRAM_END		SRAM_BASE + 0x18000

	// See real address in map file for __FWNewFlag variable
	#define	FW_NEW_FLAG		((uint32_t *)(FW_BASE + 0x134))

#else
	#error "Unknown MCU type"
#endif

typedef struct VECTORS_s
{
	uint32_t	Stack;
	uint32_t	Entry;
} VECTORS_t;

ERROR_t FWCheckUpdate(void);
ERROR_t FWCheckCopy(void);

#endif
