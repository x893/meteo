#ifndef __CHARGER_H__
#define __CHARGER_H__

#include "common.h"
#include "board.h"

void ChargerOff(void);
void ChargerOn(void);
void ChargerDeInit(void);
void ChargerInit(uint16_t freqKHz);
void ChargerDuty(uint8_t duty);
void ChargerPrecharge(void);
void ChargerCheck(void);

#endif
