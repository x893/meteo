#ifndef __FM24_H
#define __FM24_H

#include "common.h"
#include "board.h"
   
#define FM24_I2C_TIMEOUT_SB             1000
#define FM24_I2C_TIMEOUT_ADDR           1000
#define FM24_I2C_TIMEOUT_TXE            1000

#define FM24_I2C_TIMEOUT_RXNE           2000
#define FM24_I2C_TIMEOUT_BTF            4
#define FM24_I2C_TIMEOUT_BUSY           5000

#define FM24_I2C_TIMEOUT_DEFAULT        ((uint32_t)0xFFFFFFFF)
#define FM24_I2C_TIMEOUT_MIN            ((uint32_t)0x00000001)
#define FM24_I2C_TIMEOUT_DETECTED       ((uint32_t)0x00000000)
 
#define FM24_I2C_DR					((uint32_t)&FM24_I2C->DR)

#ifdef BOOTLOADER
	typedef enum
	{
		FM24_NO_ERROR			= 0x0000,
		FM24_I2C_ERR_TIMEOUT	= 0x00FF,
		FM24_I2C_ERR_BERR		= 0x0100,
		FM24_I2C_ERR_ARLO		= 0x0200,
		FM24_I2C_ERR_AF			= 0x0400,
		FM24_I2C_ERR_OVR		= 0x0800,
		FM24_FAIL				= 0x0001,
	} FM24Error_t;

	typedef enum
	{
		FM24_STATE_DISABLED		= 0x00,
		FM24_STATE_READY		= 0x01,
		FM24_STATE_BUSY			= 0x02,
		FM24_STATE_READY_TX		= (FM24_STATE_READY | 0x04),
		FM24_STATE_READY_RX		= (FM24_STATE_READY | 0x08),
		FM24_STATE_BUSY_TX		= (FM24_STATE_BUSY | 0x04),
		FM24_STATE_BUSY_RX		= (FM24_STATE_BUSY | 0x08),
		FM24_STATE_ERROR		= 0x10,
	} FM24State_t;

	typedef struct
	{
		__IO	uint32_t	Timeout;
		const	uint8_t *	Buffer;
		__IO	uint16_t	Count;
				uint16_t	Address;
		__IO	FM24Error_t		Error;
		__IO	FM24State_t		State;
	} FM24_Context_t;

	extern FM24_Context_t FM24_Conext;
	/*
	void	 FM24_I2C_StructInit	(void);

	uint32_t FM24_I2C_Init			(void);
	uint32_t FM24_I2C_DeInit		(void);
	ERROR_t FM24_I2C_IsDeviceReady	(void);
	ERROR_t FM24_I2C_Write			( const uint8_t* pBuffer, uint16_t WriteAddr, uint32_t NumByteToWrite );
	ERROR_t FM24_I2C_Read  		( uint8_t* pBuffer, uint16_t ReadAddr, uint32_t NumByteToRead );

	FM24State_t FM24_I2C_GetState	(void);
	FM24Error_t FM24_I2C_GetError	(void);
	*/
#endif

ERROR_t FM24Init(void);
ERROR_t FM24Read(uint16_t address, void * dst, uint16_t bytes);
ERROR_t FM24Write(uint16_t address, void * src, uint16_t bytes);

#endif /*__FM24_H */
