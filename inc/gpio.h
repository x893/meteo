#ifndef __GPIO_H__
#define __GPIO_H__

#include "stm32f10x.h"

#define PIN_ANALOG			(GPIO_Mode_AIN)
#define PIN_INPUT			(GPIO_Mode_IN_FLOATING)
#define PIN_INPUT_PU		(GPIO_Mode_IPU)
#define PIN_INPUT_PD		(GPIO_Mode_IPD)

#define PIN_OUT_PP_2		(GPIO_Mode_Out_PP | GPIO_Speed_2MHz )
#define PIN_OUT_PP_10		(GPIO_Mode_Out_PP | GPIO_Speed_10MHz)
#define PIN_OUT_PP_50		(GPIO_Mode_Out_PP | GPIO_Speed_50MHz)

#define PIN_OUT_OD_2		(GPIO_Mode_Out_OD | GPIO_Speed_2MHz )
#define PIN_OUT_OD_10		(GPIO_Mode_Out_OD | GPIO_Speed_10MHz)
#define PIN_OUT_OD_50		(GPIO_Mode_Out_OD | GPIO_Speed_50MHz)

#define PIN_AF_PP_2			(GPIO_Mode_AF_PP | GPIO_Speed_2MHz )
#define PIN_AF_PP_10		(GPIO_Mode_AF_PP | GPIO_Speed_10MHz)
#define PIN_AF_PP_50		(GPIO_Mode_AF_PP | GPIO_Speed_50MHz)

#define PIN_AF_OD_2			(GPIO_Mode_AF_OD | GPIO_Speed_2MHz )
#define PIN_AF_OD_10		(GPIO_Mode_AF_OD | GPIO_Speed_10MHz)
#define PIN_AF_OD_50		(GPIO_Mode_AF_OD | GPIO_Speed_50MHz)

#define PIN_PORT_BASE(pin)	((uint32_t)(GPIOA_BASE + (GPIOB_BASE - GPIOA_BASE) * ((pin & 0xF0) >> 4)))
#define PIN_PORT(pin)		((GPIO_TypeDef *)PIN_PORT_BASE(pin))

#define IO_PORT_CR(pin)		((__IO uint32_t *)(PIN_PORT_BASE(pin) + ((pin & 0x08) >> 1)))
#define IO_PORT_IDR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x08))
#define IO_PORT_ODR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x0C))
#define IO_PORT_BSRR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x10))
#define IO_PORT_BRR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x14))

#define PIN_MODE(mode,pin)			((uint32_t)((mode) & 0x0F) << ((pin & 0x07) << 2))
#define PIN_MODE_SET(mode,pin)		*IO_PORT_CR(pin) = (*IO_PORT_CR(pin) & ~PIN_MODE(0x0F, pin)) | PIN_MODE(mode,pin)
#define PIN_MODE_GET(pin)			(*IO_PORT_CR(pin) & PIN_MODE(0x0F, pin))
#define PIN_MODE_RESTORE(mode,pin)	*IO_PORT_CR(pin) = (*IO_PORT_CR(pin) & ~PIN_MODE(0x0F, pin)) | mode

#define PIN_MASK(pin)		((uint32_t)0x01 << ((pin & 0x0F) +  0))
#define PIN_MASK_RESET(pin)	((uint32_t)0x01 << ((pin & 0x0F) + 16))

#define PIN_BITBAND_BASE(port,pos)	(PERIPH_BB_BASE | ((((uint32_t )port) - PERIPH_BASE) << 5) | ((pos & 0x0F) << 2))
#define PIN_BITBAND(port,pos)		((__IO uint32_t *)PIN_BITBAND_BASE(port,pos))

#define PIN_SET(pin)			*IO_PORT_BSRR( pin ) = PIN_MASK( pin )
#define PIN_RESET(pin)			*IO_PORT_BRR ( pin ) = PIN_MASK( pin )
#define PIN_READ(pin)			*PIN_BITBAND(IO_PORT_IDR(pin), pin)
#define PIN_READ_OUT(pin)		*PIN_BITBAND(IO_PORT_ODR(pin), pin)

#define PINS_OUT(port, bits)	*IO_PORT_ODR( port )  = bits
#define PINS_SET(port, bits)	*IO_PORT_BSRR( port ) = bits
#define PINS_RESET(port, bits)	*IO_PORT_BRR( port )  = bits
#define PINS_READ_OUT(port)		*IO_PORT_ODR( port )

#define NVIC_IRQ_ENABLE(IRQn)	NVIC->ISER[(IRQn >> 5)]
#define NVIC_IRQ_DISABLE(IRQn)	NVIC->ICER[(IRQn >> 5)]
#define NVIC_IRQ_CLEAR(IRQn)	NVIC->ICPR[(IRQn >> 5)]
#define NVIC_IRQ_MASK(IRQn)		(1 << (IRQn & 0x1F))

enum PortName_e
{
	PA		= 0x00,
	PA_LOW	= 0x07,
	PA_HIGH	= 0x0F,
	PB		= 0x10,
	PB_LOW	= 0x17,
	PB_HIGH	= 0x1F,
	PC		= 0x20,
	PC_LOW	= 0x27,
	PC_HIGH	= 0x2F,
	PD		= 0x30,
	PD_LOW	= 0x37,
	PD_HIGH	= 0x3F,
	PE		= 0x40,
	PE_LOW	= 0x47,
	PE_HIGH	= 0x4F,
};
enum PinName_e
{
	PA0 = 0x00,PA1,PA2,PA3,PA4,PA5,PA6,PA7,PA8,PA9,PA10,PA11,PA12,PA13,PA14,PA15,
	PB0 = 0x10,PB1,PB2,PB3,PB4,PB5,PB6,PB7,PB8,PB9,PB10,PB11,PB12,PB13,PB14,PB15,
	PC0 = 0x20,PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,PC11,PC12,PC13,PC14,PC15,
	PD0 = 0x30,PD1,PD2,PD3,PD4,PD5,PD6,PD7,PD8,PD9,PD10,PD11,PD12,PD13,PD14,PD15,
	PE0 = 0x40,PE1,PE2,PE3,PE4,PE5,PE6,PE7,PE8,PE9,PE10,PE11,PE12,PE13,PE14,PE15,
	NC = 0xFF
};

#endif
