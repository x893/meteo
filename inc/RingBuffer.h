#ifndef __RINGBUFFER_H__
#define __RINGBUFFER_H__

#include <stdint.h>
#include <stdbool.h>

typedef struct RingBuffer_s {
	uint8_t * Head;
	uint8_t * Tail;
	uint8_t * Start;
	uint8_t * End;
} RingBuffer_t;

bool RBGet(RingBuffer_t *rb, uint8_t * c);
bool RBPut(RingBuffer_t *rb, uint8_t c);
void RBInit(RingBuffer_t *rb, uint8_t * buffer, int size);

#endif
