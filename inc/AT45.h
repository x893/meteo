#ifndef __AT45_H__
#define __AT45_H__

#include <stdint.h>
#include <stdbool.h>
#include "common.h"

#define AT45_PAGE_SIZE			512
#define AT45_PAGE_SIZE_DEFAULT	528
#define AT45_PAGE_SIZE_BINARY	512
#define AT45_PAGES				4096UL
#define AT45_BLOCKS				512
#define AT45_SECTORS			16

ERROR_t AT45Init (void);
ERROR_t AT45Write(uint8_t *src, uint32_t address, uint16_t bytes);
ERROR_t AT45Read(uint8_t *dst, uint32_t address, uint16_t bytes);

#endif
