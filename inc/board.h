#ifndef __BOARD_H__
#define __BOARD_H__

#include "stm32f10x.h"

#define SET_BAUD_RATE		(uint32_t *)0x01

#define PIN_ANALOG			(GPIO_Mode_AIN)
#define PIN_INPUT			(GPIO_Mode_IN_FLOATING)
#define PIN_INPUT_PU		(GPIO_Mode_IPU)
#define PIN_INPUT_PD		(GPIO_Mode_IPD)

#define PIN_OUT_PP_2		(GPIO_Mode_Out_PP | GPIO_Speed_2MHz )
#define PIN_OUT_PP_10		(GPIO_Mode_Out_PP | GPIO_Speed_10MHz)
#define PIN_OUT_PP_50		(GPIO_Mode_Out_PP | GPIO_Speed_50MHz)

#define PIN_OUT_OD_2		(GPIO_Mode_Out_OD | GPIO_Speed_2MHz )
#define PIN_OUT_OD_10		(GPIO_Mode_Out_OD | GPIO_Speed_10MHz)
#define PIN_OUT_OD_50		(GPIO_Mode_Out_OD | GPIO_Speed_50MHz)

#define PIN_AF_PP_2			(GPIO_Mode_AF_PP | GPIO_Speed_2MHz )
#define PIN_AF_PP_10		(GPIO_Mode_AF_PP | GPIO_Speed_10MHz)
#define PIN_AF_PP_50		(GPIO_Mode_AF_PP | GPIO_Speed_50MHz)

#define PIN_AF_OD_2			(GPIO_Mode_AF_OD | GPIO_Speed_2MHz )
#define PIN_AF_OD_10		(GPIO_Mode_AF_OD | GPIO_Speed_10MHz)
#define PIN_AF_OD_50		(GPIO_Mode_AF_OD | GPIO_Speed_50MHz)

#define PIN_PORT_BASE(pin)	((uint32_t)(GPIOA_BASE + (GPIOB_BASE - GPIOA_BASE) * ((pin & 0xF0) >> 4)))
#define PIN_PORT(pin)		((GPIO_TypeDef *)PIN_PORT_BASE(pin))
//	Port Control Register - CRL(0) or CRH(4)
#define IO_PORT_CR(pin)		((__IO uint32_t *)(PIN_PORT_BASE(pin) + ((pin & 0x08) >> 1)))
#define IO_PORT_IDR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x08))
#define IO_PORT_ODR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x0C))
#define IO_PORT_BSRR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x10))
#define IO_PORT_BRR(pin)	((__IO uint32_t *)(PIN_PORT_BASE(pin) + 0x14))

#define PIN_MODE(mode,pin)			((uint32_t)((mode) & 0x0F) << ((pin & 0x07) << 2))
#define PIN_MODE_SET(mode,pin)		*IO_PORT_CR(pin) = (*IO_PORT_CR(pin) & ~PIN_MODE(0x0F, pin)) | PIN_MODE(mode,pin)
#define PIN_MODE_GET(pin)			(*IO_PORT_CR(pin) & PIN_MODE(0x0F, pin))
#define PIN_MODE_RESTORE(mode,pin)	*IO_PORT_CR(pin) = (*IO_PORT_CR(pin) & ~PIN_MODE(0x0F, pin)) | mode

#define PIN_MASK(pin)		((uint32_t)0x01 << ((pin & 0x0F) +  0))
#define PIN_MASK_RESET(pin)	((uint32_t)0x01 << ((pin & 0x0F) + 16))

#define PIN_BITBAND_BASE(port,pos)	(PERIPH_BB_BASE | ((((uint32_t )port) - PERIPH_BASE) << 5) | ((pos & 0x0F) << 2))
#define PIN_BITBAND(port,pos)		((__IO uint32_t *)PIN_BITBAND_BASE(port,pos))

#define PIN_SET(pin)			*IO_PORT_BSRR( pin ) = PIN_MASK( pin )
#define PIN_RESET(pin)			*IO_PORT_BRR ( pin ) = PIN_MASK( pin )
#define PIN_READ(pin)			*PIN_BITBAND(IO_PORT_IDR(pin), pin)
#define PIN_READ_OUT(pin)		*PIN_BITBAND(IO_PORT_ODR(pin), pin)

#define PINS_OUT(port, bits)	*IO_PORT_ODR( port )  = bits
#define PINS_SET(port, bits)	*IO_PORT_BSRR( port ) = bits
#define PINS_RESET(port, bits)	*IO_PORT_BRR( port )  = bits
#define PINS_READ_OUT(port)		*IO_PORT_ODR( port )

#define NVIC_IRQ_ENABLE(IRQn)	NVIC->ISER[(IRQn >> 5)]
#define NVIC_IRQ_DISABLE(IRQn)	NVIC->ICER[(IRQn >> 5)]
#define NVIC_IRQ_CLEAR(IRQn)	NVIC->ICPR[(IRQn >> 5)]
#define NVIC_IRQ_MASK(IRQn)		(1 << (IRQn & 0x1F))

enum PortName_e
{
	PA		= 0x00,
	PA_LOW	= 0x07,
	PA_HIGH	= 0x0F,
	PB		= 0x10,
	PB_LOW	= 0x17,
	PB_HIGH	= 0x1F,
	PC		= 0x20,
	PC_LOW	= 0x27,
	PC_HIGH	= 0x2F,
	PD		= 0x30,
	PD_LOW	= 0x37,
	PD_HIGH	= 0x3F,
	PE		= 0x40,
	PE_LOW	= 0x47,
	PE_HIGH	= 0x4F,
};
enum PinName_e
{
	PA0 = 0x00,PA1,PA2,PA3,PA4,PA5,PA6,PA7,PA8,PA9,PA10,PA11,PA12,PA13,PA14,PA15,
	PB0 = 0x10,PB1,PB2,PB3,PB4,PB5,PB6,PB7,PB8,PB9,PB10,PB11,PB12,PB13,PB14,PB15,
	PC0 = 0x20,PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,PC11,PC12,PC13,PC14,PC15,
	PD0 = 0x30,PD1,PD2,PD3,PD4,PD5,PD6,PD7,PD8,PD9,PD10,PD11,PD12,PD13,PD14,PD15,
	PE0 = 0x40,PE1,PE2,PE3,PE4,PE5,PE6,PE7,PE8,PE9,PE10,PE11,PE12,PE13,PE14,PE15,
	NC = 0xFF
};

/**
  *	RCC
  */
#define RCC_AHB_CLOCKS	(	0	\
							|	RCC_AHBPeriph_CRC		\
							|	RCC_AHBPeriph_DMA1		\
						)
#define RCC_APB1_CLOCKS	(	0	\
							|	AT45_SPI_CLK			\
							|	FM24_I2C_CLK			\
							|	DS18B20_CLK				\
							|	SDI_RS_CLK				\
							|	RCC_APB1Periph_PWR		\
							|	RCC_APB1Periph_BKP		\
						)
#define RCC_APB2_CLOCKS	(	0	\
							|	RCC_APB2Periph_AFIO		\
							|	RCC_APB2Periph_GPIOA	\
							|	RCC_APB2Periph_GPIOB	\
							|	RCC_APB2Periph_GPIOC	\
							|	RCC_APB2Periph_GPIOD	\
							|	RCC_APB2Periph_GPIOE	\
							|	RCC_APB2Periph_USART1	\
							|	RCC_APB2Periph_ADC1		\
							|	CHARGER_TIMER_CLK		\
						)
						// RCC_PCLK2_Div2
#define RCC_INIT		{	&(RCC->CFGR),		~RCC_CFGR_ADCPRE,	RCC_PCLK2_Div4	}, \
						{	&(RCC->AHBENR),		~RCC_AHB_CLOCKS,	RCC_AHB_CLOCKS	}, \
						{	&(RCC->APB1ENR),	~RCC_APB1_CLOCKS,	RCC_APB1_CLOCKS	}, \
						{	&(RCC->APB2ENR),	~RCC_APB2_CLOCKS,	RCC_APB2_CLOCKS	},
/**
  *	GPIO A
  */
#define ADC_U_IN		PA1
#define ADC_I_IN		PA2
#define SDI_DIR			PA3	/* =0 receive, =1 transmite (UART3) */
#define SPI1_NSS		PA4	/* not used, set to 0 */
#define SPI1_SCK		PA5	/* not used, set to 0 */
#define SPI1_MISO		PA6	/* not used, set to 0 */
#define SPI1_MOSI		PA7	/* not used, set to 0 */
#define MODEM_DTR		PA8
#define DEBUG_TX		PA9
#define DEBUG_RX		PA10
#define SYS_JTMS_SWDIO	PA13
#define SYS_JTCK_SWCLK	PA14
#define SYS_JTDI		PA15

#define PA_BSRR_PRE		(	0	\
							|	PIN_MASK_RESET( SDI_DIR		) \
							|	PIN_MASK_RESET( MODEM_DTR	) \
							|	PIN_MASK(		DEBUG_RX	) \
						)
#define PA_CRL_MASK		(uint32_t)0xFFFFFFFF	/* PA7-PA0 */
#define PA_CRL_INIT		(	0	\
							|	PIN_MODE( PIN_ANALOG,		PA0				) \
							|	PIN_MODE( PIN_ANALOG,		ADC_U_IN		) \
							|	PIN_MODE( PIN_ANALOG,		ADC_I_IN		) \
							|	PIN_MODE( PIN_OUT_PP_2,		SDI_DIR			) \
							|	PIN_MODE( PIN_OUT_PP_2,		SPI1_NSS		) \
							|	PIN_MODE( PIN_OUT_PP_2,		SPI1_SCK		) \
							|	PIN_MODE( PIN_OUT_PP_2,		SPI1_MISO		) \
							|	PIN_MODE( PIN_OUT_PP_2,		SPI1_MOSI		) \
						)
#define PA_CRH_MASK		(uint32_t)0x000FFFFF	/* PA13-PA8 */
#define PA_CRH_INIT		(	0	\
							|	PIN_MODE( PIN_ANALOG,		MODEM_DTR		) \
							|	PIN_MODE( PIN_AF_PP_50,		DEBUG_TX		) \
							|	PIN_MODE( PIN_INPUT_PU,		DEBUG_RX		) \
							|	PIN_MODE( PIN_ANALOG,		PA11			) \
							|	PIN_MODE( PIN_ANALOG,		PA12			) \
						)
#define PA_INIT			{	&GPIOA->BSRR,		0,			PA_BSRR_PRE		}, \
						{	&GPIOA->CRL,	~PA_CRL_MASK,	PA_CRL_INIT		}, \
						{	&GPIOA->CRH,	~PA_CRH_MASK,	PA_CRH_INIT		},
/**
  *	GPIO B
  */
#define ADC_UBAT		PB0
#define ADC_IBAT_P		PB1
#define SYS_JTDO_SWO	PB3
#define SYS_JTRST		PB4
#define MODEM_READY		PB5
#define FM24_SCL		PB6		/* FM24LC64 */
#define FM24_SDA		PB7
#define MODEM_ONOFF		PB8
#define MODEM_RESET		PB9
#define SDI_RS_TX		PB10
#define SDI_RS_RX		PB11
#define AT45_NSS		PB12	/* DataFlash AT45DB161 */
#define AT45_SCK		PB13
#define AT45_MISO		PB14
#define AT45_MOSI		PB15

#define PB_BSRR_PRE		(	0	\
							|	PIN_MASK_RESET( SDI_RS_TX		) \
							|	PIN_MASK_RESET(	SDI_RS_RX		) \
							|	PIN_MASK_RESET( MODEM_ONOFF		) \
							|	PIN_MASK_RESET( MODEM_RESET		) \
							|	PIN_MASK_RESET(	MODEM_READY		) \
							|	PIN_MASK(		AT45_MISO		) \
							|	PIN_MASK(		AT45_NSS		) \
							|	PIN_MASK(		FM24_SCL		) \
							|	PIN_MASK(		FM24_SDA		) \
						)
#define PB_CRL_MASK		(uint32_t)0xFFF00FFF	/* PB7-PB5, PB1, PB0, */
#define PB_CRL_INIT		(	0	\
							|	PIN_MODE( PIN_ANALOG,		ADC_UBAT		) \
							|	PIN_MODE( PIN_ANALOG,		ADC_IBAT_P		) \
							|	PIN_MODE( PIN_ANALOG,		PB2				) \
							/* PB3 */	\
							/* PB4 */	\
							|	PIN_MODE( PIN_INPUT_PD,		MODEM_READY		) \
							|	(	FM24_PIN_I2C							) \
						)
#define PB_CRH_MASK		(uint32_t)0xFFFFFFFF	/* PB15-PB8 */
#define PB_CRH_INIT		(	0 \
							|	PIN_MODE( PIN_OUT_PP_2,		MODEM_ONOFF		) \
							|	PIN_MODE( PIN_OUT_PP_2,		MODEM_RESET		) \
							|	PIN_MODE( PIN_AF_PP_50,		SDI_RS_TX		) \
							|	PIN_MODE( PIN_INPUT,		SDI_RS_RX		) \
							|	PIN_MODE( PIN_OUT_PP_50,	AT45_NSS		) \
							|	PIN_MODE( PIN_AF_PP_50,		AT45_SCK		) \
							|	PIN_MODE( PIN_INPUT_PU,		AT45_MISO		) \
							|	PIN_MODE( PIN_AF_PP_50,		AT45_MOSI		) \
						)
#define PB_INIT			{	&GPIOB->BSRR,		0,			PB_BSRR_PRE		}, \
						{	&GPIOB->CRL,	~PB_CRL_MASK,	PB_CRL_INIT		}, \
						{	&GPIOB->CRH,	~PB_CRH_MASK,	PB_CRH_INIT		},
						
#define FM24_PIN_OUT	(	0	\
							|	PIN_MODE( PIN_OUT_OD_50,	FM24_SCL		) \
							|	PIN_MODE( PIN_OUT_OD_50,	FM24_SDA		) \
						)

#define FM24_PIN_I2C	(	0	\
							|	PIN_MODE( PIN_AF_OD_50,		FM24_SCL		) \
							|	PIN_MODE( PIN_AF_OD_50,		FM24_SDA		) \
						)
#define FM24_PIN_DEINIT	(	0	\
							|	PIN_MODE( PIN_ANALOG,		FM24_SCL		) \
							|	PIN_MODE( PIN_ANALOG,		FM24_SDA		) \
						)
#define FM24_PIN_MASK	(PIN_MODE(0xF, FM24_SCL) | PIN_MODE(0xF, FM24_SDA))
#define FM24_PIN_I2C_INIT	{	&GPIOB->CRL,	~FM24_PIN_MASK,	FM24_PIN_I2C	},
#define FM24_PIN_OUT_INIT	{	&GPIOB->CRL,	~FM24_PIN_MASK,	FM24_PIN_OUT	}, \
							{	&RCC->APB1RSTR,	~FM24_I2C_CLK,	FM24_I2C_CLK	}, \
							{	&RCC->APB1RSTR,	~FM24_I2C_CLK,	0				},

/**
  *	GPIO C
  */
#define ADC_IBAT_M		PC4
#define MODEM_DCD		PC7
#define MODEM_RI		PC8
#define MODEM_DSR		PC9
#define GPS_TX			PC10
#define GPS_RX			PC11
#define DS18B20_TX		PC12	/* Connect to TEMPSENSOR_RX */

#define PC_BSRR_PRE		(	0	\
							|	PIN_MASK( MODEM_DCD		) \
							|	PIN_MASK( MODEM_RI		) \
							|	PIN_MASK( MODEM_DSR		) \
							|	PIN_MASK( GPS_RX		) \
							|	PIN_MASK( DS18B20_TX	) \
						)
#define PC_CRL_MASK		(uint32_t)0xFFFFFFFF	/* PC7-PC0 */
#define PC_CRL_INIT		(	0	\
							|	PIN_MODE( PIN_ANALOG,		PC0				) \
							|	PIN_MODE( PIN_ANALOG,		PC1				) \
							|	PIN_MODE( PIN_ANALOG,		PC2				) \
							|	PIN_MODE( PIN_ANALOG,		PC3				) \
							|	PIN_MODE( PIN_ANALOG,		ADC_IBAT_M		) \
							|	PIN_MODE( PIN_ANALOG,		PC5				) \
							|	PIN_MODE( PIN_ANALOG,		PC6				) \
							|	PIN_MODE( PIN_ANALOG,		MODEM_DCD		) \
						)
#define PC_CRH_MASK		(uint32_t)0x00FFFFFF	/* PC13-PC8 */
#define PC_CRH_INIT		(	0	\
							|	PIN_MODE( PIN_ANALOG,		MODEM_RI		) \
							|	PIN_MODE( PIN_ANALOG,		MODEM_DSR		) \
							|	PIN_MODE( PIN_ANALOG,		GPS_TX			) \
							|	PIN_MODE( PIN_ANALOG,		GPS_RX			) \
							|	PIN_MODE( PIN_INPUT,		DS18B20_TX		) \
							|	PIN_MODE( PIN_ANALOG,		PC13			) \
						)
#define PC_INIT			{	&GPIOC->BSRR,		0,			PC_BSRR_PRE		}, \
						{	&GPIOC->CRL,	~PC_CRL_MASK,	PC_CRL_INIT		}, \
						{	&GPIOC->CRH,	~PC_CRH_MASK,	PC_CRH_INIT		},

/**
  *	GPIO D
  */
#define DS18B20_RX		PD2
#define MODEM_CTS		PD3
#define MODEM_RTS		PD4
#define MODEM_TX		PD5
#define MODEM_RX		PD6
#define MODEM_PWR		PD7
#define RS_RX			PD8
#define RS_TX			PD9
#define IC_1			PD14
#define IC_2			PD15

#define PD_BSRR_PRE		(	0	\
							|	PIN_MASK_RESET(	RS_TX			) 	\
							|	PIN_MASK_RESET(	MODEM_RX		) 	\
							|	PIN_MASK_RESET(	MODEM_RTS		) 	\
							|	PIN_MASK(		MODEM_CTS		) 	\
							|	PIN_MASK(		MODEM_PWR		)	\
							|	PIN_MASK(		DS18B20_RX		)	\
							|	PIN_MASK(		RS_RX			) 	\
						)
#define PD_CRL_MASK		(uint32_t)0xFFFFFFFF	/* PD7-PD0 */
#define PD_CRL_INIT		(	0	\
							|	PIN_MODE( PIN_ANALOG,		PD0				) \
							|	PIN_MODE( PIN_ANALOG,		PD1				) \
							|	PIN_MODE( PIN_INPUT,		DS18B20_RX		) \
							|	PIN_MODE( PIN_ANALOG,		MODEM_CTS		) \
							|	PIN_MODE( PIN_ANALOG,		MODEM_RTS		) \
							|	PIN_MODE( PIN_ANALOG,		MODEM_TX		) \
							|	PIN_MODE( PIN_ANALOG,		MODEM_RX		) \
							|	PIN_MODE( PIN_OUT_OD_2,		MODEM_PWR		) \
						)
#define PD_CRH_MASK		(uint32_t)0xFFFFFFFF	/* PD15-PD8 */
#define PD_CRH_INIT		(	0	\
							|	PIN_MODE( PIN_OUT_PP_2,		RS_RX			) \
							|	PIN_MODE( PIN_OUT_PP_2,		RS_TX			) \
							|	PIN_MODE( PIN_ANALOG,		PD10			) \
							|	PIN_MODE( PIN_ANALOG,		PD11			) \
							|	PIN_MODE( PIN_ANALOG,		PD12			) \
							|	PIN_MODE( PIN_ANALOG,		PD13			) \
							|	PIN_MODE( PIN_ANALOG,		IC_1			) \
							|	PIN_MODE( PIN_ANALOG,		IC_2			) \
						)
#define PD_INIT			{	&GPIOD->BSRR,		0,			PD_BSRR_PRE		}, \
						{	&GPIOD->CRL,	~PD_CRL_MASK,	PD_CRL_INIT		}, \
						{	&GPIOD->CRH,	~PD_CRH_MASK,	PD_CRH_INIT		},
/**
  *	GPIO E
  */
#define LEDS_PORT		PE_LOW
#define LEDS_PIN_RESET	(	0	\
							|	PIN_MASK_RESET(LED2_G)	\
							|	PIN_MASK_RESET(LED1_R)	\
							|	PIN_MASK_RESET(LED1_G)	\
							|	PIN_MASK_RESET(LED2_R)	\
						)
#define LED2_G			PE1
#define LED1_R			PE2
#define LED1_G			PE3
#define CALIBRATE_RQ	PE4
#define LED2_R			PE5
#define BALAST			PE7
#define HEATER			PE8
#define CHARGER_OUT		PE9
#define DC_SDI			PE10
#define DC_AUX2			PE11
#define DC_AUX1			PE12
#define AC_AUX2			PE13
#define AC_AUX1			PE14
#define GPS_PWR			PE15

#define PE_BSRR_PRE		(	0	\
							|	PIN_MASK(		LED2_G			) \
							|	PIN_MASK(		LED1_R			) \
							|	PIN_MASK(		LED1_G			) \
							|	PIN_MASK(		CALIBRATE_RQ	) \
							|	PIN_MASK(		LED2_R			) \
							|	PIN_MASK_RESET( BALAST			) \
							|	PIN_MASK_RESET( HEATER			) \
							|	PIN_MASK_RESET( CHARGER_OUT		) \
							|	PIN_MASK_RESET( DC_SDI			) \
							|	PIN_MASK_RESET( DC_AUX2			) \
							|	PIN_MASK_RESET( DC_AUX1			) \
							|	PIN_MASK_RESET( AC_AUX2			) \
							|	PIN_MASK_RESET( AC_AUX1			) \
							|	PIN_MASK_RESET( GPS_PWR			) \
						)
#define PE_CRL_MASK		(uint32_t)0xFFFFFFFF	/* PE7-PE0 */
#define PE_CRL_INIT		(	0	\
							|	PIN_MODE( PIN_ANALOG,		PE0			) \
							|	PIN_MODE( PIN_OUT_PP_2,		LED2_G		) \
							|	PIN_MODE( PIN_OUT_PP_2,		LED1_R		) \
							|	PIN_MODE( PIN_OUT_PP_2,		LED1_G		) \
							|	PIN_MODE( PIN_INPUT_PU,		CALIBRATE_RQ) \
							|	PIN_MODE( PIN_OUT_PP_2,		LED2_R		) \
							|	PIN_MODE( PIN_ANALOG,		PE6			) \
							|	PIN_MODE( PIN_OUT_PP_2,		BALAST		) \
						)
#define PE_CRH_MASK		(uint32_t)0xFFFFFFFF	/* PE15-PE8 */
#define PE_CRH_INIT		(	0	\
							|	PIN_MODE( PIN_INPUT,		HEATER		) \
							|	PIN_MODE( PIN_OUT_PP_2,		CHARGER_OUT	) \
							|	PIN_MODE( PIN_OUT_PP_2,		DC_SDI		) \
							|	PIN_MODE( PIN_OUT_PP_2,		DC_AUX2		) \
							|	PIN_MODE( PIN_OUT_PP_2,		DC_AUX1		) \
							|	PIN_MODE( PIN_OUT_PP_2,		AC_AUX2		) \
							|	PIN_MODE( PIN_OUT_PP_2,		AC_AUX1		) \
							|	PIN_MODE( PIN_OUT_PP_2,		GPS_PWR		) \
						)
#define PE_INIT			{ &GPIOE->BSRR,	0,				PE_BSRR_PRE	}, \
						{ &GPIOE->CRL,	~PE_CRL_MASK,	PE_CRL_INIT	}, \
						{ &GPIOE->CRH,	~PE_CRH_MASK,	PE_CRH_INIT	},

#define CHARGER_TIM			TIM1
#define CHARGER_PIN_OUT()	PIN_MODE_SET(PIN_OUT_PP_2, CHARGER_OUT)
#define CHARGER_PIN_TIMER()	PIN_MODE_SET(PIN_AF_PP_2, CHARGER_OUT)
#define CHARGER_TIMER_CLK	RCC_APB2Periph_TIM1
#define CHARGER_RESET()		RCC_APB2PeriphResetCmd(CHARGER_TIMER_CLK, ENABLE);	\
							RCC_APB2PeriphResetCmd(CHARGER_TIMER_CLK, DISABLE)

/**
  *	Interfaces
  */
#define USART_CR1_MODE_Mask		(USART_CR1_RE | USART_CR1_TE | USART_CR1_M | USART_CR1_PCE | USART_CR1_PS)
#define USART_CR1_UE_IE_Mask	(USART_CR1_UE | USART_CR1_RXNEIE | USART_CR1_TXEIE)
#define USART_CR3_CLEAR_Mask	(USART_CR3_RTSE | USART_CR3_CTSE)

#define DEBUG_UART			USART1
#define DEBUG_UART_BASE		USART1_BASE
#define DEBUG_IRQn			USART1_IRQn
#define DEBUG_IRQHandler	USART1_IRQHandler

#ifdef BOOTLOADER
	#define DEBUG_TX_INT_ENABLE()
	#define DEBUG_TX_INT_DISABLE()

	#define DEBUG_INIT		{	&RCC->APB2ENR,		~RCC_APB2ENR_USART1EN,		RCC_APB2ENR_USART1EN			}, \
							{	&RCC->APB2RSTR,		~RCC_APB2RSTR_USART1RST,	RCC_APB2RSTR_USART1RST			}, \
							{	&RCC->APB2RSTR,		~RCC_APB2RSTR_USART1RST,	0								}, \
							{	SET_BAUD_RATE,		DEBUG_UART_BASE,			115200UL						},
	#define DEBUG_UART_INIT	{	&DEBUG_UART->CR2,	~USART_CR2_STOP,			USART_StopBits_1				}, \
							{	&DEBUG_UART->CR1,	~USART_CR1_MODE_Mask,		0	\
																			|	USART_WordLength_8b		\
																			|	USART_Parity_No			\
																			|	USART_Mode_Rx			\
																			|	USART_Mode_Tx					}, \
							{	&(DEBUG_UART->CR3),	~USART_CR3_CLEAR_Mask,		USART_HardwareFlowControl_None	}, \
							{	&(DEBUG_UART->CR1),	~USART_CR1_UE_IE_Mask,		USART_CR1_UE					},
#else
	#define DEBUG_TX_INT_ENABLE()	*PIN_BITBAND( &DEBUG_UART->CR1, 7) = 1
	#define DEBUG_TX_INT_DISABLE()	*PIN_BITBAND( &DEBUG_UART->CR1, 7) = 0

	#define DEBUG_INIT		{	&NVIC_IRQ_CLEAR(DEBUG_IRQn),		0,			NVIC_IRQ_MASK(DEBUG_IRQn)		}, \
							{	&NVIC_IRQ_ENABLE(DEBUG_IRQn),		0,			NVIC_IRQ_MASK(DEBUG_IRQn)		},
	#define DEBUG_UART_INIT	{	&(DEBUG_UART->CR1),	~USART_CR1_UE_IE_Mask,		USART_CR1_UE | USART_CR1_RXNEIE	},
#endif

/*
MODEM_DTR	67	PA8		->	44	~CT108/DTR	IN

MODEM_READY	91	PB5		<-	7	WISMO_READY	OUT
MODEM_ONOFF		PB8		->
MODEM_RESET		PB9		->

MODEM_DCD	64	PC7		<-	43	~CT109/DCD	OUT
MODEM_RI	65	PC8		<-	45	~CT125/RI	OUT
MODEM_DSR	66	PC9		<-	42	~CT107/DSR	OUT

MODEM_CTS	84	PD3		<-	41	~CT106/CTS	OUT
MODEM_RTS	85	PD4		->	39	~CT105/RTS	IN
MODEM_TX	86	PD5		->	38	~CT103/TXD	IN
MODEM_RX	87	PD6		<-	40	~CT104/RXD	OUT

MODEM_PWR		PD7		->
*/
#define MODEM_UART			USART2
#define MODEM_UART_BASE		USART2_BASE
#define MODEM_IRQn			USART2_IRQn
#define MODEM_IRQHandle		USART2_IRQHandler

#define MODEM_TX_INT_ENABLE()	*PIN_BITBAND( &MODEM_UART->CR1, 7) = 1
#define MODEM_TX_INT_DISABLE()	*PIN_BITBAND( &MODEM_UART->CR1, 7) = 0

#define MODEM_UART_INIT		{	&(MODEM_UART->CR2),	~(USART_CR2_STOP),		USART_StopBits_1					},	\
							{	&(MODEM_UART->CR1),	~USART_CR1_MODE_Mask,	0						\
																		|	USART_WordLength_8b		\
																		|	USART_Parity_No			\
																		|	USART_Mode_Rx			\
																		|	USART_Mode_Tx						},	\
							{	&(MODEM_UART->CR3),	~USART_CR3_CLEAR_Mask,	USART_HardwareFlowControl_RTS_CTS	}, \
							{	&(MODEM_UART->CR1),	~USART_CR1_UE_IE_Mask,	USART_CR1_UE | USART_CR1_RXNEIE		},

#define MODEM_ONOFF_LOW()	*IO_PORT_BSRR( MODEM_ONOFF ) = PIN_MASK( MODEM_ONOFF )
#define MODEM_ONOFF_HIGH()	*IO_PORT_BRR ( MODEM_ONOFF ) = PIN_MASK( MODEM_ONOFF )

#define MODEM_PWR_ON()		PIN_RESET( MODEM_PWR )
#define MODEM_PWR_OFF()		PIN_SET( MODEM_PWR )

#define MODEM_INIT			{	&(RCC->APB1ENR),	~RCC_APB1ENR_USART2EN,		RCC_APB1ENR_USART2EN	}, \
							{	&(RCC->APB1RSTR),	~RCC_APB1RSTR_USART2RST,	RCC_APB1RSTR_USART2RST	}, \
							{	&(RCC->APB1RSTR),	~RCC_APB1RSTR_USART2RST,	0						}, \
							{	SET_BAUD_RATE,		MODEM_UART_BASE,			115200UL				}, \
							{	IO_PORT_CR(PD_LOW), 0xF0000FFF,	0	\
															|	PIN_MODE( PIN_AF_PP_50,	MODEM_TX	)	\
															|	PIN_MODE( PIN_INPUT,	MODEM_RX	)	\
															|	PIN_MODE( PIN_AF_PP_50,	MODEM_RTS	)	\
															|	PIN_MODE( PIN_INPUT,	MODEM_CTS	)	}, \
							{	IO_PORT_CR(PC_LOW), 0x0FFFFFFF,	PIN_MODE( PIN_INPUT,	MODEM_DCD		)}, \
							{	IO_PORT_CR(PC_HIGH),0xFFFFFF00,	0	\
															|	PIN_MODE( PIN_INPUT,	MODEM_RI	)	\
															|	PIN_MODE( PIN_INPUT,	MODEM_DSR	)	}, \
							{	IO_PORT_CR(PA_HIGH),0xFFFFFFF0,	PIN_MODE( PIN_OUT_OD_2,	MODEM_DTR	)	}, \
							{	&NVIC_IRQ_CLEAR(MODEM_IRQn),	0,	NVIC_IRQ_MASK(MODEM_IRQn)	}, \
							{	&NVIC_IRQ_ENABLE(MODEM_IRQn),		0,	NVIC_IRQ_MASK(MODEM_IRQn)	},

#define MODEM_DEINIT		{	&NVIC_IRQ_DISABLE(MODEM_IRQn),	0,	NVIC_IRQ_MASK(MODEM_IRQn)	}, \
							{	IO_PORT_CR(PD_LOW), 0xF0000FFF,	0	\
															|	PIN_MODE( PIN_ANALOG,	MODEM_TX	)	\
															|	PIN_MODE( PIN_ANALOG,	MODEM_RX	)	\
															|	PIN_MODE( PIN_ANALOG,	MODEM_RTS	)	\
															|	PIN_MODE( PIN_ANALOG,	MODEM_CTS	)	}, \
							{	IO_PORT_CR(PC_LOW), 0x0FFFFFFF,	PIN_MODE( PIN_ANALOG,	MODEM_DCD	)	}, \
							{	IO_PORT_CR(PC_HIGH),0xFFFFFF00,	0	\
															|	PIN_MODE( PIN_ANALOG,	MODEM_RI	)	\
															|	PIN_MODE( PIN_ANALOG,	MODEM_DSR	)	}, \
							{	IO_PORT_CR(PA_HIGH),0xFFFFFFF0,	PIN_MODE( PIN_ANALOG,	MODEM_DTR	)	}, \
							{	&(RCC->APB1RSTR),	~RCC_APB1RSTR_USART2RST,	RCC_APB1RSTR_USART2RST	}, \
							{	&(RCC->APB1RSTR),	~RCC_APB1RSTR_USART2RST,	0						}, \
							{	&(RCC->APB1ENR),	~RCC_APB1ENR_USART2EN,		0						},

#define SDI_RS_UART			USART3
#define SDI_RS_UART_BASE	USART3_BASE
#define SDI_RS_CLK			RCC_APB1Periph_USART3
#define SDI_RS_IRQn			USART3_IRQn
#define SDI_RS_IRQHandle	USART3_IRQHandler
#define SDI_RS_INIT			{	&(RCC->APB1ENR),		~RCC_APB1ENR_USART3EN,	RCC_APB1ENR_USART3EN			}, \
							{	&(RCC->APB1RSTR),		~RCC_APB1RSTR_USART3RST,RCC_APB1RSTR_USART3RST			}, \
							{	&(RCC->APB1RSTR),		~RCC_APB1RSTR_USART3RST,0								}, \
							{	SET_BAUD_RATE,			SDI_RS_UART_BASE,		1200							}, \
							{	&NVIC_IRQ_CLEAR(SDI_RS_IRQn),	0,				NVIC_IRQ_MASK(SDI_RS_IRQn)		}, \
							{	&NVIC_IRQ_ENABLE(SDI_RS_IRQn),		0,				NVIC_IRQ_MASK(SDI_RS_IRQn)		},

#define SDI_UART_INIT		{	&(SDI_RS_UART->CR2),	~(USART_CR2_STOP),		USART_StopBits_1					}, \
							{	&(SDI_RS_UART->CR1),	~USART_CR1_MODE_Mask,	0							\
																				|	USART_WordLength_8b		\
																				|	USART_Parity_Even		\
																				|	USART_Mode_Rx			\
																				|	USART_Mode_Tx					}, \
							{	&(SDI_RS_UART->CR3),	~USART_CR3_CLEAR_Mask,	USART_HardwareFlowControl_None		}, \
							{	&(SDI_RS_UART->CR1),	~USART_CR1_UE_IE_Mask,	USART_CR1_UE						},

#define GPS_UART			UART4
#define GPS_UART_BASE		UART4_BASE
#define GPS_IRQn			UART4_IRQn
#define GPS_IRQHandle		UART4_IRQHandler
#define GPS_INIT			{	&(RCC->APB1ENR),		~RCC_APB1ENR_UART4EN,	RCC_APB1ENR_UART4EN				}, \
							{	&(RCC->APB1RSTR),		~RCC_APB1RSTR_UART4RST,	RCC_APB1RSTR_UART4RST			}, \
							{	&(RCC->APB1RSTR),		~RCC_APB1RSTR_UART4RST,	0								}, \
							{	SET_BAUD_RATE,			GPS_UART_BASE,			9600							}, \
							{	IO_PORT_ODR(GPS_PWR),	~PIN_MASK(GPS_PWR),		PIN_MASK(GPS_PWR)				}, \
							{	IO_PORT_CR(GPS_TX),		~PIN_MODE(0x0F, GPS_TX), PIN_MODE(PIN_AF_PP_50, GPS_TX) }, \
							{	IO_PORT_CR(GPS_RX),		~PIN_MODE(0x0F, GPS_RX), PIN_MODE(PIN_INPUT_PU, GPS_RX)	}, \
							{	&NVIC_IRQ_CLEAR(GPS_IRQn),	0,					NVIC_IRQ_MASK(GPS_IRQn)			}, \
							{	&NVIC_IRQ_ENABLE(GPS_IRQn),	0,					NVIC_IRQ_MASK(GPS_IRQn)			},
#define GPS_UART_INIT		{	&(GPS_UART->CR2),		~(USART_CR2_STOP),		USART_StopBits_1				}, \
							{	&(GPS_UART->CR1),		~USART_CR1_MODE_Mask,	0							\
																				|	USART_WordLength_8b		\
																				|	USART_Parity_No			\
																				|	USART_Mode_Rx			\
																				|	USART_Mode_Tx				}, \
							{	&(GPS_UART->CR3),	~USART_CR3_CLEAR_Mask,	USART_HardwareFlowControl_None		}, \
							{	&(GPS_UART->CR1),	~USART_CR1_UE_IE_Mask,		USART_CR1_UE | USART_CR1_RXNEIE	},

#define GPS_DEINIT			{	IO_PORT_CR(GPS_TX),		~PIN_MODE(0xF, GPS_TX),	PIN_MODE(PIN_ANALOG, GPS_TX)	}, \
							{	IO_PORT_CR(GPS_RX),		~PIN_MODE(0xF, GPS_RX),	PIN_MODE(PIN_ANALOG, GPS_RX)	}, \
							{	IO_PORT_ODR(GPS_PWR),	~PIN_MASK(GPS_PWR),		0								}, \
							{	&(RCC->APB1ENR),		~RCC_APB1ENR_UART4EN,	0								},

#define DS18B20_UART		UART5
#define DS18B20_UART_BASE	UART5_BASE
#define DS18B20_IRQn		UART5_IRQn
#define DS18B20_IRQHandler	UART5_IRQHandler
#define DS18B20_CLK			RCC_APB1Periph_UART5
#define DS18B20_INIT		{	&(RCC->APB1ENR),		~RCC_APB1ENR_UART5EN,	RCC_APB1ENR_UART5EN				}, \
							{	&(RCC->APB1RSTR),		~RCC_APB1RSTR_UART5RST,	RCC_APB1RSTR_UART5RST			}, \
							{	&(RCC->APB1RSTR),		~RCC_APB1RSTR_UART5RST,	0								}, \
							{	SET_BAUD_RATE,			DS18B20_UART_BASE,		115200							}, \
							{	&NVIC_IRQ_CLEAR(DS18B20_IRQn),	0,				NVIC_IRQ_MASK(DS18B20_IRQn)		}, \
							{	&NVIC_IRQ_ENABLE(DS18B20_IRQn),	0,				NVIC_IRQ_MASK(DS18B20_IRQn)		},

#define DS18B20_UART_INIT	{	&(DS18B20_UART->CR2),	~(USART_CR2_STOP),		USART_StopBits_1				}, \
							{	&(DS18B20_UART->CR1),	~USART_CR1_MODE_Mask,	0							\
																				|	USART_WordLength_8b		\
																				|	USART_Parity_No			\
																				|	USART_Mode_Rx			\
																				|	USART_Mode_Tx				}, \
							{	&(DS18B20_UART->CR3),	~USART_CR3_CLEAR_Mask,	USART_HardwareFlowControl_None	}, \
							{	&(DS18B20_UART->CR1),	~USART_CR1_UE_IE_Mask,	USART_CR1_UE					},

#define DS18B20_PIN_INIT()	do {	\
								PIN_MODE_SET(PIN_AF_OD_50, DS18B20_TX);	\
								PIN_MODE_SET(PIN_INPUT, DS18B20_RX);	\
							} while (0)
#define DS18B20_PIN_POWER()	do {	\
								PIN_MODE_SET(PIN_OUT_PP_2, DS18B20_TX);	\
								PIN_MODE_SET(PIN_OUT_PP_2, DS18B20_RX);	\
							} while (0)

#define AT45_SPI			SPI2
#define AT45_SPI_CLK		RCC_APB1Periph_SPI2
#define AT45_CS_HIGH()		PIN_SET(AT45_NSS)
#define AT45_CS_LOW()		PIN_RESET(AT45_NSS)

#define FM24_ADDRESS_READ	(0xA0 | 1)
#define FM24_ADDRESS_WRITE	(0xA0 | 0)

#define FM24_I2C			I2C1
#define FM24_EV_IRQn		I2C1_EV_IRQn
#define FM24_ER_IRQn		I2C1_ER_IRQn
#define FM24_EV_IRQHandler	I2C1_EV_IRQHandler
#define FM24_ER_IRQHandler	I2C1_ER_IRQHandler
#define FM24_I2C_CLK		RCC_APB1Periph_I2C1

#define FM24_DMA_TX_Channel     DMA1_Channel6
#define FM24_DMA_RX_Channel     DMA1_Channel7
#define FM24_DMA_TX_IRQn		DMA1_Channel6_IRQn
#define FM24_DMA_RX_IRQn		DMA1_Channel7_IRQn
#define FM24_DMA				DMA1
   
#define FM24_DMA_TX_TC_FLAG		DMA1_FLAG_TC6
#define FM24_DMA_TX_TE_FLAG		DMA1_FLAG_TE6
  
#define FM24_DMA_RX_TC_FLAG		DMA1_FLAG_TC7
#define FM24_DMA_RX_TE_FLAG		DMA1_FLAG_TE7


#define FM24_I2Cx_DMA_TX_IRQHandler	DMA1_Channel6_IRQHandler
#define FM24_I2Cx_DMA_RX_IRQHandler	DMA1_Channel7_IRQHandler
  
#define DBG_MASK			(	0	\
							|	DBGMCU_SLEEP				\
							|	DBGMCU_STOP					\
							|	DBGMCU_STANDBY				\
							|	DBGMCU_I2C1_SMBUS_TIMEOUT	\
							)
#define DBG_INIT			{	&DBGMCU->CR, ~DBG_MASK, DBG_MASK	},

#define ADC_PORT			ADC1
#define ADC_PORT_DR			((uint32_t)(ADC1_BASE + 0x4C))
#define ADC_DMA				DMA1
#define ADC_DMA_CH			DMA1_Channel1
#define ADC_DMA_IT_TC		DMA1_IT_TC1
#define ADC_DMA_FLAG_TC		DMA1_FLAG_TC1
#define ADC_DMA_IT_GL		DMA1_IT_GL1
#define ADC_DMA_IRQn		DMA1_Channel1_IRQn
#define ADC_DMA_IRQHandler	DMA1_Channel1_IRQHandler
#define ADC_SAMPLING		ADC_SampleTime_55Cycles5

#define ADC_U_IN_CH			ADC_Channel_1
#define ADC_I_IN_CH			ADC_Channel_2
#define ADC_UBAT_CH			ADC_Channel_8
#define ADC_IBAT_P_CH		ADC_Channel_9
#define ADC_IBAT_M_CH		ADC_Channel_14

typedef struct PinInit32_s
{
	__IO uint32_t * Address;
		uint32_t	Mask;
		uint32_t	Value;
} PinInit32_t;

typedef struct PinInit16_s
{
volatile uint16_t * Address;
		uint16_t	Mask;
		uint16_t	Value;
} PinInit16_t;

void PinInit32( register const PinInit32_t *ports );
void PinInit16( register const PinInit16_t *ports );

void UartSetBaudrate( uint32_t usartxbase, uint32_t baudrate );

#endif
