#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "common.h"

#define ADC_CALIBRATE_UIN	1400
#define ADC_CALIBRATE_UBAT	1400

#define DEF_ADC_UIN_MUL		1400
#define DEF_ADC_UIN_DIV		1634

#define DEF_ADC_IIN_MUL		2000
#define DEF_ADC_IIN_DIV		1021

#define DEF_ADC_UBAT_MUL	1400
#define DEF_ADC_UBAT_DIV	3213

#define DEF_ADC_IBAT_M_MUL	1000
#define DEF_ADC_IBAT_M_DIV	1021

#define DEF_ADC_IBAT_P_MUL	1000
#define DEF_ADC_IBAT_P_DIV	1021

#define DEF_PRECHARGE_ENTER		((uint16_t)(10.5 * 100))	//	Level in mV to enter precharge mode
#define DEF_PRECHARGE_EXIT		((uint16_t)(11.5 * 100))	//	Level in mV to exit precharge mode
#define DEF_CHARGER_MAX_CURRENT	((uint16_t) 1800)			// Maximum charge current in mA
#define DEF_ADC_PERIOD_TICKS	100							// Period for ADC conversion in ticks (ms)

// Timeout for deactivate GSM without tasks
#define GSM_TASK_TIMEOUT	(30000  / portTICK_PERIOD_MS)

#define GPS_TASK_TIMEOUT	(120000 / portTICK_PERIOD_MS)
#define GPS_SAMPLES_TOTAL	240
#define GPS_SAMPLES_VALID	20
#define GPS_TIMEZONE_OFFSET	(4 * 60 * 60)
#define GPS_DISTANCE_CHANGE	(100.0)

#define HTTP_SERVER_ADDRESS	"wind.openweathermap.org"
#define HTTP_SERVER_PORT	"8080"
#define HTTP_FW_INFO_ALL	"/stationfw/v1/fwinfoall.json"
#define HTTP_USER_AGENT		"EFarm HW1.0"
#define HTTP_CONNECTION		"close"

typedef struct CpuId_s
{
	uint8_t	U8[12];
} CpuId_t;

typedef struct DS18B20Id_s
{
	uint8_t	U8[8];
} DS18B20Id_t;

enum ParameterStatus_e
{
	PARAMS_STATUS_CHANGE	= 0x00000001,
	PARAMS_STATUS_CPUID		= 0x00000002,	// CPU ID Change
	PARAMS_STATUS_DS18B20	= 0x00000004,	// DS18B20 ID Change
};

typedef struct Parameters_s
{
	Version_t	Version;

	uint32_t	Status;

	CpuId_t		CpuId;
	DS18B20Id_t	DS18B20Id;
		double	Latitude;
		double	Longitude;

} Parameters_t;

typedef struct ConfigHost_s
{
	Version_t	Version;

	uint16_t	GsmSendPeriod;	// Send interval to host in seconds
	uint16_t	PreChargeEnter;
	uint16_t	PreChargeExit;
	uint16_t	ChargerMaxCurrent;
	uint16_t	AdcPeriodTicks;

} ConfigHost_t;

typedef struct ConfigLocal_s
{
	Version_t	Version;

	uint16_t	Adc_Uin_Mul;
	uint16_t	Adc_Uin_Div;

	uint16_t	Adc_Iin_Mul;
	uint16_t	Adc_Iin_Div;

	uint16_t	Adc_Ubat_Mul;
	uint16_t	Adc_Ubat_Div;

	uint16_t	Adc_Ibat_M_Mul;
	uint16_t	Adc_Ibat_M_Div;

	uint16_t	Adc_Ibat_P_Mul;
	uint16_t	Adc_Ibat_P_Div;

} ConfigLocal_t;

extern ConfigHost_t		ConfigHost;
extern ConfigLocal_t	ConfigLocal;
extern Parameters_t		Parameters;

ERROR_t ConfigInit(void);
ERROR_t ConfigSave(void);
bool CheckCpuId(void);
bool CheckDS18B20(uint8_t * id);
void ConfigChanged(void);

#endif
