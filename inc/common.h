#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define BL_VERSION			"1.0"
#define FW_VERSION			"1.0"
#define FW_VERSION_NUMERIC	1

#define STRING_SIZE(s)	(sizeof(s) - 1)

typedef union UU32_s
{
	uint32_t U32;
	uint8_t  U8[4];
} UU32_t;

typedef union UU16_s
{
	uint16_t U16;
	uint8_t  U8[2];
} UU16_t;

typedef enum ERROR_e
{
	NO_ERROR			= 0,
	BAD_CRC32			= 1,

	GSM_NO_GPRS			= 0x11,
	GSM_NOT_REGISTER	= 0x12,
	GSM_ERROR			= 0x13,
	GSM_SIM_FAIL		= 0x14,
	GSM_NOT_FOUND		= 0x15,
	GSM_TIMEOUT			= 0x16,
	GSM_BAD_RESPONSE	= 0x17,
	GSM_TASK_FAIL		= 0x18,

	GPS_NOT_FOUND		= 0x21,
	GPS_TASK_FAIL		= 0x22,
	GPS_NO_DATA			= 0x23,
	GPS_NO_FIX			= 0x24,

	DS18B20_ERROR		= 0x28,
	DS18B20_NO_DEVICE	= 0x29,

	AT45_FAILURE		= 0x31,
	AT45_NOT_READY		= 0x32,
	AT45_PARAM_ERROR	= 0x33,
	
	SYS_ADC_ERROR		= 0x38,
	SYS_ADC_NOTREADY	= 0x39,
	SYS_ADC_FAILED		= 0x3A,
	SYS_CALIBRATE_UBAT	= 0x3B,
	SYS_RTC_FAIL		= 0x3C,

	FM24_NOT_FOUND		= 0x41,
	FM24_TIMEOUT		= 0x42,
	FM24_FAILURE		= 0x43,
	FM24_I2C_ERROR		= 0x44,

	CHARGE_BAD_FREQ		= 0x48,

	SYS_FW_INVALID		= 0x51,
	SYS_OS_FAIL			= 0x52,
	SYS_FATAL_ERR		= 0x53,
	SYS_FW_ERROR		= 0x54,
	SYS_OS_TASK_ADD		= 0x55,
	SYS_CFG_CHANGE		= 0x56,
	SYS_VERSION_FAIL	= 0x58,

	SDI_EX_ERROR		= 0x58,
	SDI_RESP_ERROR		= 0x59,
	SDI_SEND			= 0x5E,
	SDI_RECEIVE			= 0x5F,

} ERROR_t;

#define NELEMENTS(a) (sizeof(a) / sizeof(*(a)))

typedef enum LEDS_e
{
	LED_1R = 1,
	LED_1G = 2,
	LED_2R = 4,
	LED_2G = 8,
	LED_ALL = (LED_1R | LED_1G | LED_2R | LED_2G),
} LEDS_t;

#ifndef BOOTLOADER

	#include "FreeRTOS.h"
	#include "task.h"
	#include "queue.h"

	#define ADC_AVERAGE_COUNT	5
	#define SDI_RX_BUFFER		32

	#define SNR_STACK_SIZE	0x200
	#define GPS_STACK_SIZE	0x200
	#define GSM_STACK_SIZE	0x200

	#define SNR_PRIORITY	tskIDLE_PRIORITY + 3
	#define GSM_PRIORITY	tskIDLE_PRIORITY + 2
	#define GPS_PRIORITY	tskIDLE_PRIORITY + 1

	typedef struct SdiContext_s
	{
	  const	char *	TxBuffer;
			char	TxLast;
			char	Address;
			uint8_t	RxIndex;
			uint8_t	TxIndex;
			ERROR_t	Status;
			char	RxBuffer[SDI_RX_BUFFER];
	} SdiContext_t;
	extern SdiContext_t SdiContext;

	typedef struct AdcValues_s
	{
		uint16_t	Adc_Uin;
		uint16_t	Adc_Iin;
		uint16_t	Adc_Ubat;
		uint16_t	Adc_Ibat_P;
		uint16_t	Adc_Ibat_M;
	} AdcValues_t;

	typedef struct AdcContext_s
	{
		AdcValues_t		Values[ADC_AVERAGE_COUNT];
		AdcValues_t		AvgValues;
			uint8_t		Count;
			uint8_t		Time;
			uint8_t		AvgIndex;
	   volatile bool	Ready;
	   volatile bool	Disable;
	} AdcContext_t;
	extern AdcContext_t AdcContext;

	typedef struct MainContext_s
	{
		TaskHandle_t	GpsTaskHandle;
		TaskHandle_t	GsmTaskHandle;
		TaskHandle_t	SensorsTaskHandle;
		QueueHandle_t	ErrorQueue;
		UU16_t			ErrorState;
		uint16_t		ErrorTime;
	} MainContext_t;

	extern MainContext_t MainContext;

#else

	#define ShowError(e)	ShowErrorEx(e)

#endif	/*	BOOTLOADER	*/

//	Version structure for write/read transactions
//	Transaction data block
//	{
//		Version_t	Version		must be first element
//			...					other data
//	}
typedef struct Version_s
{
	uint32_t	CRC32;		// CRC32 for data structure
	uint32_t	Version;	// Numeric version
	uint16_t	SizeX;		// Size of data block (total)
	uint16_t	AddressX;	// Size of data block (total)
} Version_t;

void DelayMicro(uint16_t us);
void DelayMilli(uint16_t ms);

void Delay(uint16_t ms);

void LEDS_Off(LEDS_t leds);
void LEDS_On(LEDS_t leds);
void LEDS_Set(LEDS_t leds);
void LEDS_Toggle(LEDS_t leds);

void ShowError(ERROR_t error);
void ShowFatalError(ERROR_t error);
void ShowErrorEx(ERROR_t error);

ERROR_t LogError2(const char * msg, ERROR_t error);

bool IsCalibrate(void);
bool IsPowerSolar(void);
bool IsPowerExt(void);

void RebootSystem(void);

ERROR_t LogInit(void);
ERROR_t LogAdd(uint32_t logType, uint32_t time, const char * message);
ERROR_t LogAddFormat(uint32_t logType, uint32_t time, const char * fmt, ...);

#ifdef __GNUC__
/*	With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
	set to 'Yes') calls __io_putchar() */
	#define PUTCHAR_PROTOTYPE	int	__io_putchar(int ch)
	#define PUTCHAR(ch)				__io_putchar(ch)
#else
	#define PUTCHAR_PROTOTYPE	int	fputc(int ch, FILE *f)
	#define PUTCHAR(ch)				debug_putc(ch)
#endif /* __GNUC__ */

#define GETCHAR()			debug_getc()
#define GETCHAR_NOWAIT()	debug_getc_nowait()
#define DEBUG(msg)			debug(msg)
#define PRINTF(msg, ...)	printf(msg, __VA_ARGS__)

void debug(const char *msg);
PUTCHAR_PROTOTYPE;
char debug_getc_nowait(void);
char debug_getc(void);
void debug_putc(uint8_t ch);
bool Debug_IsOutput(void);

// Commands and Task definitions for GSM Task
typedef enum
{
	GSM_START,
	GSM_STOP,
	GSM_CHECK_SMS,
	GSM_SEND_HTTP,
} GSM_COMMAND_t;

typedef struct GsmTaskData_s
{
	GSM_COMMAND_t	Command;
	const char	  *	Data;
		ERROR_t		Error;
} GsmTaskData_t;

typedef void (*pf_GSM_Callback)(GsmTaskData_t * task);

typedef struct GsmTask_s
{
	struct GsmTaskData_s TaskData;
	pf_GSM_Callback pfCallback;
} GsmTask_t;

#define GSM_TASK_QUEUE_SIZE	16

// Commands and Task definitions for GPS Task
typedef enum
{
	GPS_START,
	GPS_STOP,
} GPS_COMMAND_t;

typedef struct GpsTaskData_s
{
	GPS_COMMAND_t	Command;
	const char	  *	Data;
		ERROR_t		Error;
} GpsTaskData_t;

typedef void (*pf_GPS_Callback)(GpsTaskData_t * task);

typedef struct GpsTask_s
{
	struct GpsTaskData_s TaskData;
	pf_GPS_Callback pfCallback;
} GpsTask_t;

#define GPS_TASK_QUEUE_SIZE	2

#endif
