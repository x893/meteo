#ifndef __VERSION_H__
#define __VERSION_H__

#include "common.h"

ERROR_t VersionFill(void * src, uint16_t size, uint32_t version, uint16_t address);
ERROR_t VersionCheck(Version_t * src);
ERROR_t VersionLoad(void *dst, uint16_t address, uint16_t size);
ERROR_t VersionLoadEx(void *dst, uint16_t address, uint16_t size);
ERROR_t VersionSaveEx(void *src, uint16_t address, uint16_t size);

#endif
