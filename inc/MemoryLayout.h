#ifndef __MEM_LAYOUT_H__
#define __MEM_LAYOUT_H__

#include <stdint.h>
#include "config.h"

#define FW_BLOCK_SIZE		(2048)
#define FW_BLOCK_COUNT		((256 * 1024) / FW_BLOCK_SIZE)

#define FW_REGION_TABLE		(FW_BASE + 0x130)

typedef struct FM24_FW_Info_s
{
	uint32_t	InfoCRC;
	uint32_t	InfoVersion;

	uint32_t	FwCRC;
	uint8_t		FwVersion;
	uint8_t		FwFails;	// 0xFF for original copy
							// 0x0x count for fail start
	uint16_t	FwBlocks;
	uint32_t	FwSize;
	uint32_t	FwBlockCRC[FW_BLOCK_COUNT];
} FM24_FW_Info_t;

typedef struct FM24_FW_InfoAll_s
{
	FM24_FW_Info_t	Fw0[2];
	FM24_FW_Info_t	Fw1[2];

} FM24_FW_InfoAll_t;


typedef struct FM24_ConfigAll_s
{
	ConfigHost_t	ConfigHost[2];
	ConfigLocal_t	ConfigLocal[2];
	Parameters_t	Parameters[2];
} FM24_ConfigAll_t;

#define FW_INFO_SIZE_U32	((sizeof(FM24_FW_Info_t) + sizeof(uint32_t) - 1) / sizeof(uint32_t))
#define FM24_BASE			(0x0)
typedef struct FM24_Memory_s
{
	FM24_FW_Info_t	FwInfo_0[2];
	FM24_FW_Info_t	FwInfo_1[2];
	ConfigHost_t	ConfigHost[2];
	ConfigLocal_t	ConfigLocal[2];
	Parameters_t	Parameters[2];
} FM24_Memory_t;

#define FM24_OFFSET(elem)	(FM24_BASE + offsetof(FM24_Memory_t, elem))

#define AT45_FW_0_BASE		(uint32_t)(0x0)
#define AT45_FW_1_BASE		(uint32_t)(AT45_FW_0_BASE	+ 256 * 1024)
#define AT45_LOG_BASE		(uint32_t)(AT45_FW_1_BASE	+ 256 * 1024)
#define AT45_LOG_END		(uint32_t)(AT45_LOG_BASE	+ 1536 * 1024)

typedef struct RegionTable_s {
	uint32_t *	RegionTableLimit;
	uint32_t *	SectionDataBase;
	uint32_t	SectionDataSize;
	uint32_t *	__scatterload_copy;
	uint32_t *	LR_IROM1_Limit;
	uint32_t *	Section_BSS_Base;
	uint32_t	RW_IRAM1_Size;
	uint32_t *	__scatterload_zeroinit;
} RegionTable_t;

#endif
